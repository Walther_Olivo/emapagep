import 'dart:convert';
import 'package:emapagep/models/medio_promocion.dart';
import 'package:http/http.dart' as http;
import 'package:emapagep/dto/generic_response_dto.dart';
import 'package:emapagep/config/constants.dart';

class MedioPromocionService {
  static String url = Constants.baseUrl + Constants.pathMedioPromocion;

  // Create
  Future<GenericResponseDto<MedioPromocion>> createMedioPromocion(
      MedioPromocion medioPromocion, String token) async {
    final response = await http.post(
      Uri.parse(url),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: json.encode(medioPromocion.toJson()),
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      return GenericResponseDto<MedioPromocion>.fromJson(
        jsonResponse,
        (payload) => MedioPromocion.fromJson(payload),
      );
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  // Read (by ID)
  Future<GenericResponseDto<MedioPromocion>> getMedioPromocionById(
      int id, String token) async {
    final response = await http.get(
      Uri.parse('$url/$id'),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      return GenericResponseDto<MedioPromocion>.fromJson(
        jsonResponse,
        (payload) => MedioPromocion.fromJson(payload),
      );
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  // Read (all)
  Future<GenericResponseDto<List<MedioPromocion>>> getAllMedioPromociones(
    String token, {
    int? page,
    int? size,
  }) async {
    // Construir la URL con parámetros opcionales
    final uri = Uri.parse('$url').replace(
      queryParameters: {
        if (page != null) 'page': page.toString(),
        if (size != null) 'size': size.toString(),
      },
    );

    final response = await http.get(
      uri,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      final genericResponse = GenericResponseDto<List<MedioPromocion>>.fromJson(
        jsonResponse,
        (payload) => (payload as List)
            .map((item) => MedioPromocion.fromJson(item))
            .toList(),
      );
      return genericResponse;
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  // Update
  Future<GenericResponseDto<MedioPromocion>> updateMedioPromocion(
      int id, MedioPromocion medioPromocion, String token) async {
    final response = await http.put(
      Uri.parse('$url/$id'),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: json.encode(medioPromocion.toJson()),
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      return GenericResponseDto<MedioPromocion>.fromJson(
        jsonResponse,
        (payload) => MedioPromocion.fromJson(payload),
      );
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  // Delete
  Future<GenericResponseDto<void>> deleteMedioPromocion(
      int id, String token) async {
    final response = await http.delete(
      Uri.parse('$url/$id'),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );
    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      return GenericResponseDto<void>.fromJson(
        jsonResponse,
        (payload) => null,
      );
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }
}
