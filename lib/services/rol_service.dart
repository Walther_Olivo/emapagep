import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:emapagep/models/rol.dart';
import 'package:emapagep/dto/generic_response_dto.dart';
import 'package:emapagep/config/constants.dart';

class RolService {
  static String url = Constants.baseUrl + Constants.pathRol;

  Future<GenericResponseDto<Rol>> createRol(Rol rol, String token) async {
    if (token == null) {
      throw Exception('Unknown error occurred');
    }
    final response = await http.post(
      Uri.parse(url),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token'
      },
      body: json.encode(rol.toJson()),
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      return GenericResponseDto<Rol>.fromJson(
        jsonResponse,
        (payload) => Rol.fromJson(payload),
      );
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  // Read (by ID)
  Future<GenericResponseDto<Rol>> getRolById(int id, String token) async {
    if (token == null) {
      throw Exception('Token not found. Please log in again.');
    }
    final response = await http.get(
      Uri.parse('$url/$id'),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      return GenericResponseDto<Rol>.fromJson(
        jsonResponse,
        (payload) => Rol.fromJson(payload),
      );
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  Future<GenericResponseDto<List<Rol>>> getAllRoles(
    String token, {
    int? page,
    int? size,
  }) async {
    // Construir la URL con parámetros opcionales
    final uri = Uri.parse('$url').replace(
      queryParameters: {
        if (page != null) 'page': page.toString(),
        if (size != null) 'size': size.toString(),
      },
    );

    final response = await http.get(
      uri,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      final genericResponse = GenericResponseDto<List<Rol>>.fromJson(
        jsonResponse,
        (payload) =>
            (payload as List).map((item) => Rol.fromJson(item)).toList(),
      );
      return genericResponse;
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  // Update
  Future<GenericResponseDto<Rol>> updateRol(
      int id, Rol rol, String token) async {
    if (token == null) {
      throw Exception('Token not found. Please log in again.');
    }
    final response = await http.put(
      Uri.parse('$url/$id'),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token'
      },
      body: json.encode(rol.toJson()),
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      return GenericResponseDto<Rol>.fromJson(
        jsonResponse,
        (payload) => Rol.fromJson(payload),
      );
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  // Delete
  Future<GenericResponseDto<void>> deleteRol(int id, String token) async {
    if (token == null) {
      throw Exception('Token not found. Please log in again.');
    }
    final response = await http.delete(
      Uri.parse('$url/$id'),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token'
      },
    );
    final jsonResponse = json.decode(response.body);
    if (response.statusCode == 201) {
      return GenericResponseDto<void>.fromJson(
        jsonResponse,
        (payload) => null,
      );
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }
}
