import 'dart:convert';
import 'package:emapagep/models/usuario.dart';
import 'package:http/http.dart' as http;
import 'package:emapagep/dto/change_password_dto.dart';
import 'package:emapagep/dto/validation_code_dto.dart';
import 'package:emapagep/dto/login_dto.dart';
import 'package:emapagep/dto/generic_response_dto.dart';
import 'package:emapagep/config/constants.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:emapagep/dto/login_response_dto.dart';
import 'package:emapagep/dto/valid_email_send_code_dto.dart';

class AuthService {
  static String urlChangePass =
      '${Constants.baseUrl}${Constants.pathAuth}${Constants.pathChangePassword}';
  static String urlValidateCode =
      '${Constants.baseUrl}${Constants.pathAuth}${Constants.pathValidateCode}';
  static String urlValidEmailSend =
      '${Constants.baseUrl}${Constants.pathAuth}${Constants.pathValidEmailSendCode}';
  static String urlLogin =
      '${Constants.baseUrl}${Constants.pathAuth}${Constants.pathLogin}';
  static String urlRegister =
      '${Constants.baseUrl}${Constants.pathAuth}${Constants.pathRegister}';
  final FlutterSecureStorage storage;
  AuthService(this.storage);

  Future<GenericResponseDto<Usuario>> register(Usuario usuario) async {
    final response = await http.post(
      Uri.parse(urlRegister),
      headers: {'Content-Type': 'application/json'},
      body: json.encode(usuario.toJson()),
    );

    final jsonResponse = json.decode(response.body);
    if (response.statusCode == 201) {
      return GenericResponseDto<Usuario>.fromJson(
        jsonResponse,
        (payload) => Usuario.fromJson(payload),
      );
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  Future<GenericResponseDto<LoginResponseDto>> login(LoginDto loginDto) async {
    final response = await http.post(
      Uri.parse(urlLogin),
      headers: {'Content-Type': 'application/json'},
      body: json.encode(loginDto.toJson()),
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      final payload = jsonResponse['payload'];
      final token = payload['token'] as String;
      if (token != null && token.isNotEmpty) {
        await storage.write(key: 'auth_token', value: token);
      } else {
        throw Exception('Failed to retrieve toasasaken');
      }
      return GenericResponseDto<LoginResponseDto>.fromJson(
        jsonResponse,
        (payload) => LoginResponseDto.fromJson(payload),
      );
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  Future<GenericResponseDto<String>> changePassword(
      ChangePasswordDto changePasswordDto) async {
    final response = await http.post(
      Uri.parse(urlChangePass),
      headers: {'Content-Type': 'application/json'},
      body: json.encode(changePasswordDto.toJson()),
    );

    final jsonResponse = json.decode(response.body);

    return GenericResponseDto<String>.fromJson(
      jsonResponse,
      (payload) => payload as String,
    );
  }

  Future<GenericResponseDto<String>> validateCode(
      ValidationCodeDto validationCodeDto) async {
    final response = await http.post(
      Uri.parse(urlValidateCode),
      headers: {'Content-Type': 'application/json'},
      body: json.encode(validationCodeDto.toJson()),
    );
    final jsonResponse = json.decode(response.body);

    return GenericResponseDto<String>.fromJson(
      jsonResponse,
      (payload) => payload as String,
    );
  }

  Future<GenericResponseDto<String>> validEmailAndSendCode(
      ValidEmailSendCodeDto validEmailSendCodeDto) async {
    final response = await http.post(
      Uri.parse('$urlValidEmailSend'),
      headers: {'Content-Type': 'application/json'},
      body: json.encode(validEmailSendCodeDto.toJson()),
    );

    final jsonResponse = json.decode(response.body);

    return GenericResponseDto<String>.fromJson(
      jsonResponse,
      (payload) => payload as String,
    );
  }
}
