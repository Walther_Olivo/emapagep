import 'dart:convert';
import 'package:emapagep/dto/asingar_solucion_dto.dart';
import 'package:emapagep/dto/reclamos_dto.dart';
import 'package:emapagep/models/reclamo.dart';
import 'package:http/http.dart' as http;
import 'package:emapagep/dto/generic_response_dto.dart';
import 'package:emapagep/config/constants.dart';

class ReclamoService {
  static String url = Constants.baseUrl + Constants.pathReclamo;
  static String urlReclamosByCedula =
      Constants.baseUrl + Constants.pathReclamo + "/buscarPorCedula";
  static String urlSolucionReclamos = "$url/solucion";
  static String urlReclamosByContrato =
      Constants.baseUrl + Constants.pathReclamo + Constants.pathFindContrato;
  Future<GenericResponseDto<Reclamo>> createReclamo(
      Reclamo reclamo, String token) async {
    final response = await http.post(
      Uri.parse(url),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token'
      },
      body: json.encode(reclamo.toJson()),
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      return GenericResponseDto<Reclamo>.fromJson(
        jsonResponse,
        (payload) => Reclamo.fromJson(payload),
      );
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  // Read (by ID)
  Future<GenericResponseDto<Reclamo>> getReclamoById(
      int id, String token) async {
    final response = await http.get(
      Uri.parse('$url/$id'),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token'
      },
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      return GenericResponseDto<Reclamo>.fromJson(
        jsonResponse,
        (payload) => Reclamo.fromJson(payload),
      );
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  // Read (all)
  Future<GenericResponseDto<List<Reclamo>>> getAllReclamos(
    String token, {
    int? creadorId,
    int? page,
    int? size,
  }) async {
    // Construir la URL con parámetros opcionales
    final uri = Uri.parse('$url').replace(
      queryParameters: {
        if (creadorId != null) 'creadorId': creadorId.toString(),
        if (page != null) 'page': page.toString(),
        if (size != null) 'size': size.toString(),
      },
    );

    final response = await http.get(
      uri,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      final genericResponse = GenericResponseDto<List<Reclamo>>.fromJson(
        jsonResponse,
        (payload) =>
            (payload as List).map((item) => Reclamo.fromJson(item)).toList(),
      );
      return genericResponse;
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  // Update
  Future<GenericResponseDto<Reclamo>> updateReclamo(
      int id, Reclamo reclamo, String token) async {
    final response = await http.put(
      Uri.parse('$url/$id'),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token'
      },
      body: json.encode(reclamo.toJson()),
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      return GenericResponseDto<Reclamo>.fromJson(
        jsonResponse,
        (payload) => Reclamo.fromJson(payload),
      );
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  // Delete
  Future<GenericResponseDto<void>> deleteReclamo(int id, String token) async {
    final response = await http.delete(
      Uri.parse('$url/$id'),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token'
      },
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      return GenericResponseDto<void>.fromJson(
        jsonResponse,
        (payload) => null,
      );
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  Future<List<Reclamo>> getReclamosByIdentificacion(
      String token, String cedula) async {
    final response = await http.get(
      Uri.parse('$urlReclamosByCedula?cedula=$cedula'),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token'
      },
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      final genericResponse = GenericResponseDto<List<Reclamo>>.fromJson(
        jsonResponse,
        (payload) =>
            (payload as List).map((item) => Reclamo.fromJson(item)).toList(),
      );
      return genericResponse.payload;
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  Future<GenericResponseDto<Reclamo>> respondReclamo(
      AsignarSolucionDto asignarSolucion, String token) async {
    final response = await http.put(
      Uri.parse('$urlSolucionReclamos'),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token'
      },
      body: json.encode(asignarSolucion.toJson()),
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      return GenericResponseDto<Reclamo>.fromJson(
        jsonResponse,
        (payload) => Reclamo.fromJson(payload),
      );
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  Future<GenericResponseDto<ReclamosDto>> getReclamosByContrato(
      String token, String numContrato) async {
    final response = await http.get(
      Uri.parse('$urlReclamosByContrato?numContrato=$numContrato'),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      if (jsonResponse['payload'] != null &&
          jsonResponse['payload'].isNotEmpty) {
        final genericResponse = GenericResponseDto<ReclamosDto>.fromJson(
          jsonResponse,
          (payload) => ReclamosDto.fromJson(payload),
        );
        return genericResponse;
      } else {
        throw Exception('${jsonResponse['message']}');
      }
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }
}
