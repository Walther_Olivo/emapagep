import 'dart:convert';
import 'package:emapagep/models/detalle_parametro.dart';
import 'package:http/http.dart' as http;
import 'package:emapagep/dto/generic_response_dto.dart';
import 'package:emapagep/config/constants.dart';

class DetalleParametroService {
  static String url = Constants.baseUrl + Constants.pathDetalleParametro;
  static String urlRespuestas = url + Constants.pathRespuestas;
  static String urlSolucionReclamos = url + Constants.pathSolucionReclamos;
  static String urlMotivosSolucionReclamos =
      url + Constants.pathMotivosSolucionReclamos;

  static String urlParentescos = url + Constants.pathParentescos;

  // Create
  Future<GenericResponseDto<DetalleParametro>> createDetalleParametro(
      DetalleParametro detalleParametro, String token) async {
    final response = await http.post(
      Uri.parse(url),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: json.encode(detalleParametro.toJson()),
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      return GenericResponseDto<DetalleParametro>.fromJson(
        jsonResponse,
        (payload) => DetalleParametro.fromJson(payload),
      );
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  // Read (by ID)
  Future<GenericResponseDto<DetalleParametro>> getDetalleParametroById(
      int id, String token) async {
    final response = await http.get(
      Uri.parse('$url/$id'),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      return GenericResponseDto<DetalleParametro>.fromJson(
        jsonResponse,
        (payload) => DetalleParametro.fromJson(payload),
      );
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  // Read (all)
  Future<GenericResponseDto<List<DetalleParametro>>> getAllDetalleParametros(
    String token, {
    int? page,
    int? size,
  }) async {
    // Construir la URL con parámetros opcionales
    final uri = Uri.parse('$url').replace(
      queryParameters: {
        if (page != null) 'page': page.toString(),
        if (size != null) 'size': size.toString(),
      },
    );

    final response = await http.get(
      uri,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      final genericResponse =
          GenericResponseDto<List<DetalleParametro>>.fromJson(
        jsonResponse,
        (payload) => (payload as List)
            .map((item) => DetalleParametro.fromJson(item))
            .toList(),
      );
      return genericResponse;
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  Future<List<DetalleParametro>> getAllRespuestasIteragua(String token) async {
    final response = await http.get(
      Uri.parse(urlRespuestas),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      final genericResponse =
          GenericResponseDto<List<DetalleParametro>>.fromJson(
        jsonResponse,
        (payload) => (payload as List)
            .map((item) => DetalleParametro.fromJson(item))
            .toList(),
      );
      return genericResponse.payload ?? [];
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  // Update
  Future<GenericResponseDto<DetalleParametro>> updateDetalleParametro(
      int id, DetalleParametro detalleParametro, String token) async {
    final response = await http.put(
      Uri.parse('$url/$id'),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: json.encode(detalleParametro.toJson()),
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      return GenericResponseDto<DetalleParametro>.fromJson(
        jsonResponse,
        (payload) => DetalleParametro.fromJson(payload),
      );
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  // Delete
  Future<GenericResponseDto<void>> deleteDetalleParametro(
      int id, String token) async {
    final response = await http.delete(
      Uri.parse('$url/$id'),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      return GenericResponseDto<void>.fromJson(
        jsonResponse,
        (payload) => null,
      );
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  Future<List<DetalleParametro>> getSolucionReclamos(String token) async {
    final response = await http.get(
      Uri.parse(urlSolucionReclamos),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      final genericResponse =
          GenericResponseDto<List<DetalleParametro>>.fromJson(
        jsonResponse,
        (payload) => (payload as List)
            .map((item) => DetalleParametro.fromJson(item))
            .toList(),
      );
      return genericResponse.payload ?? [];
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  Future<List<DetalleParametro>> getMotivosSolucionReclamos(
      String token) async {
    final response = await http.get(
      Uri.parse(urlMotivosSolucionReclamos),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      final genericResponse =
          GenericResponseDto<List<DetalleParametro>>.fromJson(
        jsonResponse,
        (payload) => (payload as List)
            .map((item) => DetalleParametro.fromJson(item))
            .toList(),
      );
      return genericResponse.payload ?? [];
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  Future<List<DetalleParametro>> getParentescos(String token) async {
    final response = await http.get(
      Uri.parse(urlParentescos),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      final genericResponse =
          GenericResponseDto<List<DetalleParametro>>.fromJson(
        jsonResponse,
        (payload) => (payload as List)
            .map((item) => DetalleParametro.fromJson(item))
            .toList(),
      );
      return genericResponse.payload ?? [];
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }
}
