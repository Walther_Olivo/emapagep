import 'dart:convert';
import 'package:emapagep/dto/edit_profile_dto.dart';
import 'package:emapagep/models/usuario.dart';
import 'package:http/http.dart' as http;
import 'package:emapagep/dto/generic_response_dto.dart';
import 'package:emapagep/config/constants.dart';

class UsuarioService {
  static String url = Constants.baseUrl + Constants.pathUsuario;
  static String profile = Constants.pathEditProfile;

  // Create
  Future<GenericResponseDto<Usuario>> createUsuario(
      Usuario usuario, String token) async {
    final response = await http.post(
      Uri.parse(url),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: json.encode(usuario.toJson()),
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      return GenericResponseDto<Usuario>.fromJson(
        jsonResponse,
        (payload) => Usuario.fromJson(payload),
      );
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  // Read (by ID)
  Future<GenericResponseDto<Usuario>> getUsuarioById(
      int id, String token) async {
    final response = await http.get(
      Uri.parse('$url/$id'),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      return GenericResponseDto<Usuario>.fromJson(
        jsonResponse,
        (payload) => Usuario.fromJson(payload),
      );
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  Future<GenericResponseDto<List<Usuario>>> getAllUsuarios(
    String token, {
    int? page,
    int? size,
  }) async {
    // Construir la URL con parámetros opcionales
    final uri = Uri.parse('$url').replace(
      queryParameters: {
        if (page != null) 'page': page.toString(),
        if (size != null) 'size': size.toString(),
      },
    );

    final response = await http.get(
      uri,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      final genericResponse = GenericResponseDto<List<Usuario>>.fromJson(
        jsonResponse,
        (payload) =>
            (payload as List).map((item) => Usuario.fromJson(item)).toList(),
      );
      return genericResponse;
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  // Update
  Future<GenericResponseDto<Usuario>> updateUsuario(
      int id, Usuario usuario, String token) async {
    final response = await http.put(
      Uri.parse('$url/$id'),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: json.encode(usuario.toJson()),
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      return GenericResponseDto<Usuario>.fromJson(
        jsonResponse,
        (payload) => Usuario.fromJson(payload),
      );
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  Future<GenericResponseDto<EditProfileDto>> editProfile(
      int id, EditProfileDto usuario, String token) async {
    final response = await http.put(
      Uri.parse('$url$profile/$id'),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: json.encode(usuario.toJson()),
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      return GenericResponseDto<EditProfileDto>.fromJson(
        jsonResponse,
        (payload) => EditProfileDto.fromJson(payload),
      );
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }

  // Delete
  Future<GenericResponseDto<void>> deleteUsuario(int id, String token) async {
    final response = await http.delete(
      Uri.parse('$url/$id'),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    final jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      return GenericResponseDto<void>.fromJson(
        jsonResponse,
        (payload) => null,
      );
    } else {
      throw Exception('${jsonResponse['message'] ?? 'Unknown error occurred'}');
    }
  }
}
