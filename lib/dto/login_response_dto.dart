class LoginResponseDto {
  final String identifier;
  final String token;

  LoginResponseDto({
    required this.identifier,
    required this.token,
  });

  factory LoginResponseDto.fromJson(Map<String, dynamic> json) {
    return LoginResponseDto(
      identifier: json['identifier'],
      token: json['token'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'identifier': identifier,
      'token': token,
    };
  }
}
