class LoginDto {
  final String identifier;
  final String contrasenia;

  LoginDto({
    required this.identifier,
    required this.contrasenia,
  });

  factory LoginDto.fromJson(Map<String, dynamic> json) {
    return LoginDto(
      identifier: json['identifier'],
      contrasenia: json['contrasenia'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'identifier': identifier,
      'contrasenia': contrasenia,
    };
  }
}
