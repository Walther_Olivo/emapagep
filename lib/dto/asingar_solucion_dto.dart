class AsignarSolucionDto {
  final int reclamoId;
  final int solucionId;
  final int? motivoSolucionId;
  final String? form;

  AsignarSolucionDto({
    required this.reclamoId,
    required this.solucionId,
    this.motivoSolucionId,
    this.form,
  });

  factory AsignarSolucionDto.fromJson(Map<String, dynamic> json) {
    return AsignarSolucionDto(
      reclamoId: json['reclamoId'] as int,
      solucionId: json['solucionId'] as int,
      motivoSolucionId: json['motivoSolucionId'] as int?,
      form: json['form'] as String?,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'reclamoId': reclamoId,
      'solucionId': solucionId,
      'motivoSolucionId': motivoSolucionId,
      'form': form,
    };
  }
}
