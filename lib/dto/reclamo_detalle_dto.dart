class ReclamoDetalleDto {
  final String codigoAccion;
  final String codigoDetalle;
  final String descripcionAccion;
  final String usuaringDetalle;
  final String fechaingDetalle;
  final String estadoDetalle;

  ReclamoDetalleDto({
    required this.codigoAccion,
    required this.codigoDetalle,
    required this.descripcionAccion,
    required this.usuaringDetalle,
    required this.fechaingDetalle,
    required this.estadoDetalle,
  });

  factory ReclamoDetalleDto.fromJson(Map<String, dynamic> json) {
    return ReclamoDetalleDto(
      codigoAccion: json['codigoAccion'],
      codigoDetalle: json['codigoDetalle'],
      descripcionAccion: json['descripcionAccion'],
      usuaringDetalle: json['usuaringDetalle'],
      fechaingDetalle: json['fechaingDetalle'],
      estadoDetalle: json['estadoDetalle'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'codigoAccion': codigoAccion,
      'codigoDetalle': codigoDetalle,
      'descripcionAccion': descripcionAccion,
      'usuaringDetalle': usuaringDetalle,
      'fechaingDetalle': fechaingDetalle,
      'estadoDetalle': estadoDetalle,
    };
  }
}
