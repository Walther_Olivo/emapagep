class ValidEmailSendCodeDto {
  final String email;
  final String purpose;

  ValidEmailSendCodeDto({
    required this.email,
    required this.purpose,
  });

  // Método para crear una instancia desde un mapa (por ejemplo, JSON)
  factory ValidEmailSendCodeDto.fromJson(Map<String, dynamic> json) {
    return ValidEmailSendCodeDto(
      email: json['email'] as String,
      purpose: json['purpose'] as String,
    );
  }

  // Método para convertir la instancia a un mapa (por ejemplo, para JSON)
  Map<String, dynamic> toJson() {
    return {
      'email': email,
      'purpose': purpose,
    };
  }
}
