class GenericResponseDto<T> {
  final int code;
  final String message;
  final T payload;
  final String status;
  final int? totalRecords;
  final int? totalPages;

  GenericResponseDto({
    required this.code,
    required this.message,
    required this.payload,
    required this.status,
    this.totalRecords,
    this.totalPages,
  });

  factory GenericResponseDto.fromJson(
      Map<String, dynamic> json, T Function(dynamic) fromJsonT) {
    return GenericResponseDto<T>(
      code: json['code'],
      message: json['message'],
      payload: fromJsonT(json['payload']),
      status: json['status'],
      totalRecords: json['totalRecords'] != null ? json['totalRecords'] : null,
      totalPages: json['totalPages'] != null ? json['totalPages'] : null,
    );
  }
}
