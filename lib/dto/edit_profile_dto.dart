class EditProfileDto {
  final int? id;
  final String nombres;
  final String apellidos;
  final String nombreUsuario;
  final String telefono;
  final String direccion;
  final String? foto;

  EditProfileDto({
    this.id,
    required this.nombres,
    required this.apellidos,
    required this.nombreUsuario,
    required this.telefono,
    required this.direccion,
    this.foto,
  });

  factory EditProfileDto.fromJson(Map<String, dynamic> json) {
    return EditProfileDto(
      id: json['id'],
      nombres: json['nombres'],
      apellidos: json['apellidos'],
      nombreUsuario: json['nombreUsuario'],
      telefono: json['telefono'],
      direccion: json['direccion'],
      foto: json['foto'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'nombres': nombres,
      'apellidos': apellidos,
      'nombreUsuario': nombreUsuario,
      'telefono': telefono,
      'direccion': direccion,
      'foto': foto,
    };
  }
}
