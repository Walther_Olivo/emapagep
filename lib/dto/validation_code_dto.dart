class ValidationCodeDto {
  final String email;
  final String code;
  final String purpose;

  ValidationCodeDto({
    required this.email,
    required this.code,
    required this.purpose,
  });

  // Método para crear una instancia desde un mapa (por ejemplo, JSON)
  factory ValidationCodeDto.fromJson(Map<String, dynamic> json) {
    return ValidationCodeDto(
      email: json['email'] as String,
      code: json['code'] as String,
      purpose: json['purpose'] as String,
    );
  }

  // Método para convertir la instancia a un mapa (por ejemplo, para JSON)
  Map<String, dynamic> toJson() {
    return {
      'email': email,
      'code': code,
      'purpose': purpose,
    };
  }
}
