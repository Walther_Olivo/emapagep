import 'reclamo_detalle_dto.dart';

class ReclamosDto {
  final String codigoReclamo;
  final String comentarioReclamo;
  final List<ReclamoDetalleDto> detalles;

  ReclamosDto({
    required this.codigoReclamo,
    required this.comentarioReclamo,
    required this.detalles,
  });

  factory ReclamosDto.fromJson(Map<String, dynamic> json) {
    var detallesFromJson = json['detalles'] as List;
    List<ReclamoDetalleDto> detallesList =
        detallesFromJson.map((i) => ReclamoDetalleDto.fromJson(i)).toList();

    return ReclamosDto(
      codigoReclamo: json['codigoReclamo'],
      comentarioReclamo: json['comentarioReclamo'],
      detalles: detallesList,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'codigoReclamo': codigoReclamo,
      'comentarioReclamo': comentarioReclamo,
      'detalles': detalles.map((detalle) => detalle.toJson()).toList(),
    };
  }
}
