class ChangePasswordDto {
  final String email;
  final String newPassword;

  ChangePasswordDto({
    required this.email,
    required this.newPassword,
  });

  factory ChangePasswordDto.fromJson(Map<String, dynamic> json) {
    return ChangePasswordDto(
      email: json['email'] as String,
      newPassword: json['newPassword'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'email': email,
      'newPassword': newPassword,
    };
  }
}
