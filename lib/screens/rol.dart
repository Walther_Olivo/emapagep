import 'package:emapagep/config/constants.dart';
import 'package:emapagep/dto/generic_response_dto.dart';
import 'package:emapagep/widgets/loading_dialog.dart';
import 'package:flutter/material.dart';
import 'package:emapagep/models/rol.dart';
import 'package:emapagep/services/rol_service.dart';
import 'package:emapagep/widgets/messages.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:jwt_decoder/jwt_decoder.dart';

class RolScreen extends StatefulWidget {
  @override
  _RolScreenState createState() => _RolScreenState();
}

class _RolScreenState extends State<RolScreen> {
  final TextEditingController _nombreController = TextEditingController();
  final TextEditingController _descripcionController = TextEditingController();
  final TextEditingController _searchController = TextEditingController();
  final RolService _rolService = RolService();
  List<Rol> _roles = [];
  List<Rol> _filteredRols = [];
  Rol? _selectedRol;
  bool _isAddingOrUpdating = false;
  final storage = FlutterSecureStorage();
  List<String> roles = [];
  String? _token;
  final _formKey = GlobalKey<FormState>();
  int _currentPage = 1;
  int _totalRecords = 0;
  int _rowsPerPage = 5;

  @override
  void initState() {
    super.initState();
    _loadRolesAndToken();
  }

  Future<void> _loadRolesAndToken() async {
    String? token = await storage.read(key: 'auth_token');
    if (token != null) {
      Map<String, dynamic> decodedToken = JwtDecoder.decode(token);
      setState(() {
        roles = List<String>.from(
            decodedToken['roles'].map((role) => role['name']));
        _token = token;
      });
      _fetchRols(page: _currentPage, size: _rowsPerPage);
    }
  }

  Future<void> _fetchRols({int page = 1, int size = 5}) async {
    if (_token == null) return;
    LoadingDialog.show(context, message: 'Cargando Roles...');
    try {
      final response = await _rolService.getAllRoles(
        _token!,
        page: page,
        size: size,
      );
      final roles = response.payload;
      setState(() {
        _roles = roles;
        _filteredRols = roles;
        _totalRecords = response.totalRecords ?? 0;
        _currentPage = page; // Actualiza la página actual
        _rowsPerPage = size;
        _filterRols();
        LoadingDialog.hide(context);
      });
    } catch (e) {
      LoadingDialog.hide(context);
      final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
      Messages.showMessage(context, 'Error: $errorMessage',
          isError: true, isTop: true);
    }
  }

  void _filterRols() {
    final query = _searchController.text.toLowerCase();
    setState(() {
      _filteredRols = _roles.where((rol) {
        return rol.nombre.toLowerCase().contains(query) ||
            rol.id.toString().contains(query);
      }).toList();
    });
  }

  Future<void> _saveRol() async {
    if (_token == null) return;
    if (_formKey.currentState?.validate() ?? false) {
      final nombre = _nombreController.text;
      final descripcion = _descripcionController.text;
      if (nombre.isEmpty) return;
      if (descripcion.isEmpty) return;

      final rol = Rol(
        id: _selectedRol?.id,
        nombre: nombre,
        estado: 'Activo',
        descripcion: descripcion,
      );

      try {
        GenericResponseDto<void> response;
        if (_selectedRol == null) {
          LoadingDialog.show(context, message: 'Creando Rol...');
          response = await _rolService.createRol(rol, _token!);
          LoadingDialog.hide(context);
        } else {
          LoadingDialog.show(context, message: 'Actualizando Rol...');
          response = await _rolService.updateRol(rol.id!, rol, _token!);
          LoadingDialog.hide(context);
        }
        Messages.showMessage(context, response.message);
        Navigator.of(context).pop(); // Cierra el modal
        _fetchRols(page: _currentPage, size: _rowsPerPage);
        setState(() {
          _isAddingOrUpdating = false;
          _selectedRol = null;
        });
      } catch (e) {
        LoadingDialog.hide(context);
        final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
        Messages.showMessage(context, 'Error: $errorMessage',
            isError: true, isTop: true);
      }
    }
  }

  Future<void> _deleteRol(int id) async {
    if (_token == null) return;
    GenericResponseDto<void> response;
    LoadingDialog.show(context, message: 'Eliminando Rol...');
    try {
      response = await _rolService.deleteRol(id, _token!);
      LoadingDialog.hide(context);
      _fetchRols(page: _currentPage, size: _rowsPerPage);
      Messages.showMessage(context, response.message);
    } catch (e) {
      LoadingDialog.hide(context);
      final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
      Messages.showMessage(context, 'Error: $errorMessage',
          isError: true, isTop: true);
    }
  }

  void _showDetails(Rol rol) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16.0), // Bordes redondeados
          ),
          title: Text(
            'Detalles',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          content: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildDetailRow('ID:', rol.id.toString()),
                _buildDetailRow('Nombre:', rol.nombre),
                _buildDetailRow('Descripción:', rol.descripcion),
                _buildDetailRow('Estado:', rol.estado),
                _buildDetailRow(
                    'Fe. Creación:', rol.fechaCreacion.toString() ?? 'N/A'),
                _buildDetailRow('Fe. Modificación:',
                    rol.fechaModificacion.toString() ?? 'N/A'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                'Cerrar',
                style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  Widget _buildDetailRow(String title, String value) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(
              text: '$title ',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16,
                color: Colors.black,
              ),
            ),
            TextSpan(
              text: value,
              style: TextStyle(
                fontSize: 16,
                color: Colors.black,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _showForm() {
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (BuildContext context) {
        return Padding(
          padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Form(
                key: _formKey, // Asocia el key al Form
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      _selectedRol == null ? 'Crear Rol' : 'Actualizar Rol',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 10),
                    TextFormField(
                      controller: _nombreController,
                      maxLength: 25,
                      decoration: InputDecoration(
                        labelText: 'Nombre del Rol',
                        border: OutlineInputBorder(),
                        counterText: '',
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Por favor ingresa el nombre del rol';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 10),
                    TextFormField(
                      // Cambié a TextFormField para validación
                      controller: _descripcionController,
                      maxLength: 100,
                      decoration: InputDecoration(
                        labelText: 'Descripción del Rol',
                        border: OutlineInputBorder(),
                        counterText: '',
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Por favor ingresa la descripción del rol';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        ElevatedButton(
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              _saveRol();
                            }
                          },
                          child: Text('Guardar'),
                        ),
                        ElevatedButton(
                          onPressed: () {
                            Navigator.of(context).pop(); // Cierra el modal
                            setState(() {
                              _isAddingOrUpdating = false;
                              _nombreController.clear();
                              _descripcionController.clear();
                              _selectedRol = null;
                            });
                          },
                          child: Text('Cancelar'),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: roles.contains(Constants.roleAdmin)
          ? Padding(
              padding: const EdgeInsets.only(
                  bottom: 60.0), // Ajusta el valor según sea necesario
              child: FloatingActionButton(
                onPressed: () {
                  setState(() {
                    _isAddingOrUpdating = true;
                    _selectedRol = null;
                    _nombreController.clear();
                    _descripcionController.clear();
                    _showForm(); // Muestra el formulario
                  });
                },
                child: Icon(Icons.add),
                shape: CircleBorder(), // Asegura que el botón sea redondo
                backgroundColor: Colors.blue, // Color de fondo del botón
              ),
            )
          : null,
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: _searchController,
                    decoration: InputDecoration(
                      hintText: 'Buscar por ID o Nombre',
                      border: OutlineInputBorder(),
                      suffixIcon: Icon(Icons.search),
                    ),
                    onChanged: (value) => _filterRols(),
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
            Expanded(
              child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: PaginatedDataTable(
                    rowsPerPage: _rowsPerPage,
                    availableRowsPerPage: const [5, 10, 15],
                    onRowsPerPageChanged: (rowsPerPage) {
                      setState(() {
                        _rowsPerPage = rowsPerPage ?? 5;
                        _currentPage = 1; // Reinicia a la primera página
                      });
                      _fetchRols(page: _currentPage, size: _rowsPerPage);
                    },
                    onPageChanged: (page) {
                      final newPage = page ~/ _rowsPerPage + 1;
                      if (newPage != _currentPage) {
                        setState(() {
                          _currentPage = newPage;
                        });
                        _fetchRols(page: _currentPage, size: _rowsPerPage);
                      }
                    },
                    columns: [
                      DataColumn(label: Center(child: Text('ID'))),
                      DataColumn(label: Center(child: Text('Nombre'))),
                      DataColumn(label: Center(child: Text('Estado'))),
                      DataColumn(label: Center(child: Text('Acciones'))),
                    ],
                    source: RolDataSource(
                      context: context,
                      roles: _filteredRols, // Usa la lista filtrada
                      onDelete: _deleteRol,
                      onEdit: (Rol rol) {
                        setState(() {
                          _selectedRol = rol;
                          _nombreController.text = rol.nombre;
                          _descripcionController.text = rol.descripcion;
                          _isAddingOrUpdating = true;
                        });
                        _showForm(); // Muestra el formulario de actualización
                      },
                      onView: _showDetails,
                      rolesFound: roles,
                      totalRecords: _filteredRols
                          .length, // Asegúrate de usar la cantidad filtrada
                    ),
                  )),
            ),
          ],
        ),
      ),
    );
  }
}

class RolDataSource extends DataTableSource {
  final BuildContext context;
  final List<Rol> roles;
  final Function(int) onDelete;
  final Function(Rol) onEdit;
  final Function(Rol) onView;
  final List<String> rolesFound;
  final int totalRecords;

  RolDataSource({
    required this.context,
    required this.roles,
    required this.onDelete,
    required this.onEdit,
    required this.onView,
    required this.rolesFound,
    required this.totalRecords,
  });

  @override
  DataRow? getRow(int index) {
    if (roles.isEmpty || index >= roles.length) {
      return null; // No hay coincidencias o estamos fuera de rango
    }
    final rol = roles[index];
    return DataRow(cells: [
      DataCell(Center(child: Text(rol.id.toString()))),
      DataCell(Center(child: Text(rol.nombre))),
      DataCell(Center(child: Text(rol.estado))),
      DataCell(Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: _buildActionButtons(rol),
      )),
    ]);
  }

  List<Widget> _buildActionButtons(Rol rol) {
    List<Widget> buttons = [];

    // Mostrar el botón de detalles a todos los roles
    buttons.add(
      IconButton(
        icon: Icon(Icons.visibility),
        onPressed: () => onView(rol),
      ),
    );

    // Mostrar el botón de edición solo para ADMIN
    if (rolesFound.contains(Constants.roleAdmin)) {
      buttons.add(
        IconButton(
          icon: Icon(Icons.edit),
          onPressed: () => onEdit(rol),
        ),
      );
    }

    // Mostrar el botón de eliminación solo para ADMIN
    if (rolesFound.contains(Constants.roleAdmin)) {
      buttons.add(
        IconButton(
          icon: Icon(Icons.delete),
          onPressed: () {
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text('Confirmación'),
                  content:
                      Text('¿Estás seguro de que quieres eliminar este Rol?'),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text('Cancelar'),
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                        onDelete(rol.id!);
                      },
                      child: Text('Eliminar'),
                    ),
                  ],
                );
              },
            );
          },
        ),
      );
    }

    return buttons;
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => totalRecords;

  @override
  int get selectedRowCount => 0;

  @override
  void selectAll(bool checked) {}
}
