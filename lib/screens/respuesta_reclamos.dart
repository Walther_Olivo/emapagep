import 'dart:io';
import 'dart:typed_data';
import 'package:emapagep/dto/asingar_solucion_dto.dart';
import 'package:emapagep/dto/generic_response_dto.dart';
import 'package:emapagep/models/reclamo.dart';
import 'package:emapagep/services/reclamo_service.dart';
import 'package:emapagep/widgets/loading_dialog.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:emapagep/models/detalle_parametro.dart';
import 'package:emapagep/services/detalle_parametro_service.dart';
import 'package:emapagep/widgets/messages.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'dart:convert';
import 'package:path_provider/path_provider.dart';
import 'package:open_file/open_file.dart';
import 'package:emapagep/config/constants.dart';

class RespuestaReclamoScreen extends StatefulWidget {
  final String cedula;
  final int id;

  RespuestaReclamoScreen({required this.cedula, required this.id});

  @override
  _RespuestaReclamoScreenState createState() => _RespuestaReclamoScreenState();
}

class _RespuestaReclamoScreenState extends State<RespuestaReclamoScreen> {
  final TextEditingController _searchController = TextEditingController();
  final DetalleParametroService _detalleParametroService =
      DetalleParametroService();
  final ReclamoService _reclamoService = ReclamoService();

  List<Reclamo> _reclamos = [];
  List<Reclamo> _filteredReclamos = [];
  List<DetalleParametro> _solucionReclamos = [];
  List<DetalleParametro> _motivosSolucionReclamos = [];
  List<DetalleParametro> _parentescos = [];

  final storage = FlutterSecureStorage();
  List<String> roles = [];
  String? _token;
  Map<int, String> _solucionReclamoMap = {};
  Map<int, String> _motivoSolucionReclamoMap = {};
  Map<int, String> _parentescosMap = {};
  int _currentPage = 1;
  int _totalRecords = 0;
  int _rowsPerPage = 5;
  late ReclamoDataSource _source;

  @override
  void initState() {
    super.initState();
    _loadUserData();
    _source = ReclamoDataSource(
      context: context,
      reclamos: _filteredReclamos,
      onDelete: _deleteReclamo,
      onEdit: (Reclamo reclamo) {
        // TODO: Implement edit functionality
      },
      onView: _showDetails,
      roles: roles,
      solucionReclamos: _solucionReclamos,
      motivosSolucionReclamos: _motivosSolucionReclamos,
      onRespond: _respondReclamo,
      totalRecords: _totalRecords,
      isFilter: false,
    );
  }

  Future<void> _loadUserData() async {
    String? token = await storage.read(key: 'auth_token');
    if (token != null) {
      Map<String, dynamic> decodedToken = JwtDecoder.decode(token);
      roles =
          List<String>.from(decodedToken['roles'].map((role) => role['name']));
      _token = token;
      _fetchSolucionReclamos();
      _fetchMotivosSolucionReclamos();
      _fetchParentescos();
      if (roles.contains(Constants.roleCliente)) {
        //_fetchReclamosByCedula(widget.cedula);
        _fetchAllReclamos(
            creadorId: widget.id, page: _currentPage, size: _rowsPerPage);
      } else {
        _fetchAllReclamos(page: _currentPage, size: _rowsPerPage);
      }
    }
  }

  Future<void> _fetchReclamosByCedula(String cedula) async {
    if (_token == null) return;
    try {
      final reclamos =
          await _reclamoService.getReclamosByIdentificacion(_token!, cedula);
      setState(() {
        _reclamos = reclamos;
        _filteredReclamos = reclamos;
      });
    } catch (e) {
      final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
      Messages.showMessage(context, '$errorMessage', isError: true);
    }
  }

  Future<void> _fetchAllReclamos(
      {int? creadorId, int page = 1, int size = 5}) async {
    if (_token == null) return;
    LoadingDialog.show(context, message: 'Cargando Reclamos...');
    try {
      final response = await _reclamoService.getAllReclamos(
        _token!,
        creadorId: creadorId,
        page: page,
        size: size,
      );
      final reclamos = response.payload;
      setState(() {
        _reclamos = reclamos;
        _filteredReclamos = reclamos;
        _totalRecords = response.totalRecords ?? 0;
        _currentPage = page; // Actualiza la página actual
        _rowsPerPage = size;
        LoadingDialog.hide(context);
        _source = ReclamoDataSource(
          context: context,
          reclamos: _filteredReclamos,
          onDelete: _deleteReclamo,
          onEdit: (Reclamo reclamo) {
            // TODO: Implement edit functionality
          },
          onView: _showDetails,
          roles: roles,
          solucionReclamos: _solucionReclamos,
          motivosSolucionReclamos: _motivosSolucionReclamos,
          onRespond: _respondReclamo,
          totalRecords: _totalRecords,
          isFilter: false,
        );
      });
    } catch (e) {
      LoadingDialog.hide(context);
      final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
      Messages.showMessage(context, '$errorMessage', isError: true);
    }
  }

  Future<void> _fetchSolucionReclamos() async {
    if (_token == null) return;
    try {
      final solucionReclamos =
          await _detalleParametroService.getSolucionReclamos(_token!);
      setState(() {
        _solucionReclamos = solucionReclamos;
        _solucionReclamoMap = {for (var s in solucionReclamos) s.id!: s.nombre};
      });
    } catch (e) {
      final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
      Messages.showMessage(context, '$errorMessage', isError: true);
    }
  }

  Future<void> _deleteReclamo(int id) async {
    GenericResponseDto<void> response;
    if (_token == null) return;
    LoadingDialog.show(context, message: 'Eliminando reclamo...');
    try {
      response = await _reclamoService.deleteReclamo(id, _token!);
      _fetchAllReclamos(page: _currentPage, size: _rowsPerPage);
      LoadingDialog.hide(context);
      Messages.showMessage(context, response.message);
    } catch (e) {
      LoadingDialog.hide(context);
      final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
      Messages.showMessage(context, 'Error: $errorMessage', isError: true);
    }
  }

  Future<void> _fetchMotivosSolucionReclamos() async {
    if (_token == null) return;
    try {
      final motivosSolucionReclamos =
          await _detalleParametroService.getMotivosSolucionReclamos(_token!);
      setState(() {
        _motivosSolucionReclamos = motivosSolucionReclamos;
        _motivoSolucionReclamoMap = {
          for (var m in motivosSolucionReclamos) m.id!: m.valor!
        };
      });
    } catch (e) {
      final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
      Messages.showMessage(context, '$errorMessage', isError: true);
    }
  }

  Future<void> _fetchParentescos() async {
    if (_token == null) return;
    try {
      final parentescos =
          await _detalleParametroService.getParentescos(_token!);
      setState(() {
        _parentescos = parentescos;
        _parentescosMap = {for (var m in parentescos) m.id!: m.valor!};
      });
    } catch (e) {
      final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
      Messages.showMessage(context, '$errorMessage', isError: true);
    }
  }

  Future<void> _respondReclamo(AsignarSolucionDto asignarSolucion) async {
    if (_token == null) return;
    LoadingDialog.show(context, message: 'Asignando solución...');
    GenericResponseDto<void> response;
    try {
      response = await _reclamoService.respondReclamo(asignarSolucion, _token!);
      LoadingDialog.hide(context);
      _fetchAllReclamos(page: _currentPage, size: _rowsPerPage);
      Messages.showMessage(context, response.message);
    } catch (e) {
      LoadingDialog.hide(context);
      final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
      Messages.showMessage(context, '$errorMessage', isError: true);
    }
  }

  Uint8List _decodeBase64(String base64String) {
    return base64Decode(base64String);
  }

// Mostrar imagen desde base64
  Widget _buildImageFromBase64(String base64Image) {
    return InteractiveViewer(
      minScale: 0.5, // Escala mínima del zoom
      maxScale: 4.0, // Escala máxima del zoom
      child: Image.memory(
        _decodeBase64(base64Image),
        fit: BoxFit.contain, // Para que la imagen se ajuste mejor al contenedor
      ),
    );
  }

// Guardar PDF desde base64 a un archivo
  Future<File> _savePdfToFile(String base64Pdf) async {
    final directory = await getTemporaryDirectory();
    final file = File('${directory.path}/temp.pdf');
    final bytes = base64Decode(base64Pdf);
    await file.writeAsBytes(bytes);
    return file;
  }

  Widget _buildPdfFromBase64(String base64Pdf) {
    return FutureBuilder<File>(
      future: _savePdfToFile(base64Pdf),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        } else if (snapshot.hasError) {
          return Center(child: Text('Error loading PDF'));
        } else if (snapshot.hasData) {
          return PDFView(
            filePath: snapshot.data!.path,
          );
        } else {
          return Center(child: Text('No PDF available'));
        }
      },
    );
  }

// Mostrar modal con contenido
  void _showModal(BuildContext context, Widget content) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16.0),
          ),
          child: Stack(
            children: [
              Container(
                width: double.infinity,
                height: double.infinity,
                padding: EdgeInsets.all(16.0),
                child: content,
              ),
              Positioned(
                right: 0.0,
                top: 0.0,
                child: IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () {
                    Navigator.of(context).pop(); // Cierra el modal
                  },
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void _showDetails(Reclamo reclamo) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16.0),
          ),
          title: Text('Detalles',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
          content: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildDetailRow('ID:', reclamo.id.toString()),
                _buildDetailRow('Num. Contrato:', reclamo.numContrato!),
                _buildDetailRow('Comentario:', reclamo.comentario!),
                _buildDetailRow('Estado:', reclamo.estado!),
                _buildDetailRow('Solución:',
                    _solucionReclamoMap[reclamo.solucionId] ?? 'Sin solución'),
                _buildDetailRow('Motivo:',
                    _motivoSolucionReclamoMap[reclamo.motivoSolucionId] ?? ''),
                if (reclamo.formularioReclamo != null) ...[
                  _buildDetailRowWithIcon(
                    'Form de Creación:',
                    Icons.picture_as_pdf,
                    () {
                      _showModal(context,
                          _buildPdfFromBase64(reclamo.formularioReclamo!));
                    },
                    reclamo.formularioReclamo, // Base64 del PDF para descargar
                    'Formulario_de_Creacion.pdf', // Nombre del archivo),),
                  )
                ],
                if (roles.contains(Constants.roleAdmin) ||
                    roles.contains(Constants.roleAtencionAlUsuario)) ...[
                  _buildDetailRow(
                      'Cédula cliente:', reclamo.identificacionCliente ?? ''),
                  _buildDetailRow(
                      'Nombres cliente:', reclamo.nombresCliente ?? ''),
                  _buildDetailRow(
                      'Apellidos cliente:', reclamo.apellidosCliente ?? ''),
                  _buildDetailRow(
                      'Dirección Cliente:', reclamo.direccionCliente ?? ''),
                  _buildDetailRow(
                      'Celular Cliente:', reclamo.telefonoCliente ?? ''),
                  _buildDetailRow('Email Cliente:', reclamo.emailCliente ?? ''),
                  _buildDetailRow('Cédula del Representante:',
                      reclamo.identificacionRepresentante ?? ''),
                  _buildDetailRow('Nombre del Representante:',
                      reclamo.nombreRepresentante ?? ''),
                  _buildDetailRow('Dirección del Representante:',
                      reclamo.direccionRepresentante ?? ''),
                  _buildDetailRow('Celular del Representante:',
                      reclamo.telefonoRepresentante ?? ''),
                  _buildDetailRow('Parentesco:',
                      _parentescosMap[reclamo.parentescoId] ?? ''),
                  _buildDetailRow(
                      'Numero Trámite:', reclamo.numeroTramite ?? 'N/A'),
                  _buildDetailRow(
                      'Período Inicial:', reclamo.periodoInicial.toString()),
                  _buildDetailRow(
                      'Período final:', reclamo.periodoFinal.toString()),
                ],
                if (reclamo.copiaDeCedula != null) ...[
                  _buildDetailRowWithIcon(
                    'Copia de Cédula:',
                    Icons.picture_as_pdf,
                    () {
                      _showModal(
                          context, _buildPdfFromBase64(reclamo.copiaDeCedula!));
                    },
                    reclamo.copiaDeCedula, // Base64 del PDF para descargar
                    'Copia_de_Cedula.pdf', // Nombre del archivo),
                  )
                ],
                if (reclamo.respuestaInteragua != null) ...[
                  _buildDetailRowWithIcon(
                    'Resp de Interagua:',
                    Icons.picture_as_pdf,
                    () {
                      _showModal(context,
                          _buildPdfFromBase64(reclamo.respuestaInteragua!));
                    },
                    reclamo.respuestaInteragua, // Base64 del PDF para descargar
                    'Respuesta_de_Interagua.pdf', // Nombre del archivo),),
                  )
                ],
                if (reclamo.cartaExplicativa != null) ...[
                  _buildDetailRowWithIcon(
                    'Carta Explicativa:',
                    Icons.picture_as_pdf,
                    () {
                      _showModal(context,
                          _buildPdfFromBase64(reclamo.cartaExplicativa!));
                    },
                    reclamo.cartaExplicativa, // Base64 del PDF para descargar
                    'Carta_Explicativa.pdf', // Nombre del archivo),),
                  ),
                ],
                if (reclamo.cartaAutorizacion != null) ...[
                  _buildDetailRowWithIcon(
                    'Carta Autorización:',
                    Icons.picture_as_pdf,
                    () {
                      _showModal(context,
                          _buildPdfFromBase64(reclamo.cartaAutorizacion!));
                    },
                    reclamo.cartaAutorizacion, // Base64 del PDF para descargar
                    'Carta_de:Autorizacion.pdf', // Nombre del archivo),),
                  ),
                ],
                // if (reclamo.foto != null) ...[
                //   _buildDetailRowWithIcon('Imagen:', Icons.image, () {
                //     _showModal(context, _buildImageFromBase64(reclamo.foto!));
                //   }),
                // ],
                _buildDetailRow('Fe. Creación:',
                    reclamo.fechaCreacion?.toString() ?? 'N/A'),
                _buildDetailRow('Fe. Modificación:',
                    reclamo.fechaModificacion?.toString() ?? 'N/A'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('Cerrar',
                  style: TextStyle(
                      color: Colors.blue, fontWeight: FontWeight.bold)),
            ),
          ],
        );
      },
    );
  }

  // Modificar la función para agregar el ícono de descarga
  Widget _buildDetailRowWithIcon(String label, IconData icon,
      VoidCallback onTap, String? base64Pdf, String pdfName) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Row(
        // crossAxisAlignment: CrossAxisAlignment
        //     .start, // Alinea el contenido del texto en la parte superior
        children: [
          Expanded(
            child: RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: '$label ',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                      color: Colors.black,
                    ),
                  ),
                  TextSpan(
                    //text: base64Pdf != null ? 'Disponible' : 'No disponible',
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Row(
            mainAxisSize:
                MainAxisSize.min, // Ajusta el tamaño del Row a los hijos
            children: [
              IconButton(
                icon: Icon(icon),
                onPressed: onTap, // Acción para mostrar el modal con el PDF
              ),
              if (base64Pdf !=
                  null) // Mostrar el botón solo si hay un PDF cargado
                IconButton(
                  icon: Icon(Icons.download), // Ícono de descarga
                  onPressed: () {
                    _downloadPdfFile(base64Pdf, pdfName); // Descargar el PDF
                  },
                ),
            ],
          ),
        ],
      ),
    );
  }

  Future<void> _downloadPdfFile(String base64String, String fileName) async {
    try {
      // Decodificar base64 a bytes
      final decodedBytes = base64Decode(base64String);

      // Obtener el directorio de descargas
      final directory = await getExternalStorageDirectory();
      if (directory == null) {
        throw Exception(
            "No se pudo obtener el directorio de almacenamiento externo.");
      }

      final downloadsDirectory = Directory('${directory.path}/Download');
      if (!await downloadsDirectory.exists()) {
        await downloadsDirectory.create(recursive: true);
      }

      // Ruta completa para guardar el archivo
      final filePath = '${downloadsDirectory.path}/$fileName';
      final file = File(filePath);

      // Escribir los bytes en el archivo
      await file.writeAsBytes(decodedBytes);

      // Mostrar mensaje de éxito
      Messages.showMessage(
        context,
        'El archivo PDF se ha descargado en: $filePath',
      );
    } catch (e) {
      // Manejar cualquier error que ocurra
      Messages.showMessage(
        context,
        'Error al descargar el archivo: $e',
        isError: true,
      );
    }
  }

  Widget _buildDetailRow(String title, String value) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(
              text: '$title ',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16,
                color: Colors.black,
              ),
            ),
            TextSpan(
              text: value,
              style: TextStyle(
                fontSize: 16,
                color: Colors.black,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _filterReclamos() {
    final query = _searchController.text.toLowerCase();
    setState(() {
      _filteredReclamos = _reclamos.where((reclamo) {
        return reclamo.numContrato!.toLowerCase().contains(query) ||
            reclamo.id.toString().contains(query);
      }).toList();
      _source = ReclamoDataSource(
        context: context,
        reclamos: _filteredReclamos,
        onDelete: _deleteReclamo,
        onEdit: (Reclamo reclamo) {
          // TODO: Implement edit functionality
        },
        onView: _showDetails,
        roles: roles,
        solucionReclamos: _solucionReclamos,
        motivosSolucionReclamos: _motivosSolucionReclamos,
        onRespond: _respondReclamo,
        totalRecords: _totalRecords,
        isFilter: true,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: <Widget>[
          Row(
            children: [
              Expanded(
                child: TextField(
                  controller: _searchController,
                  decoration: InputDecoration(
                    hintText: 'Buscar por ID o Número de Contrato',
                    border: OutlineInputBorder(),
                    suffixIcon: Icon(Icons.search),
                  ),
                  onChanged: (value) => _filterReclamos(),
                ),
              ),
            ],
          ),
          SizedBox(height: 10),
          Expanded(
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: PaginatedDataTable(
                  rowsPerPage: _rowsPerPage,
                  availableRowsPerPage: const [5, 10, 15],
                  onRowsPerPageChanged: (rowsPerPage) {
                    setState(() {
                      _rowsPerPage = rowsPerPage ?? 5;
                      _currentPage = 1; // Reinicia a la primera página
                    });
                    _fetchAllReclamos(page: _currentPage, size: _rowsPerPage);
                  },
                  onPageChanged: (page) {
                    final newPage = page ~/ _rowsPerPage + 1;
                    if (newPage != _currentPage) {
                      setState(() {
                        _currentPage = newPage;
                      });
                      _fetchAllReclamos(page: _currentPage, size: _rowsPerPage);
                    }
                  },
                  columns: [
                    DataColumn(label: Center(child: Text('ID'))),
                    DataColumn(
                        label: Center(child: Text('Número del Contrato'))),
                    DataColumn(label: Center(child: Text('Estado'))),
                    DataColumn(label: Center(child: Text('Acciones'))),
                  ],
                  source: _source
                  // ReclamoDataSource(
                  //   context: context,
                  //   reclamos: _filteredReclamos,
                  //   onDelete: _deleteReclamo,
                  //   onEdit: (Reclamo reclamo) {
                  //     // TODO: Implement edit functionality
                  //   },
                  //   onView: _showDetails,
                  //   roles: roles,
                  //   solucionReclamos: _solucionReclamos,
                  //   motivosSolucionReclamos: _motivosSolucionReclamos,
                  //   onRespond: _respondReclamo,
                  //   totalRecords: _totalRecords,
                  // ),
                  ),
            ),
          ),
        ],
      ),
    );
  }
}

class ReclamoDataSource extends DataTableSource {
  final BuildContext context;
  final List<Reclamo> reclamos;
  final Function(int) onDelete;
  final Function(Reclamo) onEdit;
  final Function(Reclamo) onView;
  final Function(AsignarSolucionDto)? onRespond;
  final List<DetalleParametro> solucionReclamos;
  final List<DetalleParametro> motivosSolucionReclamos;
  final List<String> roles;
  final int totalRecords;
  bool isFilter;

  ReclamoDataSource({
    required this.context,
    required this.reclamos,
    required this.onDelete,
    required this.onEdit,
    required this.onView,
    required this.roles,
    required this.solucionReclamos,
    required this.motivosSolucionReclamos,
    this.onRespond,
    required this.totalRecords,
    this.isFilter = false,
  });
  List<Widget> _buildActionButtons(Reclamo reclamo) {
    List<Widget> buttons = [];
    buttons.add(
      IconButton(
        icon: Icon(Icons.visibility),
        onPressed: () => onView!(reclamo),
      ),
    );
    // if (roles.contains(Constants.roleAdmin) ||
    //     roles.contains(Constants.roleAtencionAlUsuario)) {
    //   if (reclamo.estado == 'Activo') {
    //     buttons.add(
    //       IconButton(
    //         icon: Icon(Icons.edit),
    //         onPressed: () => onEdit!(reclamo),
    //       ),
    //     );
    //   }
    // }
    if (roles.contains(Constants.roleAdmin) ||
        roles.contains(Constants.roleAtencionAlUsuario)) {
      if (reclamo.estado != 'Inactivo') {
        buttons.add(
          IconButton(
            icon: Icon(Icons.delete),
            onPressed: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Text('Confirmación'),
                    content: Text(
                        '¿Estás seguro de que quieres eliminar este Reclamo?'),
                    actions: <Widget>[
                      TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text('Cancelar'),
                      ),
                      TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                          onDelete!(reclamo.id!);
                        },
                        child: Text('Eliminar'),
                      ),
                    ],
                  );
                },
              );
            },
          ),
        );
      }
    }

    if (roles.contains(Constants.roleAdmin) ||
        roles.contains(Constants.roleAtencionAlUsuario)) {
      if (reclamo.estado != 'Inactivo') {
        buttons.add(IconButton(
          icon: Icon(Icons.reply),
          onPressed: () => _showRespondDialog(context, reclamo),
        ));
      }
    }
    return buttons;
  }

  Future<Map<String, String>?> _pickPdfFile() async {
    final file = await FilePicker.platform
        .pickFiles(type: FileType.custom, allowedExtensions: ['pdf']);

    if (file != null && file.files.single.size <= 1048576) {
      // Leer el archivo y convertirlo a base64
      final fileBytes = await File(file.files.single.path!).readAsBytes();
      final base64File = base64Encode(fileBytes);

      // Devolver nombre y base64
      return {
        'base64': base64File,
        'fileName': file.files.single.name,
      };
    } else {
      Messages.showMessage(
        context,
        'El archivo debe tener un peso menor al de 1MB.',
        isError: true,
      );
      return null;
    }
  }

  void _showRespondDialog(BuildContext context, Reclamo reclamo) {
    int? selectedSolucion;
    int? selectedMotivo;
    bool showMotivoField = false;
    bool showFileField = false; // Para mostrar el campo de carga de PDF
    String? pdfBase64; // Aquí se almacenará el PDF en base64
    String? pdfFileName; // Nombre del archivo PDF cargado

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16.0),
          ),
          title: Text('Responder Reclamo'),
          content: StatefulBuilder(
            builder: (context, setState) {
              return Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  DropdownButtonFormField<int>(
                    decoration: InputDecoration(
                      labelText: 'Solución',
                      border: OutlineInputBorder(),
                    ),
                    isExpanded: true,
                    items: solucionReclamos.map((solucion) {
                      return DropdownMenuItem<int>(
                        value: solucion.id,
                        child: Text(solucion.valor!),
                      );
                    }).toList(),
                    onChanged: (solucionId) {
                      setState(() {
                        selectedSolucion = solucionId;
                        final solucion = solucionReclamos
                            .firstWhere((s) => s.id == solucionId);
                        showMotivoField = solucion.valor != 'Ingresado';
                        showFileField = solucion.valor ==
                            'Ingresado'; // Mostrar campo de archivo si es "Ingresado"
                      });
                    },
                    value: selectedSolucion,
                  ),
                  SizedBox(height: 10),
                  if (showMotivoField)
                    DropdownButtonFormField<int>(
                      decoration: InputDecoration(
                        labelText: 'Motivo de la Solución',
                        border: OutlineInputBorder(),
                      ),
                      isExpanded: true,
                      items: motivosSolucionReclamos.map((motivo) {
                        return DropdownMenuItem<int>(
                          value: motivo.id,
                          child: Text(motivo.valor!),
                        );
                      }).toList(),
                      onChanged: (motivoId) {
                        setState(() {
                          selectedMotivo = motivoId;
                        });
                      },
                      value: selectedMotivo,
                    ),
                  SizedBox(height: 10),
                  // Mostrar campo de carga de archivo PDF si la solución es "Ingresado"
                  if (showFileField)
                    Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: TextFormField(
                                decoration: InputDecoration(
                                  labelText: 'Cargar Formulario de Creación',
                                  border: OutlineInputBorder(),
                                ),
                                readOnly: true,
                                controller: TextEditingController(
                                  text: pdfFileName ??
                                      '', // Mostrar el nombre del archivo
                                ),
                              ),
                            ),
                            // Si el archivo está cargado, mostrar "X" para eliminarlo
                            if (pdfBase64 != null)
                              IconButton(
                                icon: Icon(Icons.clear), // X para eliminar
                                onPressed: () {
                                  setState(() {
                                    pdfBase64 = null;
                                    pdfFileName = null;
                                  });
                                },
                              )
                            // Si no hay archivo cargado, mostrar el ícono de adjuntar
                            else
                              IconButton(
                                icon: Icon(Icons
                                    .attach_file), // Clip para adjuntar archivo
                                onPressed: () async {
                                  final result = await _pickPdfFile();
                                  if (result != null) {
                                    setState(() {
                                      pdfBase64 = result['base64'];
                                      pdfFileName = result['fileName'];
                                    });
                                  }
                                },
                              ),
                          ],
                        ),
                      ],
                    ),
                ],
              );
            },
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('Cancelar'),
            ),
            TextButton(
              onPressed: () {
                if (selectedSolucion == null) {
                  Messages.showMessage(
                    context,
                    'Por favor, selecciona una solución',
                    isError: true,
                  );
                } else if (showMotivoField && selectedMotivo == null) {
                  Messages.showMessage(
                    context,
                    'Por favor, selecciona un motivo',
                    isError: true,
                  );
                } else if (showFileField && pdfBase64 == null) {
                  Messages.showMessage(
                    context,
                    'Por favor, carga un archivo PDF',
                    isError: true,
                  );
                } else {
                  Navigator.of(context).pop();
                  onRespond!(
                    AsignarSolucionDto(
                      reclamoId: reclamo.id!,
                      solucionId: selectedSolucion!,
                      motivoSolucionId: selectedMotivo ?? null,
                      form:
                          pdfBase64, // Enviar el archivo en base64 en el campo form
                    ),
                  );
                }
              },
              child: Text('Enviar'),
            ),
          ],
        );
      },
    );
  }

  @override
  DataRow? getRow(int index) {
    final d;
    if (!reclamos.isEmpty || index < reclamos.length) {
      if (isFilter) {
        // Aquí puedes aplicar una lógica distinta si la propiedad isFilter es true
        if (reclamos.isEmpty || index >= reclamos.length) {
          return DataRow(cells: [
            DataCell(Center(child: Text(''))),
            DataCell
                .empty, // Celdas vacías para mantener la estructura de la tabla
            DataCell.empty,
            DataCell.empty,
          ]);
        } else {
          final d = reclamos[index];
        }
      }
      int adjustedIndex = index % reclamos.length;
      d = reclamos[adjustedIndex];
    } else {
      return DataRow(cells: [
        DataCell(Center(child: Text(''))),
        DataCell.empty, // Celdas vacías para mantener la estructura de la tabla
        DataCell.empty,
        DataCell.empty,
      ]);
    }
    int adjustedIndex = index % reclamos.length;
    if (adjustedIndex >= reclamos.length) return null;
    final reclamo = reclamos[adjustedIndex];
    return DataRow(
      cells: [
        DataCell(Center(child: Text(reclamo.id.toString()))),
        DataCell(Center(child: Text(reclamo.numContrato.toString()))),
        DataCell(Center(child: Text(reclamo.estado!))),
        DataCell(
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: _buildActionButtons(reclamo)),
        ),
      ],
    );
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => totalRecords;

  @override
  int get selectedRowCount => 0;
}
