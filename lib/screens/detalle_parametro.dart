import 'package:emapagep/config/constants.dart';
import 'package:emapagep/dto/generic_response_dto.dart';
import 'package:emapagep/models/parametro.dart';
import 'package:emapagep/services/parametro_service.dart';
import 'package:emapagep/widgets/loading_dialog.dart';
import 'package:flutter/material.dart';
import 'package:emapagep/models/detalle_parametro.dart';
import 'package:emapagep/services/detalle_parametro_service.dart';
import 'package:emapagep/widgets/messages.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:jwt_decoder/jwt_decoder.dart';

class DetalleParametroScreen extends StatefulWidget {
  @override
  _DetalleParametroScreenState createState() => _DetalleParametroScreenState();
}

class _DetalleParametroScreenState extends State<DetalleParametroScreen> {
  final TextEditingController _nombreController = TextEditingController();
  final TextEditingController _descripcionController = TextEditingController();
  final TextEditingController _searchController = TextEditingController();
  final TextEditingController _valorController = TextEditingController();
  final DetalleParametroService _detalleParametroService =
      DetalleParametroService();
  final ParametroService _parametroService = ParametroService();

  List<DetalleParametro> _detalleParametros = [];
  List<DetalleParametro> _filteredDetalleParametros = [];
  DetalleParametro? _selectedDetalleParametro;
  bool _isAddingOrUpdating = false;
  List<Parametro> _parametros = [];
  int? _selectedParametro;
  final storage = FlutterSecureStorage();
  List<String> roles = [];
  String? _token;
  final _formKey = GlobalKey<FormState>();
  Map<int, String> _parametroMap = {};
  int _currentPage = 1;
  int _totalPages = 1;
  int _totalRecords = 0;
  int _rowsPerPage = 5;
  late DetalleParametroDataSource _source;
  @override
  void initState() {
    super.initState();
    _loadRolesAndToken();
    _source = DetalleParametroDataSource(
      context: context,
      detalleParametros: [], // Lista vacía por defecto
      onDelete: _deleteDetalleParametro,
      onEdit: (detalleParametro) {
        setState(() {
          _selectedDetalleParametro = detalleParametro;
          _nombreController.text = detalleParametro.nombre;
          _descripcionController.text = detalleParametro.descripcion ?? '';
          _valorController.text = detalleParametro.valor ?? '';
          _selectedParametro = detalleParametro.parametroId;
          _isAddingOrUpdating = true;
        });
        _showForm(); // Muestra el formulario de actualización
      },
      onView: _showDetails,
      roles: roles,
      totalRecords: _totalRecords,
      isFilter: false,
    );
  }

  Future<void> _loadRolesAndToken() async {
    String? token = await storage.read(key: 'auth_token');
    if (token != null) {
      Map<String, dynamic> decodedToken = JwtDecoder.decode(token);
      setState(() {
        roles = List<String>.from(
            decodedToken['roles'].map((role) => role['name']));
        _token = token;
      });
      _fetchParametros();
      _fetchDetalleParametros(page: _currentPage, size: _rowsPerPage);
    }
  }

  Future<void> _fetchDetalleParametros({int page = 1, int size = 5}) async {
    if (_token == null) return;
    LoadingDialog.show(context, message: 'Cargando Detalles Parámetros...');
    try {
      final response = await _detalleParametroService.getAllDetalleParametros(
        _token!,
        page: page,
        size: size,
      );
      final detalleParametros = response.payload;

      setState(() {
        _detalleParametros = detalleParametros;
        _filteredDetalleParametros = detalleParametros;
        _totalRecords = response.totalRecords ?? 0;
        _totalPages = response.totalPages ?? 1;
        _currentPage = page; // Actualiza la página actual
        _rowsPerPage = size; // Actualiza el tamaño de página
        _source = DetalleParametroDataSource(
          context: context,
          detalleParametros: _detalleParametros,
          onDelete: _deleteDetalleParametro,
          onEdit: (detalleParametro) {
            setState(() {
              _selectedDetalleParametro = detalleParametro;
              _nombreController.text = detalleParametro.nombre;
              _descripcionController.text = detalleParametro.descripcion ?? '';
              _valorController.text = detalleParametro.valor ?? '';
              _selectedParametro = detalleParametro.parametroId;
              _isAddingOrUpdating = true;
            });
            _showForm();
          },
          onView: _showDetails,
          roles: roles,
          totalRecords: _totalRecords,
          isFilter: false,
        );
        LoadingDialog.hide(context);
      });
    } catch (e) {
      LoadingDialog.hide(context);
      final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
      Messages.showMessage(context, 'Error: $errorMessage',
          isError: true, isTop: true);
    }
  }

  Future<void> _fetchParametros() async {
    if (_token == null) return;
    try {
      final response = await _parametroService.getAllParametros(_token!);
      final parametros = response.payload;
      setState(() {
        _parametros = parametros;
        _parametroMap = {for (var s in parametros) s.id!: s.nombre};
      });
    } catch (e) {
      final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
      Messages.showMessage(context, 'Error: $errorMessage',
          isError: true, isTop: true);
    }
  }

  void _filterDetalleParametros() {
    final query = _searchController.text.toLowerCase();
    setState(() {
      _filteredDetalleParametros = _detalleParametros.where((detalleParametro) {
        return detalleParametro.nombre.toLowerCase().contains(query) ||
            detalleParametro.id.toString().contains(query);
      }).toList();

      // Actualizar correctamente la fuente de datos con el nuevo isFilter
      _source = DetalleParametroDataSource(
        context: context,
        detalleParametros: _filteredDetalleParametros,
        onDelete: _deleteDetalleParametro,
        onEdit: (DetalleParametro detalleParametro) {
          setState(() {
            _selectedDetalleParametro = detalleParametro;
            _nombreController.text = detalleParametro.nombre;
            _descripcionController.text = detalleParametro.descripcion ?? '';
            _valorController.text = detalleParametro.valor ?? '';
            _selectedParametro = detalleParametro.parametroId;
            _isAddingOrUpdating = true;
          });
          _showForm(); // Muestra el formulario de actualización
        },
        onView: _showDetails,
        roles: roles,
        totalRecords: _totalRecords,
        isFilter:
            true, // Aquí se asegura que el nuevo source tiene isFilter en true
      );

      // Verificar si se asigna correctamente el nuevo 'source'
      print("Nuevo DataSource creado con isFilter: ${_source.isFilter}");
    });
  }

  Future<void> _saveDetalleParametro() async {
    if (_token == null) return;
    if (_formKey.currentState?.validate() ?? false) {
      final nombre = _nombreController.text;
      final descripcion = _descripcionController.text;
      final valor = _valorController.text;
      if (nombre.isEmpty || _selectedParametro == null) return;

      final detalle = DetalleParametro(
        id: _selectedDetalleParametro?.id,
        nombre: nombre,
        estado: 'Activo',
        descripcion: descripcion,
        valor: valor,
        parametroId: _selectedParametro!,
      );

      try {
        GenericResponseDto<void> response;
        if (_selectedDetalleParametro == null) {
          LoadingDialog.show(context, message: 'Creando Detalle Parámetro...');
          response = await _detalleParametroService.createDetalleParametro(
              detalle, _token!);
          LoadingDialog.hide(context);
        } else {
          LoadingDialog.show(context,
              message: 'Actualizando Detalle Parámetro...');

          response = await _detalleParametroService.updateDetalleParametro(
              detalle.id!, detalle, _token!);
          LoadingDialog.hide(context);
        }
        Messages.showMessage(context, response.message);
        Navigator.of(context).pop(); // Cierra el modal
        _fetchDetalleParametros(page: _currentPage, size: _rowsPerPage);
        setState(() {
          _isAddingOrUpdating = false;
          _selectedDetalleParametro = null;
          _selectedParametro = null;
        });
      } catch (e) {
        LoadingDialog.hide(context);
        final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
        Messages.showMessage(context, 'Error: $errorMessage',
            isError: true, isTop: true);
      }
    }
  }

  Future<void> _deleteDetalleParametro(int id) async {
    GenericResponseDto<void> response;
    LoadingDialog.show(context, message: 'Eliminando Detalle Parámetro...');
    try {
      response =
          await _detalleParametroService.deleteDetalleParametro(id, _token!);
      LoadingDialog.hide(context);
      _fetchDetalleParametros(page: _currentPage, size: _rowsPerPage);
      Messages.showMessage(context, response.message);
    } catch (e) {
      LoadingDialog.hide(context);
      final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
      Messages.showMessage(context, 'Error: $errorMessage',
          isError: true, isTop: true);
    }
  }

  void _showDetails(DetalleParametro detalleParametro) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16.0), // Bordes redondeados
          ),
          title: Text('Detalles',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
          content: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildDetailRow('ID:', detalleParametro.id.toString()),
                _buildDetailRow('Parametro Padre:',
                    _parametroMap[detalleParametro.parametroId] ?? ''),
                _buildDetailRow('Nombre:', detalleParametro.nombre),
                _buildDetailRow(
                    'Descripción:', detalleParametro.descripcion ?? 'N/A'),
                _buildDetailRow('Valor:', detalleParametro.valor ?? 'N/A'),
                _buildDetailRow('Estado:', detalleParametro.estado),
                _buildDetailRow('Fe. Creación:',
                    detalleParametro.fechaCreacion?.toString() ?? 'N/A'),
                _buildDetailRow('Fe. Modificación:',
                    detalleParametro.fechaModificacion?.toString() ?? 'N/A'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('Cerrar',
                  style: TextStyle(
                      color: Colors.blue, fontWeight: FontWeight.bold)),
            ),
          ],
        );
      },
    );
  }

  Widget _buildDetailRow(String title, String value) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(
              text: '$title ',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16,
                color: Colors.black,
              ),
            ),
            TextSpan(
              text: value,
              style: TextStyle(
                fontSize: 16,
                color: Colors.black,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _showForm() {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return Padding(
          padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      _selectedDetalleParametro == null
                          ? 'Crear Detalle Parámetro'
                          : 'Actualizar Detalle Párametro',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 10),
                    TextFormField(
                      controller: _nombreController,
                      maxLength: 50,
                      decoration: InputDecoration(
                        labelText: 'Nombre',
                        border: OutlineInputBorder(),
                        counterText: '',
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Por favor ingresa el nombre del detalle';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 10),
                    TextFormField(
                      controller: _descripcionController,
                      maxLength: 100,
                      decoration: InputDecoration(
                        labelText: 'Descripción',
                        border: OutlineInputBorder(),
                        counterText: '',
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Por favor ingresa la descripción del detalle';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 10),
                    TextFormField(
                      controller: _valorController,
                      maxLength: 30,
                      decoration: InputDecoration(
                          labelText: 'Valor',
                          border: OutlineInputBorder(),
                          counterText: ''),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Por favor ingresa el valor del detalle';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 10),
                    DropdownButtonFormField<int>(
                      decoration: InputDecoration(
                        labelText: 'Parametro',
                        border: OutlineInputBorder(),
                      ),
                      isExpanded: true,
                      items: _parametros.map((parametro) {
                        return DropdownMenuItem<int>(
                          value: parametro.id,
                          child: Text(parametro.nombre),
                        );
                      }).toList(),
                      onChanged: (parametroId) {
                        setState(() {
                          _selectedParametro = parametroId;
                        });
                      },
                      validator: (value) {
                        if (value == null || _parametros.isEmpty) {
                          return 'Por favor selecciona un parámetro';
                        }
                        return null;
                      },
                      value: _selectedParametro,
                    ),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        ElevatedButton(
                          onPressed: () {
                            _saveDetalleParametro();
                          },
                          child: Text('Guardar'),
                        ),
                        ElevatedButton(
                          onPressed: () {
                            Navigator.of(context).pop(); // Cierra el modal
                            setState(() {
                              _isAddingOrUpdating = false;
                              _nombreController.clear();
                              _descripcionController.clear();
                              _valorController.clear();
                              _selectedDetalleParametro = null;
                              _selectedParametro = null;
                            });
                          },
                          child: Text('Cancelar'),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: roles.contains(Constants.roleAdmin)
          ? FloatingActionButton(
              onPressed: () {
                setState(() {
                  _isAddingOrUpdating = true;
                  _selectedDetalleParametro = null;
                  _nombreController.clear();
                  _descripcionController.clear();
                  _valorController.clear();
                  _selectedParametro = null;
                  _showForm(); // Muestra el formulario
                });
              },
              child: Icon(Icons.add),
              shape: CircleBorder(), // Asegura que el botón sea redondo
              backgroundColor: Colors.blue, // Color de fondo del botón
            )
          : null,
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: _searchController,
                    decoration: InputDecoration(
                      hintText: 'Buscar por ID o Nombre',
                      border: OutlineInputBorder(),
                      suffixIcon: Icon(Icons.search),
                    ),
                    onChanged: (value) => _filterDetalleParametros(),
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: PaginatedDataTable(
                    rowsPerPage: _rowsPerPage,
                    availableRowsPerPage: const [5, 10, 15],
                    onRowsPerPageChanged: (rowsPerPage) {
                      setState(() {
                        _rowsPerPage = rowsPerPage ?? 5;
                        _currentPage = 1; // Reinicia a la primera página
                      });
                      _fetchDetalleParametros(
                          page: _currentPage, size: _rowsPerPage);
                    },
                    onPageChanged: (page) {
                      final newPage = page ~/ _rowsPerPage + 1;
                      if (newPage != _currentPage) {
                        setState(() {
                          _currentPage = newPage;
                        });
                        _fetchDetalleParametros(
                            page: _currentPage, size: _rowsPerPage);
                      }
                    },
                    columns: [
                      DataColumn(label: Center(child: Text('ID'))),
                      DataColumn(label: Center(child: Text('Nombre'))),
                      DataColumn(label: Center(child: Text('Estado'))),
                      DataColumn(label: Center(child: Text('Acciones'))),
                    ],
                    source: _source

                    // DetalleParametroDataSource(
                    //   context: context,
                    //   detalleParametros: _filteredDetalleParametros,
                    //   onDelete: _deleteDetalleParametro,
                    //   onEdit: (DetalleParametro detalleParametro) {
                    //     setState(() {
                    //       _selectedDetalleParametro = detalleParametro;
                    //       _nombreController.text = detalleParametro.nombre;
                    //       _descripcionController.text =
                    //           detalleParametro.descripcion ?? '';
                    //       _valorController.text = detalleParametro.valor ?? '';
                    //       _selectedParametro = detalleParametro.parametroId;
                    //       _isAddingOrUpdating = true;
                    //     });
                    //     _showForm(); // Muestra el formulario de actualización
                    //   },
                    //   onView: _showDetails,
                    //   roles: roles,
                    //   totalRecords: _totalRecords,
                    // ),
                    ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class DetalleParametroDataSource extends DataTableSource {
  final BuildContext context;
  final List<DetalleParametro> detalleParametros;
  final Function(int) onDelete;
  final Function(DetalleParametro) onEdit;
  final Function(DetalleParametro) onView;
  final List<String> roles;
  final int totalRecords;
  bool isFilter;

  DetalleParametroDataSource({
    required this.context,
    required this.detalleParametros,
    required this.onDelete,
    required this.onEdit,
    required this.onView,
    required this.roles,
    required this.totalRecords,
    this.isFilter = false,
  });

  @override
  DataRow? getRow(int index) {
    final d;
    if (!detalleParametros.isEmpty || index < detalleParametros.length) {
      if (isFilter) {
        // Aquí puedes aplicar una lógica distinta si la propiedad isFilter es true
        if (detalleParametros.isEmpty || index >= detalleParametros.length) {
          return DataRow(cells: [
            DataCell(Center(child: Text(''))),
            DataCell
                .empty, // Celdas vacías para mantener la estructura de la tabla
            DataCell.empty,
            DataCell.empty,
          ]);
        } else {
          final d = detalleParametros[index];
        }
      }
      int adjustedIndex = index % detalleParametros.length;
      d = detalleParametros[adjustedIndex];
    } else {
      return DataRow(cells: [
        DataCell(Center(child: Text(''))),
        DataCell.empty, // Celdas vacías para mantener la estructura de la tabla
        DataCell.empty,
        DataCell.empty,
      ]);
    }
    return DataRow(cells: [
      DataCell(Center(child: Text(d.id.toString()))),
      DataCell(Center(child: Text(d.nombre))),
      DataCell(Center(child: Text(d.estado))),
      DataCell(Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: _buildActionButtons(d),
      )),
    ]);
  }

  List<Widget> _buildActionButtons(DetalleParametro d) {
    List<Widget> buttons = [];

    // Mostrar el botón de detalles a todos los roles
    buttons.add(
      IconButton(
        icon: Icon(Icons.visibility),
        onPressed: () => onView(d),
      ),
    );

    // Mostrar el botón de edición solo para ADMIN
    if (roles.contains(Constants.roleAdmin)) {
      buttons.add(
        IconButton(
          icon: Icon(Icons.edit),
          onPressed: () => onEdit(d),
        ),
      );
    }

    // Mostrar el botón de eliminación solo para ADMIN
    if (roles.contains(Constants.roleAdmin)) {
      buttons.add(
        IconButton(
          icon: Icon(Icons.delete),
          onPressed: () {
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text('Confirmación'),
                  content: Text(
                      '¿Estás seguro de que quieres eliminar este Detalle?'),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text('Cancelar'),
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                        onDelete(d.id!);
                      },
                      child: Text('Eliminar'),
                    ),
                  ],
                );
              },
            );
          },
        ),
      );
    }

    return buttons;
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => totalRecords;

  @override
  int get selectedRowCount => 0;

  @override
  void selectAll(bool checked) {}
}
