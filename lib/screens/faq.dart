import 'package:flutter/material.dart';

class FaqScreen extends StatelessWidget {
  final List<Map<String, String>> faqs = [
    {
      'question': '¿Qué es un reclamo?',
      'answer':
          'Un reclamo es una solicitud formal enviada por un cliente para notificar un problema o inconformidad con un servicio o producto. En nuestra aplicación, puedes crear un reclamo para que nuestro equipo de atención al cliente lo revise y procese.',
      'image': 'assets/reclamo.png',
    },
    {
      'question': '¿Cómo puedo crear un reclamo desde la aplicación?',
      'answer':
          'Para crear un reclamo, inicia sesión en tu cuenta, selecciona la opción de "Reclamos" y luego en "Crear Reclamo" en el menú principal, llena el formulario con los detalles de tu problema y envíalo.',
      'image': 'assets/create_reclamo.png',
    },
    {
      'question':
          '¿Cómo sé si mi reclamo fue recibido y está siendo procesado?',
      'answer':
          'Después de enviar tu reclamo, nuestro equipo de atención al cliente recibirá una notificación por correo electrónico para comenzar el proceso de evaluación. Tu reclamo será revisado, validado y gestionado para ofrecerte una solución lo más pronto posible.',
      'image': 'assets/reclamo_process.png',
    },
    {
      'question': '¿Qué estados puede tener un reclamo?',
      'answer':
          'Un reclamo puede pasar por varios estados, como "Ingresado"  o "Desestimado". Si el reclamo está en estado "Ingresado", podrás ver las acciones tomadas sobre tu reclamo en la pantalla "Listado de Reclamos" dentro de la aplicación, donde podrás seguir su progreso y ver cualquier actualización relevante.',
      'image': 'assets/status_reclamo.png',
    },
    {
      'question': '¿Cómo puedo actualizar mis datos personales?',
      'answer':
          'Puedes actualizar tu información personal, como nombres, apellidos y dirección, seleccionando el ícono de lápiz que aparece junto a tu nombre de usuario en la barra lateral. Al hacer clic en este ícono, podrás acceder a la opción de editar tu perfil.',
      'image': 'assets/update_profile.png',
    },
    {
      'question': '¿Qué hago si no puedo acceder a mi cuenta?',
      'answer':
          'Si tienes problemas para acceder a tu cuenta, puedes utilizar la opción "Recuperar Contraseña" en la pantalla de inicio de sesión o ponerte en contacto con nuestro equipo de soporte para asistencia adicional.',
      'image': 'assets/candado.png',
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemCount: faqs.length,
        itemBuilder: (context, index) {
          final faq = faqs[index];
          return Card(
            margin: EdgeInsets.all(10),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Encabezado con fondo azul
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.blue, // Fondo azul
                    borderRadius: BorderRadius.vertical(
                      top: Radius.circular(10),
                    ),
                  ),
                  child: Text(
                    faq['question']!,
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      color: Colors.white, // Texto en blanco
                    ),
                  ),
                ),
                // Imagen con bordes
                Padding(
                  padding: EdgeInsets.all(10),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8),
                    child: Image.asset(
                      faq['image']!,
                      fit: BoxFit.contain,
                      width: double.infinity,
                      height: MediaQuery.of(context).size.height * 0.25,
                    ),
                  ),
                ),
                // Respuesta
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  child: Text(
                    faq['answer']!,
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.black, // Texto en negro
                    ),
                  ),
                ),
                SizedBox(height: 10),
              ],
            ),
          );
        },
      ),
    );
  }
}
