import 'dart:convert';
import 'dart:io';
import 'package:emapagep/dto/generic_response_dto.dart';
import 'package:emapagep/dto/edit_profile_dto.dart';
import 'package:emapagep/screens/drawer_options.dart';
import 'package:emapagep/services/usuario_service.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:emapagep/widgets/messages.dart';
import 'package:emapagep/widgets/loading_dialog.dart';

class EditProfileScreen extends StatefulWidget {
  final int? id;
  final String nombres;
  final String apellidos;
  final String nombreUsuario;
  final String telefono;
  final String direccion;
  final String? foto;
  final String token;

  const EditProfileScreen({
    Key? key,
    this.id,
    required this.nombres,
    required this.apellidos,
    required this.nombreUsuario,
    required this.telefono,
    required this.direccion,
    this.foto,
    required this.token,
  }) : super(key: key);

  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  final _formKey = GlobalKey<FormState>();
  late TextEditingController _nombresController;
  late TextEditingController _apellidosController;
  late TextEditingController _nombreUsuarioController;
  late TextEditingController _telefonoController;
  late TextEditingController _direccionController;
  final UsuarioService _usuarioService = UsuarioService();
  String? _fotoBase64;

  @override
  void initState() {
    super.initState();
    _nombresController = TextEditingController(text: widget.nombres);
    _apellidosController = TextEditingController(text: widget.apellidos);
    _nombreUsuarioController =
        TextEditingController(text: widget.nombreUsuario);
    _telefonoController = TextEditingController(text: widget.telefono);
    _direccionController = TextEditingController(text: widget.direccion);
    _fotoBase64 = widget.foto;
  }

  @override
  void dispose() {
    _nombresController.dispose();
    _apellidosController.dispose();
    _nombreUsuarioController.dispose();
    _telefonoController.dispose();
    _direccionController.dispose();
    super.dispose();
  }

  Future<void> _pickImageFile() async {
    final result = await FilePicker.platform.pickFiles(type: FileType.image);
    if (result != null && result.files.single.path != null) {
      final bytes = await File(result.files.single.path!).readAsBytes();
      setState(() {
        _fotoBase64 = base64Encode(bytes);
      });
    }
  }

  void _updateProfile() async {
    if (_formKey.currentState!.validate()) {
      LoadingDialog.show(context, message: 'Actualizando perfil...');
      final usuario = EditProfileDto(
        id: widget.id,
        nombres: _nombresController.text,
        apellidos: _apellidosController.text,
        nombreUsuario: _nombreUsuarioController.text,
        telefono: _telefonoController.text,
        direccion: _direccionController.text,
        foto: _fotoBase64,
      );

      try {
        GenericResponseDto<EditProfileDto> response = await _usuarioService
            .editProfile(widget.id!, usuario, widget.token);
        LoadingDialog.hide(context);
        Messages.showMessage(context, response.message);
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => DrawerOptionsScreen()),
        );
      } catch (e) {
        LoadingDialog.hide(context);
        final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
        Messages.showMessage(context, '$errorMessage', isError: true);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Editar Perfil'),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                GestureDetector(
                  onTap: _pickImageFile,
                  child: CircleAvatar(
                    radius: 50,
                    backgroundImage: _fotoBase64 != null
                        ? MemoryImage(base64Decode(_fotoBase64!))
                        : null,
                    child: _fotoBase64 == null
                        ? Icon(Icons.add_a_photo, size: 50)
                        : null,
                  ),
                ),
                SizedBox(height: 10),
                TextFormField(
                  controller: _nombresController,
                  decoration: InputDecoration(
                    labelText: 'Nombres',
                    border: OutlineInputBorder(),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Por favor ingresa los nombres';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 10),
                TextFormField(
                  controller: _apellidosController,
                  decoration: InputDecoration(
                    labelText: 'Apellidos',
                    border: OutlineInputBorder(),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Por favor ingresa los apellidos';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 10),
                TextFormField(
                  controller: _nombreUsuarioController,
                  decoration: InputDecoration(
                    labelText: 'Nombre de Usuario',
                    border: OutlineInputBorder(),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Por favor ingresa el nombre de usuario';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 10),
                TextFormField(
                  controller: _telefonoController,
                  decoration: InputDecoration(
                    labelText: 'Celular',
                    border: OutlineInputBorder(),
                  ),
                  keyboardType: TextInputType.number,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Por favor ingresa el número de celular';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 10),
                TextFormField(
                  controller: _direccionController,
                  decoration: InputDecoration(
                    labelText: 'Dirección',
                    border: OutlineInputBorder(),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Por favor ingresa la dirección';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 20),
                ElevatedButton(
                  onPressed: _updateProfile,
                  child: Text('Guardar'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
