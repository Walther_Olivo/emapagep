import 'dart:convert';
import 'dart:io';
import 'package:emapagep/dto/generic_response_dto.dart';
import 'package:emapagep/models/detalle_parametro.dart';
import 'package:emapagep/models/medio_promocion.dart';
import 'package:emapagep/models/reclamo.dart';
import 'package:emapagep/models/tipo_reclamo.dart';
import 'package:emapagep/services/detalle_parametro_service.dart';
import 'package:emapagep/services/medio_promocion_service.dart';
import 'package:emapagep/services/reclamo_service.dart';
import 'package:emapagep/services/tipo_reclamo_service.dart';
import 'package:emapagep/widgets/loading_dialog.dart';
import 'package:emapagep/widgets/messages.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:jwt_decoder/jwt_decoder.dart';

class CreateReclamoScreen extends StatefulWidget {
  @override
  _CreateReclamoScreenState createState() => _CreateReclamoScreenState();
}

class _CreateReclamoScreenState extends State<CreateReclamoScreen> {
  bool _isOwner = false;

  final TextEditingController _numContratoController = TextEditingController();
  final TextEditingController _identificacionClienteController =
      TextEditingController();
  final TextEditingController _nombresClienteController =
      TextEditingController();
  final TextEditingController _apellidosClienteController =
      TextEditingController();
  final TextEditingController _direccionClienteController =
      TextEditingController();
  final TextEditingController _telefonoClienteController =
      TextEditingController();
  final TextEditingController _emailClienteController = TextEditingController();
  final TextEditingController _numeroTramiteController =
      TextEditingController();
  final TextEditingController _resolucionTramiteController =
      TextEditingController();
  final TextEditingController _aspectoTramiteController =
      TextEditingController();
  final TextEditingController _subAspectoTramiteController =
      TextEditingController();
  final TextEditingController _motivoTramiteController =
      TextEditingController();
  final TextEditingController _numeroQuejaController = TextEditingController();
  final TextEditingController _horaProvController = TextEditingController();
  final TextEditingController _comentarioController = TextEditingController();

  final TextEditingController _identifRepresentanteController =
      TextEditingController();
  final TextEditingController _nombresRepresentanteController =
      TextEditingController();
  final TextEditingController _direccionRepresentanteController =
      TextEditingController();
  final TextEditingController _telefonoRepresentanteController =
      TextEditingController();
  DateTime? _selectedFechaResolucion;
  final TextEditingController _fechaResolucionController =
      TextEditingController();

  DateTime? _selectedFechaQueja;
  final TextEditingController _fechaQuejaController = TextEditingController();

  DateTime? _selectedPeriodoInicial;
  final TextEditingController _periodoInicialController =
      TextEditingController();

  DateTime? _selectedPeriodoFinal;
  final TextEditingController _periodoFinalController = TextEditingController();

  DateTime? _selectedFechaIngreso;
  final TextEditingController _fechaIngresoController = TextEditingController();

  DateTime? _selectedFechaVencimiento;
  final TextEditingController _fechaVencimientoController =
      TextEditingController();

  final List<TextEditingController> _pdfFileNameControllers = [
    TextEditingController(), // Copia de Cédula
    TextEditingController(), // Respuesta de Interagua
    TextEditingController(), // Carta Explicativa del Reclamo
    TextEditingController(), // Carta de Autorización del Propietario
  ];

  List<File?> _pdfFiles = [null, null, null, null];
  final TextEditingController _imageFileNameController =
      TextEditingController();
  File? _imageFile;

  final DetalleParametroService _parametroService = DetalleParametroService();
  final MedioPromocionService _medioPromocionService = MedioPromocionService();
  final TipoReclamoService _tipoReclamoService = TipoReclamoService();
  final ReclamoService _reclamoService = ReclamoService();

  List<TipoReclamo> _tipoReclamos = [];
  int? _selectedTipoReclamo;
  List<MedioPromocion> _tipoMedioPromocion = [];
  int? _selectedMedioPromocion;
  List<DetalleParametro> _detalleparametrosParentescos = [];
  int? _selectedDetalleParametroParentescos;
  String? _token;
  final storage = FlutterSecureStorage();
  List<String> roles = [];
  int? userId;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _loadRolesAndToken();
  }

  Future<void> _loadRolesAndToken() async {
    String? token = await storage.read(key: 'auth_token');
    if (token != null) {
      Map<String, dynamic> decodedToken = JwtDecoder.decode(token);
      userId = int.parse(decodedToken['sub']);
      setState(() {
        roles = List<String>.from(
            decodedToken['roles'].map((role) => role['name']));
        _token = token;
      });
      _fetchData();
    }
  }

  Future<void> _fetchData() async {
    if (_token == null) return;
    try {
      final responseTypeReclamos =
          await _tipoReclamoService.getAllTipoReclamos(_token!);
      _tipoReclamos = responseTypeReclamos.payload;
      final responseMedioPromocion =
          await _medioPromocionService.getAllMedioPromociones(_token!);
      _tipoMedioPromocion = responseMedioPromocion.payload;

      final responseParentescos =
          await _parametroService.getParentescos(_token!);
      _detalleparametrosParentescos = responseParentescos;
      setState(() {});
    } catch (e) {
      Messages.showMessage(context, 'Failed to load data: $e', isError: true);
    }
  }

  Future<void> _pickPdfFile(int index) async {
    final file = await FilePicker.platform
        .pickFiles(type: FileType.custom, allowedExtensions: ['pdf']);
    if (file != null && file.files.single.size <= 1048576) {
      setState(() {
        _pdfFiles[index] = File(file.files.single.path!);
        _pdfFileNameControllers[index].text = file.files.single.name;
      });
    } else {
      Messages.showMessage(
          context, 'El archivo debe tener un peso menor al de 1MB.',
          isError: true);
    }
  }

  void _clearPdfFile(int index) {
    setState(() {
      _pdfFiles[index] = null;
      _pdfFileNameControllers[index].clear();
    });
  }

  Future<void> _pickImageFile() async {
    final file = await FilePicker.platform.pickFiles(type: FileType.image);
    if (file != null) {
      setState(() {
        _imageFile = File(file.files.single.path!);
        _imageFileNameController.text = file.files.single.name;
      });
    }
  }

  void _clearImageFile() {
    setState(() {
      _imageFile = null;
      _imageFileNameController.clear();
    });
  }

  Future<String> _convertFileToBase64(File file) async {
    final bytes = await file.readAsBytes();
    return base64Encode(bytes);
  }

  Future<void> _saveReclamo() async {
    if (_token == null) return;
    if (_formKey.currentState?.validate() ?? false) {
      List<String?> pdfBase64Files = _pdfFiles.map((file) {
        if (file != null) {
          return base64Encode(file.readAsBytesSync());
        }
        return null;
      }).toList();
      LoadingDialog.show(context, message: 'Creando reclamo...');
      final reclamo = Reclamo(
        tipoReclamoId: _selectedTipoReclamo,
        medioPromocionId: _selectedMedioPromocion,
        parentescoId: _selectedDetalleParametroParentescos,
        numContrato: _numContratoController.text,
        identificacionCliente: _identificacionClienteController.text,
        nombresCliente: _nombresClienteController.text,
        apellidosCliente: _apellidosClienteController.text,
        direccionCliente: _direccionClienteController.text,
        telefonoCliente: _telefonoClienteController.text,
        emailCliente: _emailClienteController.text,
        identificacionRepresentante: _identifRepresentanteController.text,
        nombreRepresentante: _nombresRepresentanteController.text,
        direccionRepresentante: _direccionRepresentanteController.text,
        telefonoRepresentante: _telefonoRepresentanteController.text,
        numeroTramite: _numeroTramiteController.text,
        periodoInicial: _selectedPeriodoInicial,
        periodoFinal: _selectedPeriodoFinal,
        comentario: _comentarioController.text,
        copiaDeCedula: pdfBase64Files[0],
        respuestaInteragua: pdfBase64Files[1],
        cartaExplicativa: pdfBase64Files[2],
        cartaAutorizacion: pdfBase64Files[3],
        estado: 'Activo',
        creadorId: userId,
      );

      try {
        GenericResponseDto<void> response;
        response = await _reclamoService.createReclamo(reclamo, _token!);
        LoadingDialog.hide(context);
        Messages.showMessage(context, response.message);
        _clearForm();
      } catch (e) {
        LoadingDialog.hide(context);
        final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
        Messages.showMessage(context, 'Error: $errorMessage', isError: true);
      }
    }
  }

  Future<bool> _showConfirmationDialog() async {
    return await showDialog<bool>(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text('Confirmación'),
              content: Text(
                  '¿Estás seguro de crear tu reclamo? Si lo envías no podrás editarlo.'),
              actions: [
                TextButton(
                  onPressed: () => Navigator.of(context).pop(false),
                  child: Text('Cancelar'),
                ),
                TextButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: Text('Confirmar'),
                ),
              ],
            );
          },
        ) ??
        false;
  }

  void _selectDateTime(BuildContext context, TextEditingController controller,
      DateTime? selectedDateTime, Function(DateTime) onDateTimeSelected) async {
    // Selección de la fecha
    final DateTime? pickedDate = await showDatePicker(
      context: context,
      initialDate: selectedDateTime ?? DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2099),
    );

    if (pickedDate != null) {
      // Selección de la hora
      final TimeOfDay? pickedTime = await showTimePicker(
        context: context,
        initialTime: TimeOfDay.fromDateTime(selectedDateTime ?? DateTime.now()),
      );

      if (pickedTime != null) {
        // Combina la fecha y la hora seleccionadas
        final DateTime combinedDateTime = DateTime(
          pickedDate.year,
          pickedDate.month,
          pickedDate.day,
          pickedTime.hour,
          pickedTime.minute,
        );

        // Formatea la fecha y hora en el formato deseado
        final formattedDateTime =
            DateFormat('yyyy-MM-dd HH:mm:ss').format(combinedDateTime);

        // Actualiza el controlador de texto y llama al callback
        controller.text = formattedDateTime;
        onDateTimeSelected(combinedDateTime);
      }
    }
  }

  void _selectTime(
      BuildContext context, TextEditingController controller) async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );
    if (picked != null) {
      final now = DateTime.now();
      final selectedDateTime = DateTime(
        now.year,
        now.month,
        now.day,
        picked.hour,
        picked.minute,
      );
      controller.text = "${selectedDateTime.hour}:${selectedDateTime.minute}";
    }
  }

  void _clearForm() {
    _numContratoController.clear();
    _identificacionClienteController.clear();
    _nombresClienteController.clear();
    _apellidosClienteController.clear();
    _direccionClienteController.clear();
    _telefonoClienteController.clear();
    _emailClienteController.clear();
    _numeroTramiteController.clear();
    _resolucionTramiteController.clear();
    _aspectoTramiteController.clear();
    _subAspectoTramiteController.clear();
    _motivoTramiteController.clear();
    _numeroQuejaController.clear();
    _horaProvController.clear();
    _comentarioController.clear();
    _fechaResolucionController.clear();
    _fechaQuejaController.clear();
    _periodoInicialController.clear();
    _periodoFinalController.clear();
    _fechaIngresoController.clear();
    _fechaVencimientoController.clear();
    _pdfFileNameControllers.forEach((controller) => controller.clear());
    _pdfFiles = List.generate(4, (_) => null);
    _imageFileNameController.clear();
    _imageFile = null;
    _identifRepresentanteController.clear();
    _nombresRepresentanteController.clear();
    _direccionRepresentanteController.clear();
    _telefonoRepresentanteController.clear();
    setState(() {
      _selectedTipoReclamo = null;
      _selectedMedioPromocion = null;
      _selectedDetalleParametroParentescos = null;
      _selectedFechaResolucion = null;
      _selectedFechaQueja = null;
      _selectedPeriodoInicial = null;
      _selectedPeriodoFinal = null;
      _selectedFechaIngreso = null;
      _selectedFechaVencimiento = null;
    });
  }

  String _getLabelTextForPdfField(int index) {
    switch (index) {
      case 0:
        return 'Copia de Cédula';
      case 1:
        return 'Respuesta de Interagua';
      case 2:
        return 'Carta Explicativa del Reclamo';
      case 3:
        return 'Carta de Autorización del Propietario';
      default:
        return 'Archivo PDF';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        padding: EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              DropdownButtonFormField<int>(
                decoration: InputDecoration(
                  labelText: 'Tipo Reclamo',
                  border: OutlineInputBorder(),
                ),
                items: _tipoReclamos.map((tipoReclamo) {
                  return DropdownMenuItem<int>(
                    value: tipoReclamo.id,
                    child: Text(tipoReclamo.nombre),
                  );
                }).toList(),
                onChanged: (tipoReclamoId) {
                  setState(() {
                    _selectedTipoReclamo = tipoReclamoId;
                  });
                },
                validator: (value) {
                  if (value == null || _tipoReclamos.isEmpty) {
                    return 'Por favor selecciona un Tipo reclamo';
                  }
                  return null;
                },
                value: _selectedTipoReclamo,
              ),
              SizedBox(height: 10),
              DropdownButtonFormField<int>(
                decoration: InputDecoration(
                  labelText: 'Medio Promoción',
                  border: OutlineInputBorder(),
                ),
                items: _tipoMedioPromocion.map((medioPromocion) {
                  return DropdownMenuItem<int>(
                    value: medioPromocion.id,
                    child: Text(medioPromocion.nombre),
                  );
                }).toList(),
                onChanged: (medioPromocionId) {
                  setState(() {
                    _selectedMedioPromocion = medioPromocionId;
                  });
                },
                validator: (value) {
                  if (value == null || _tipoMedioPromocion.isEmpty) {
                    return 'Por favor selecciona un Medio de Promoción';
                  }
                  return null;
                },
                value: _selectedMedioPromocion,
              ),
              SizedBox(height: 10),
              TextFormField(
                controller: _numContratoController,
                maxLength: 30,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  labelText: 'Número de Contrato',
                  counterText: '',
                  border: OutlineInputBorder(),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Por favor ingresa el número del contrato';
                  }
                  return null;
                },
              ),
              SizedBox(height: 10),
              CheckboxListTile(
                title: Text('¿Eres el propietario del contrato?'),
                value: _isOwner,
                onChanged: (bool? value) {
                  setState(() {
                    _isOwner = value ?? false;

                    // Limpiar los controladores de los campos que se ocultan
                    if (_isOwner) {
                      // Si el propietario es el dueño, limpiar los campos del representante
                      _identifRepresentanteController.clear();
                      _nombresRepresentanteController.clear();
                      _direccionRepresentanteController.clear();
                      _telefonoRepresentanteController.clear();
                      _clearPdfFile(
                          3); // Limpiar el PDF de autorización del propietario
                    } else {
                      // Si el propietario NO es el dueño, limpiar los campos del cliente
                      _identificacionClienteController.clear();
                      _apellidosClienteController.clear();
                      _nombresClienteController.clear();
                      _direccionClienteController.clear();
                      _telefonoClienteController.clear();
                      _emailClienteController.clear();
                    }
                  });
                },
              ),
              if (_isOwner) ...[
                TextFormField(
                  controller: _identificacionClienteController,
                  keyboardType: TextInputType.number,
                  maxLength: 10,
                  decoration: InputDecoration(
                    labelText: 'Cédula del Cliente',
                    border: OutlineInputBorder(),
                    counterText: '',
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Por favor ingresa la cédula del cliente';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 10),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: TextFormField(
                        controller: _apellidosClienteController,
                        maxLength: 30,
                        decoration: InputDecoration(
                          labelText: 'Apellidos del Cliente',
                          border: OutlineInputBorder(),
                          counterText: '',
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Ingresa los apellidos';
                          }
                          return null;
                        },
                      ),
                    ),
                    SizedBox(width: 10),
                    Expanded(
                      child: TextFormField(
                        controller: _nombresClienteController,
                        maxLength: 30,
                        decoration: InputDecoration(
                          labelText: 'Nombres del Cliente',
                          border: OutlineInputBorder(),
                          counterText: '',
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Ingresa los nombres';
                          }
                          return null;
                        },
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 10),
                TextFormField(
                  controller: _direccionClienteController,
                  maxLength: 100,
                  decoration: InputDecoration(
                    labelText: 'Dirección del Cliente',
                    border: OutlineInputBorder(),
                    counterText: '',
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Ingresa la dirección del cliente';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 10),
                TextFormField(
                  controller: _telefonoClienteController,
                  keyboardType: TextInputType.number,
                  maxLength: 10,
                  decoration: InputDecoration(
                    labelText: 'Celular del Cliente',
                    border: OutlineInputBorder(),
                    counterText: '',
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Ingresa el celular del cliente';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 10),
                TextFormField(
                  controller: _emailClienteController,
                  maxLength: 30,
                  decoration: InputDecoration(
                    labelText: 'Correo del Cliente',
                    border: OutlineInputBorder(),
                    counterText: '',
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Ingresa el correo electrónico del cliente';
                    }
                    final emailRegex = RegExp(r'^[^@]+@[^@]+\.[^@]+$');
                    if (!emailRegex.hasMatch(value)) {
                      return 'Por favor ingresa un correo electrónico válido';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 10),
              ] else ...[
                DropdownButtonFormField<int>(
                  decoration: InputDecoration(
                    labelText: 'Parentesco',
                    border: OutlineInputBorder(),
                  ),
                  items: _detalleparametrosParentescos.map((medioPromocion) {
                    return DropdownMenuItem<int>(
                      value: medioPromocion.id,
                      child: Text(medioPromocion.valor!),
                    );
                  }).toList(),
                  onChanged: (medioPromocionId) {
                    setState(() {
                      _selectedDetalleParametroParentescos = medioPromocionId;
                    });
                  },
                  validator: (value) {
                    if (value == null ||
                        _detalleparametrosParentescos.isEmpty) {
                      return 'Por favor selecciona un Parentesco';
                    }
                    return null;
                  },
                  value: _selectedDetalleParametroParentescos,
                ),
                SizedBox(height: 10),
                TextFormField(
                  controller: _identifRepresentanteController,
                  keyboardType: TextInputType.number,
                  maxLength: 10,
                  decoration: InputDecoration(
                    labelText: 'Cédula del Representante',
                    border: OutlineInputBorder(),
                    counterText: '',
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Por favor ingresa la cédula del representante';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 10),
                TextFormField(
                  controller: _nombresRepresentanteController,
                  maxLength: 30,
                  decoration: InputDecoration(
                    labelText: 'Nombres del Representante',
                    border: OutlineInputBorder(),
                    counterText: '',
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Ingresa el nombre';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 10),
                TextFormField(
                  controller: _direccionRepresentanteController,
                  maxLength: 100,
                  decoration: InputDecoration(
                    labelText: 'Dirección del Representante',
                    border: OutlineInputBorder(),
                    counterText: '',
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Ingresa la dirección del representante';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 10),
                TextFormField(
                  controller: _telefonoRepresentanteController,
                  keyboardType: TextInputType.number,
                  maxLength: 10,
                  decoration: InputDecoration(
                    labelText: 'Celular del Representante',
                    border: OutlineInputBorder(),
                    counterText: '',
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Ingresa el celular del representante';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 10),
              ],
              TextFormField(
                controller: _numeroTramiteController,
                keyboardType: TextInputType.number,
                maxLength: 30,
                decoration: InputDecoration(
                  labelText: 'Número del Trámite',
                  border: OutlineInputBorder(),
                  counterText: '',
                ),
              ),
              SizedBox(height: 10),
              TextFormField(
                controller: _periodoInicialController,
                decoration: InputDecoration(
                  labelText: 'Fecha de Inicio del Reclamo',
                  border: OutlineInputBorder(),
                  suffixIcon: IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      _periodoInicialController.clear();
                    },
                  ),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Selecciona la fecha de inicio';
                  }
                  return null;
                },
                readOnly: true,
                onTap: () {
                  _selectDateTime(context, _periodoInicialController,
                      _selectedPeriodoInicial, (pickedDate) {
                    _selectedPeriodoInicial = pickedDate;
                  });
                },
              ),
              SizedBox(height: 10),
              TextFormField(
                controller: _periodoFinalController,
                decoration: InputDecoration(
                  labelText: 'Fecha de Final del Reclamo',
                  border: OutlineInputBorder(),
                  suffixIcon: IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      _periodoFinalController.clear();
                    },
                  ),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Selecciona la fecha final';
                  }
                  return null;
                },
                readOnly: true,
                onTap: () {
                  _selectDateTime(
                      context, _periodoFinalController, _selectedPeriodoFinal,
                      (pickedDate) {
                    _selectedPeriodoFinal = pickedDate;
                  });
                },
              ),
              SizedBox(height: 10),
              TextFormField(
                controller: _comentarioController,
                maxLength: 200,
                decoration: InputDecoration(
                  labelText: 'Comentario',
                  border: OutlineInputBorder(),
                  counterText: '',
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Por favor ingresa un comentario para tu reclamo';
                  }
                  return null;
                },
              ),
              SizedBox(height: 10),
              if (_isOwner) ...[
                Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: TextFormField(
                            controller: _pdfFileNameControllers[0],
                            decoration: InputDecoration(
                              labelText: 'Copia de Cédula',
                              border: OutlineInputBorder(),
                            ),
                            validator: (value) {
                              if (_pdfFiles[0] == null) {
                                return 'Por favor agrega la copia de cédula';
                              }
                              return null;
                            },
                            readOnly: true,
                          ),
                        ),
                        IconButton(
                          icon: Icon(_pdfFiles[0] != null
                              ? Icons.clear
                              : Icons.attach_file),
                          onPressed: () {
                            if (_pdfFiles[0] != null) {
                              _clearPdfFile(0);
                            } else {
                              _pickPdfFile(0); // Abre el explorador de archivos
                            }
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Row(
                      children: [
                        Expanded(
                          child: TextFormField(
                            controller: _pdfFileNameControllers[2],
                            decoration: InputDecoration(
                              labelText: 'Carta Explicativa del Reclamo',
                              border: OutlineInputBorder(),
                            ),
                            validator: (value) {
                              if (_pdfFiles[2] == null) {
                                return 'Por favor agrega la carta explicativa del reclamo';
                              }
                              return null;
                            },
                            readOnly: true,
                          ),
                        ),
                        IconButton(
                          icon: Icon(_pdfFiles[2] != null
                              ? Icons.clear
                              : Icons.attach_file),
                          onPressed: () {
                            if (_pdfFiles[2] != null) {
                              _clearPdfFile(2);
                            } else {
                              _pickPdfFile(2); // Abre el explorador de archivos
                            }
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Row(
                      children: [
                        Expanded(
                          child: TextFormField(
                            controller: _pdfFileNameControllers[1],
                            decoration: InputDecoration(
                              labelText: 'Respuesta de Interagua',
                              border: OutlineInputBorder(),
                            ),
                            validator: (value) {
                              // Campo opcional
                              return null;
                            },
                            readOnly: true,
                          ),
                        ),
                        IconButton(
                          icon: Icon(_pdfFiles[1] != null
                              ? Icons.clear
                              : Icons.attach_file),
                          onPressed: () {
                            if (_pdfFiles[1] != null) {
                              _clearPdfFile(1);
                            } else {
                              _pickPdfFile(1); // Abre el explorador de archivos
                            }
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                  ],
                ),
              ] else ...[
                Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: TextFormField(
                            controller: _pdfFileNameControllers[0],
                            decoration: InputDecoration(
                              labelText: 'Copia de Cédula',
                              border: OutlineInputBorder(),
                            ),
                            validator: (value) {
                              if (_pdfFiles[0] == null) {
                                return 'Por favor agrega la copia de cédula';
                              }
                              return null;
                            },
                            readOnly: true,
                          ),
                        ),
                        IconButton(
                          icon: Icon(_pdfFiles[0] != null
                              ? Icons.clear
                              : Icons.attach_file),
                          onPressed: () {
                            if (_pdfFiles[0] != null) {
                              _clearPdfFile(0);
                            } else {
                              _pickPdfFile(0); // Abre el explorador de archivos
                            }
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Row(
                      children: [
                        Expanded(
                          child: TextFormField(
                            controller: _pdfFileNameControllers[1],
                            decoration: InputDecoration(
                              labelText: 'Respuesta de Interagua',
                              border: OutlineInputBorder(),
                            ),
                            validator: (value) {
                              // Campo opcional
                              return null;
                            },
                            readOnly: true,
                          ),
                        ),
                        IconButton(
                          icon: Icon(_pdfFiles[1] != null
                              ? Icons.clear
                              : Icons.attach_file),
                          onPressed: () {
                            if (_pdfFiles[1] != null) {
                              _clearPdfFile(1);
                            } else {
                              _pickPdfFile(1); // Abre el explorador de archivos
                            }
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Row(
                      children: [
                        Expanded(
                          child: TextFormField(
                            controller: _pdfFileNameControllers[2],
                            decoration: InputDecoration(
                              labelText: 'Carta Explicativa del Reclamo',
                              border: OutlineInputBorder(),
                            ),
                            validator: (value) {
                              if (_pdfFiles[2] == null) {
                                return 'Por favor agrega la carta explicativa del reclamo';
                              }
                              return null;
                            },
                            readOnly: true,
                          ),
                        ),
                        IconButton(
                          icon: Icon(_pdfFiles[2] != null
                              ? Icons.clear
                              : Icons.attach_file),
                          onPressed: () {
                            if (_pdfFiles[2] != null) {
                              _clearPdfFile(2);
                            } else {
                              _pickPdfFile(2); // Abre el explorador de archivos
                            }
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Row(
                      children: [
                        Expanded(
                          child: TextFormField(
                            controller: _pdfFileNameControllers[3],
                            decoration: InputDecoration(
                              labelText:
                                  'Carta de Autorización del Propietario',
                              border: OutlineInputBorder(),
                            ),
                            validator: (value) {
                              if (_pdfFiles[3] == null) {
                                return 'Por favor agrega la carta de autorización del propietario';
                              }
                              return null;
                            },
                            readOnly: true,
                          ),
                        ),
                        IconButton(
                          icon: Icon(_pdfFiles[3] != null
                              ? Icons.clear
                              : Icons.attach_file),
                          onPressed: () {
                            if (_pdfFiles[3] != null) {
                              _clearPdfFile(3);
                            } else {
                              _pickPdfFile(3); // Abre el explorador de archivos
                            }
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                  ],
                ),
              ],
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  ElevatedButton(
                    onPressed: () async {
                      bool? confirm = await _showConfirmationDialog();
                      if (confirm == true) {
                        _saveReclamo();
                      }
                    },
                    child: Text('Guardar'),
                  ),
                  ElevatedButton(
                    onPressed: _clearForm,
                    child: Text('Cancelar'),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
