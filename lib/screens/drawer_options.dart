import 'dart:convert';

import 'package:emapagep/config/constants.dart';
import 'package:emapagep/dto/generic_response_dto.dart';
import 'package:emapagep/models/usuario.dart';
import 'package:emapagep/screens/create_reclamo.dart';
import 'package:emapagep/screens/faq.dart';
import 'package:emapagep/screens/home.dart';
import 'package:emapagep/screens/respuesta_reclamos.dart';
import 'package:emapagep/services/usuario_service.dart';
import 'package:flutter/material.dart';
import 'package:emapagep/screens/reclamos.dart';
import 'package:emapagep/screens/detalle_parametro.dart';
import 'package:emapagep/screens/medio_promocion.dart';
import 'package:emapagep/screens/parametro.dart';
import 'package:emapagep/screens/rol.dart';
import 'package:emapagep/screens/tipo_reclamo.dart';
import 'package:emapagep/screens/usuario.dart';
import 'package:emapagep/screens/login.dart';
import 'package:emapagep/screens/editar_perfil.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:jwt_decoder/jwt_decoder.dart';

class DrawerOptionsScreen extends StatefulWidget {
  @override
  _DrawerOptionsScreenState createState() => _DrawerOptionsScreenState();
}

class _DrawerOptionsScreenState extends State<DrawerOptionsScreen> {
  Widget _selectedScreen = HomeScreen(); // Pantalla predeterminada
  String _screenTitle = 'Inicio'; // Título predeterminado
  final storage = FlutterSecureStorage();
  final UsuarioService _usuarioService = UsuarioService();
  List<String> roles = [];
  int? userId;
  String nombre = '';
  String apellido = '';
  String nombreUsuario = '';
  String direccion = '';
  String telefono = '';
  late String cedula = '';
  String? imageBase64;
  String? _token;
  Map<String, Widget> _screenOptions = {};

  @override
  void initState() {
    super.initState();
    _loadRoles();
  }

  Future<void> _loadRoles() async {
    String? token = await storage.read(key: 'auth_token');
    if (token != null) {
      Map<String, dynamic> decodedToken = JwtDecoder.decode(token);
      setState(() {
        roles = List<String>.from(
            decodedToken['roles'].map((role) => role['name']));
        _token = token;
      });
      _loadUserData();
    }
  }

  Future<void> _loadUserData() async {
    String? token = await storage.read(key: 'auth_token');
    if (token != null) {
      Map<String, dynamic> decodedToken = JwtDecoder.decode(token);
      userId = int.parse(decodedToken['sub']);
      try {
        GenericResponseDto<Usuario> response =
            await _usuarioService.getUsuarioById(userId!, token);

        setState(() {
          nombre = response.payload.nombres;
          apellido = response.payload.apellidos;
          cedula = response.payload.identificacion;
          nombreUsuario = response.payload.nombreUsuario;
          telefono = response.payload.telefono;
          direccion = response.payload.direccion;
          imageBase64 = response.payload.foto;
          // Aquí puedes definir las pantallas que se mostrarán
          _screenOptions = {
            'Inicio': HomeScreen(),
            'Rol': RolScreen(),
            'Medio Promoción': MedioPromocionScreen(),
            'Parámetros': ParametroScreen(),
            'Detalles Parámetros': DetalleParametroScreen(),
            'Usuarios': UsuarioScreen(),
            'Tipo Reclamo': TipoReclamoScreen(),
            'Crear Reclamo': CreateReclamoScreen(),
            'Respuestas a mi Reclamo':
                RespuestaReclamoScreen(cedula: cedula, id: userId!),
            'Listado de Reclamos': ReclamoScreen(),
            'Preguntas frecuentes': FaqScreen(),
          };
        });
      } catch (e) {
        print('Error fetching user data: $e');
      }
    }
  }

  Widget _buildProfileImage() {
    if (imageBase64 != null) {
      return CircleAvatar(
        radius: 35,
        backgroundImage: MemoryImage(base64Decode(imageBase64!)),
      );
    } else {
      return CircleAvatar(
        radius: 35,
        backgroundColor: Colors.white,
        child: Icon(Icons.person, size: 40, color: Colors.blue),
      );
    }
  }

  void _onMenuItemTap(String screenName) {
    setState(() {
      _selectedScreen = _screenOptions[screenName]!;
      _screenTitle = screenName;
    });
    Navigator.pop(
        context); // Cierra el Drawer después de seleccionar una opción
  }

  void _logout() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => LoginRegisterScreen(), // Redirige al login
      ),
    );
  }

  void _goToEditProfile() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => EditProfileScreen(
          id: userId!,
          nombres: nombre,
          apellidos: apellido,
          nombreUsuario: nombreUsuario,
          telefono: telefono,
          direccion: direccion,
          foto: imageBase64,
          token: _token!,
        ),
      ),
    );
  }

  List<Widget> _buildDrawerOptions() {
    List<Widget> options = [];
    options.add(
      ListTile(
        leading: Icon(Icons.home),
        title: Text('Inicio'),
        onTap: () => _onMenuItemTap('Inicio'),
      ),
    );
    if (roles.contains(Constants.roleAdmin) ||
        roles.contains(Constants.roleAtencionAlUsuario)) {
      options.add(
        ListTile(
          leading: Icon(Icons.person),
          title: Text('Rol'),
          onTap: () => _onMenuItemTap('Rol'),
        ),
      );
    }

    if (roles.contains(Constants.roleAdmin) ||
        roles.contains(Constants.roleAtencionAlUsuario)) {
      options.add(
        ListTile(
          leading: Icon(Icons.campaign),
          title: Text('Medio Promoción'),
          onTap: () => _onMenuItemTap('Medio Promoción'),
        ),
      );
    }

    if (roles.contains(Constants.roleAdmin) ||
        roles.contains(Constants.roleAtencionAlUsuario)) {
      options.add(
        ListTile(
          leading: Icon(Icons.settings),
          title: Text('Parámetros'),
          onTap: () => _onMenuItemTap('Parámetros'),
        ),
      );
    }

    if (roles.contains(Constants.roleAdmin) ||
        roles.contains(Constants.roleAtencionAlUsuario)) {
      options.add(
        ListTile(
          leading: Icon(Icons.details),
          title: Text('Detalles Parámetros'),
          onTap: () => _onMenuItemTap('Detalles Parámetros'),
        ),
      );
    }

    if (roles.contains(Constants.roleAdmin) ||
        roles.contains(Constants.roleAtencionAlUsuario)) {
      options.add(
        ListTile(
          leading: Icon(Icons.people),
          title: Text('Usuarios'),
          onTap: () => _onMenuItemTap('Usuarios'),
        ),
      );
    }
    if (roles.contains(Constants.roleAdmin) ||
        roles.contains(Constants.roleAtencionAlUsuario)) {
      options.add(
        ListTile(
          leading: Icon(Icons.receipt),
          title: Text('Tipo Reclamo'),
          onTap: () => _onMenuItemTap('Tipo Reclamo'),
        ),
      );
    }
    options.add(
      ExpansionTile(
        leading: Icon(Icons.report),
        title: Text('Reclamos'),
        children: [
          if (roles.contains(Constants.roleAdmin) ||
              roles.contains(Constants.roleAtencionAlUsuario) ||
              roles.contains(Constants.roleCliente))
            ListTile(
              leading: Icon(Icons.create),
              title: Text('Crear Reclamo'),
              onTap: () => _onMenuItemTap('Crear Reclamo'),
            ),
          if (roles.contains(Constants.roleAdmin) ||
              roles.contains(Constants.roleAtencionAlUsuario) ||
              roles.contains(Constants.roleCliente))
            ListTile(
              leading: Icon(Icons.recommend),
              title: Text('Respuestas a mi Reclamo'),
              onTap: () => _onMenuItemTap('Respuestas a mi Reclamo'),
            ),
          ListTile(
            leading: Icon(Icons.list_alt),
            title: Text('Listado de Reclamos'),
            onTap: () => _onMenuItemTap('Listado de Reclamos'),
          ),
        ],
      ),
    );
    //   if (roles.contains(Constants.roleCliente)) {
    options.add(
      ListTile(
        leading: Icon(Icons.question_answer),
        title: Text('Preguntas frecuentes'),
        onTap: () => _onMenuItemTap('Preguntas frecuentes'),
      ),
    );
    // }
    options.add(
      Divider(
          height:
              1), // Añade un divisor para separar el logout del resto de ítems
    );
    options.add(
      ListTile(
        leading: Icon(Icons.logout),
        title: Text('Logout'),
        onTap: _logout,
      ),
    );

    return options;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_screenTitle),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
              child: Column(
                children: [
                  _buildProfileImage(),
                  const SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '$nombreUsuario',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                        ),
                      ),
                      IconButton(
                        icon: Icon(Icons.edit, color: Colors.white),
                        onPressed: _goToEditProfile,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            ..._buildDrawerOptions(),
          ],
        ),
      ),
      body: _selectedScreen,
    );
  }
}
