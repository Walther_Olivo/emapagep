import 'package:emapagep/dto/change_password_dto.dart';
import 'package:emapagep/dto/generic_response_dto.dart';
import 'package:emapagep/dto/valid_email_send_code_dto.dart';
import 'package:emapagep/dto/validation_code_dto.dart';
import 'package:emapagep/screens/login.dart';
import 'package:emapagep/services/auth_service.dart';
import 'package:emapagep/widgets/loading_dialog.dart';
import 'package:emapagep/widgets/messages.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class RecuperarContraseniaScreen extends StatefulWidget {
  @override
  _RecuperarContraseniaScreenState createState() =>
      _RecuperarContraseniaScreenState();
}

class _RecuperarContraseniaScreenState
    extends State<RecuperarContraseniaScreen> {
  int _currentStep = 0;
  final _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _codeController = TextEditingController();
  final _passwordController = TextEditingController();
  final _confirmPasswordController = TextEditingController();
  bool _obscurePassword = true;
  bool _obscureConfirmPassword = true;
  final AuthService _authService = AuthService(FlutterSecureStorage());

  @override
  void dispose() {
    _emailController.dispose();
    _codeController.dispose();
    _passwordController.dispose();
    _confirmPasswordController.dispose();
    super.dispose();
  }

  void _validEmailAndSendCode() async {
    if (_formKey.currentState?.validate() ?? false) {
      String email = _emailController.text;
      LoadingDialog.show(context, message: 'Comprobando email...');
      final va = ValidEmailSendCodeDto(
        email: email,
        purpose: 'password-reset',
      );
      try {
        GenericResponseDto<String> response =
            await _authService.validEmailAndSendCode(va);
        if (response.code == 201) {
          LoadingDialog.hide(context);
          Messages.showMessage(context, response.payload);
          setState(() {
            if (_currentStep < 2) {
              _currentStep++;
            }
          });
        } else {
          LoadingDialog.hide(context);
          Messages.showMessage(context, response.payload, isError: true);
        }
      } catch (e) {
        LoadingDialog.hide(context);
        final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
        Messages.showMessage(context, 'Error: $errorMessage', isError: true);
      }
    }
  }

  Future<void> _validateCode() async {
    if (_formKey.currentState?.validate() ?? false) {
      String email = _emailController.text;
      LoadingDialog.show(context, message: 'Validando código...');
      final v = ValidationCodeDto(
        email: email,
        code: _codeController.text,
        purpose: 'password-reset',
      );
      try {
        GenericResponseDto<String> response =
            await _authService.validateCode(v);
        if (response.code == 201) {
          LoadingDialog.hide(context);
          Messages.showMessage(context, response.payload);
          setState(() {
            if (_currentStep < 2) {
              _currentStep++;
            }
          });
        } else {
          LoadingDialog.hide(context);
          Messages.showMessage(context, response.payload, isError: true);
        }
      } catch (e) {
        LoadingDialog.hide(context);
        Messages.showMessage(context, e.toString(), isError: true);
      }
    }
  }

  Future<void> _changePassword() async {
    if (_formKey.currentState?.validate() ?? false) {
      String email = _emailController.text;
      LoadingDialog.show(context, message: 'Cambiando contraseña...');
      final v = ChangePasswordDto(
        email: email,
        newPassword: _confirmPasswordController.text,
      );
      try {
        GenericResponseDto<String> response =
            await _authService.changePassword(v);
        if (response.code == 201) {
          LoadingDialog.hide(context);
          Messages.showMessage(context, response.payload);
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => LoginRegisterScreen()),
          );
        } else {
          LoadingDialog.hide(context);
          Messages.showMessage(context, response.payload, isError: true);
        }
      } catch (e) {
        LoadingDialog.hide(context);
        Messages.showMessage(context, e.toString(), isError: true);
      }
    }
  }

  Widget _buildStepContent() {
    switch (_currentStep) {
      case 0:
        return Form(
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                controller: _emailController,
                keyboardType: TextInputType.emailAddress,
                maxLength: 30,
                decoration: InputDecoration(
                  labelText: 'Correo electrónico',
                  border: OutlineInputBorder(),
                  counterText: '',
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Por favor ingresa el correo electrónico';
                  }
                  final emailRegex = RegExp(r'^[^@]+@[^@]+\.[^@]+');
                  if (!emailRegex.hasMatch(value)) {
                    return 'Por favor ingresa un correo electrónico válido';
                  }
                  return null;
                },
              ),
              SizedBox(height: 20),
              MaterialButton(
                onPressed: _validEmailAndSendCode,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                color: Colors.blueAccent,
                padding: const EdgeInsets.symmetric(
                  horizontal: 80,
                  vertical: 15,
                ),
                child: const Text(
                  'Enviar',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
        );
      case 1:
        return Form(
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                controller: _codeController,
                keyboardType: TextInputType.number,
                maxLength: 6,
                decoration: InputDecoration(
                  labelText: 'Código de verificación',
                  counterText: '',
                  border: OutlineInputBorder(),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Por favor ingresa el código de verificación';
                  }
                  if (!RegExp(r'^\d+$').hasMatch(value)) {
                    return 'El código de verificación debe ser un número';
                  }
                  return null;
                },
              ),
              SizedBox(height: 20),
              MaterialButton(
                onPressed: _validateCode,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                color: Colors.blueAccent,
                padding: const EdgeInsets.symmetric(
                  horizontal: 80,
                  vertical: 15,
                ),
                child: const Text(
                  'Validar',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
        );
      case 2:
        return Form(
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                controller: _passwordController,
                maxLength: 15,
                obscureText: _obscurePassword,
                decoration: InputDecoration(
                  labelText: 'Nueva contraseña',
                  counterText: '',
                  border: OutlineInputBorder(),
                  suffixIcon: IconButton(
                    icon: Icon(_obscurePassword
                        ? Icons.visibility
                        : Icons.visibility_off),
                    onPressed: () {
                      setState(() {
                        _obscurePassword = !_obscurePassword;
                      });
                    },
                  ),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Por favor ingresa la nueva contraseña';
                  }
                  return null;
                },
              ),
              SizedBox(height: 20),
              TextFormField(
                controller: _confirmPasswordController,
                obscureText: _obscureConfirmPassword,
                maxLength: 15,
                decoration: InputDecoration(
                  labelText: 'Confirmar contraseña',
                  border: OutlineInputBorder(),
                  counterText: '',
                  suffixIcon: IconButton(
                    icon: Icon(_obscureConfirmPassword
                        ? Icons.visibility
                        : Icons.visibility_off),
                    onPressed: () {
                      setState(() {
                        _obscureConfirmPassword = !_obscureConfirmPassword;
                      });
                    },
                  ),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Por favor confirma la nueva contraseña';
                  }
                  if (value != _passwordController.text) {
                    return 'Las contraseñas no coinciden';
                  }
                  return null;
                },
              ),
              SizedBox(height: 20),
              MaterialButton(
                onPressed: _changePassword,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                color: Colors.blueAccent,
                padding: const EdgeInsets.symmetric(
                  horizontal: 80,
                  vertical: 15,
                ),
                child: const Text(
                  'Restablecer contraseña',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
        );
      default:
        return Container();
    }
  }

  Widget _buildStepIndicator() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _buildIndicatorDot(0),
        _buildIndicatorDot(1),
        _buildIndicatorDot(2),
      ],
    );
  }

  Widget _buildIndicatorDot(int index) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 4),
      width: 12,
      height: 12,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: _currentStep == index ? Colors.blue : Colors.grey,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Recuperar contraseña'),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            // Redirigir al login
            Navigator.pop(context);
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              LayoutBuilder(
                builder: (context, constraints) {
                  return Center(
                    child: Image.asset(
                      'assets/candado.png',
                      width: constraints.maxWidth * 0.6,
                      height: constraints.maxWidth * 0.6,
                    ),
                  );
                },
              ),
              SizedBox(height: 20),
              _buildStepContent(),
              SizedBox(height: 20),
              _buildStepIndicator(),
            ],
          ),
        ),
      ),
    );
  }
}

void main() {
  runApp(MaterialApp(
    home: RecuperarContraseniaScreen(),
  ));
}
