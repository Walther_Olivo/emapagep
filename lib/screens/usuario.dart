import 'package:emapagep/config/constants.dart';
import 'package:emapagep/dto/generic_response_dto.dart';
import 'package:emapagep/widgets/loading_dialog.dart';
import 'package:flutter/material.dart';
import 'package:emapagep/models/usuario.dart';
import 'package:emapagep/services/usuario_service.dart';
import 'package:emapagep/widgets/messages.dart';
import 'package:emapagep/models/rol.dart';
import 'package:emapagep/services/rol_service.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:intl/intl.dart';

class UsuarioScreen extends StatefulWidget {
  @override
  _UsuarioScreenState createState() => _UsuarioScreenState();
}

class _UsuarioScreenState extends State<UsuarioScreen> {
  final TextEditingController _nombreController = TextEditingController();
  final TextEditingController _identificacionController =
      TextEditingController();
  final TextEditingController _apellidosController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _usuarioController = TextEditingController();
  final TextEditingController _contraseniaController = TextEditingController();
  final TextEditingController _telefonoController = TextEditingController();
  final TextEditingController _direccionController = TextEditingController();
  final TextEditingController _searchController = TextEditingController();
  DateTime? _selectedFechaNacimiento;
  final TextEditingController _fechaNacimientoController =
      TextEditingController();
  final UsuarioService _usuarioService = UsuarioService();
  final RolService _rolService = RolService();

  List<Usuario> _usuarios = [];
  List<Usuario> _filteredUsuarios = [];
  Usuario? _selectedUsuario;
  bool _isAddingOrUpdating = false;
  List<Rol> _roles = [];
  List<int>? _selectedRol;
  final storage = FlutterSecureStorage();
  List<String> roles = [];
  String? _token;
  final _formKey = GlobalKey<FormState>();
  Map<int, String> _rolesMap = {};
  int _currentPage = 1;
  int _totalRecords = 0;
  int _rowsPerPage = 5;
  late UsuarioDataSource _source;

  @override
  void initState() {
    super.initState();
    _loadRolesAndToken();
    _source = UsuarioDataSource(
        context: context,
        usuarios: _filteredUsuarios,
        onDelete: _deleteUsuario,
        onEdit: (Usuario usuario) {
          setState(() {
            _selectedUsuario = usuario;
            _nombreController.text = usuario.nombres;
            _apellidosController.text = usuario.apellidos;
            _identificacionController.text = usuario.identificacion;
            _emailController.text = usuario.email;
            _usuarioController.text = usuario.nombreUsuario;
            _contraseniaController.text = usuario.contrasenia;
            _telefonoController.text = usuario.telefono;
            _direccionController.text = usuario.direccion;
            _fechaNacimientoController.text =
                usuario.fechaNacimiento.toString();
            _selectedFechaNacimiento = usuario.fechaNacimiento;
            _selectedRol = usuario.roles;
            _isAddingOrUpdating = true;
          });
          _showForm(); // Muestra el formulario de actualización
        },
        onView: _showDetails,
        roles: roles,
        totalRecords: _totalRecords,
        isFilter: true);
  }

  Future<void> _loadRolesAndToken() async {
    String? token = await storage.read(key: 'auth_token');
    if (token != null) {
      Map<String, dynamic> decodedToken = JwtDecoder.decode(token);
      setState(() {
        roles = List<String>.from(
            decodedToken['roles'].map((role) => role['name']));
        _token = token;
      });
      _fetchRoles();
      _fetchUsuarios(page: _currentPage, size: _rowsPerPage);
    }
  }

  Future<void> _fetchUsuarios({int page = 1, int size = 5}) async {
    if (_token == null) return;
    LoadingDialog.show(context, message: 'Cargando Usuarios...');
    try {
      final response = await _usuarioService.getAllUsuarios(
        _token!,
        page: page,
        size: size,
      );
      final usuarios = response.payload;
      setState(() {
        _usuarios = usuarios;
        _filteredUsuarios = usuarios;
        _totalRecords = response.totalRecords ?? 0;
        _currentPage = page; // Actualiza la página actual
        _rowsPerPage = size; // Actualiza el tamaño de página
        _source = UsuarioDataSource(
            context: context,
            usuarios: _filteredUsuarios,
            onDelete: _deleteUsuario,
            onEdit: (Usuario usuario) {
              setState(() {
                _selectedUsuario = usuario;
                _nombreController.text = usuario.nombres;
                _apellidosController.text = usuario.apellidos;
                _identificacionController.text = usuario.identificacion;
                _emailController.text = usuario.email;
                _usuarioController.text = usuario.nombreUsuario;
                _contraseniaController.text = usuario.contrasenia;
                _telefonoController.text = usuario.telefono;
                _direccionController.text = usuario.direccion;
                _fechaNacimientoController.text =
                    usuario.fechaNacimiento.toString();
                _selectedFechaNacimiento = usuario.fechaNacimiento;
                _selectedRol = usuario.roles;
                _isAddingOrUpdating = true;
              });
              _showForm(); // Muestra el formulario de actualización
            },
            onView: _showDetails,
            roles: roles,
            totalRecords: _totalRecords,
            isFilter: true);
        LoadingDialog.hide(context);
      });
    } catch (e) {
      LoadingDialog.hide(context);
      final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
      Messages.showMessage(context, 'Error: $errorMessage',
          isError: true, isTop: true);
    }
  }

  Future<void> _fetchRoles() async {
    if (_token == null) return;
    try {
      final response = await _rolService.getAllRoles(_token!);
      final roles = response.payload;
      setState(() {
        _roles = roles;
        _rolesMap = {for (var s in roles) s.id!: s.nombre};
      });
    } catch (e) {
      final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
      Messages.showMessage(context, 'Error: $errorMessage',
          isError: true, isTop: true);
    }
  }

  void _filterUsuarios() {
    final query = _searchController.text.toLowerCase();
    setState(() {
      _filteredUsuarios = _usuarios.where((usuario) {
        return usuario.nombres.toLowerCase().contains(query) ||
            usuario.id.toString().contains(query);
      }).toList();
      _source = UsuarioDataSource(
          context: context,
          usuarios: _filteredUsuarios,
          onDelete: _deleteUsuario,
          onEdit: (Usuario usuario) {
            setState(() {
              _selectedUsuario = usuario;
              _nombreController.text = usuario.nombres;
              _apellidosController.text = usuario.apellidos;
              _identificacionController.text = usuario.identificacion;
              _emailController.text = usuario.email;
              _usuarioController.text = usuario.nombreUsuario;
              _contraseniaController.text = usuario.contrasenia;
              _telefonoController.text = usuario.telefono;
              _direccionController.text = usuario.direccion;
              _fechaNacimientoController.text =
                  usuario.fechaNacimiento.toString();
              _selectedFechaNacimiento = usuario.fechaNacimiento;
              _selectedRol = usuario.roles;
              _isAddingOrUpdating = true;
            });
            _showForm(); // Muestra el formulario de actualización
          },
          onView: _showDetails,
          roles: roles,
          totalRecords: _totalRecords,
          isFilter: true);
    });
  }

  Future<void> _saveUsuario() async {
    if (_token == null) return;
    if (_formKey.currentState?.validate() ?? false) {
      final nombre = _nombreController.text;
      final identificacion = _identificacionController.text;
      final email = _emailController.text;
      final apellidos = _apellidosController.text;
      final telefono = _telefonoController.text;
      final direccion = _direccionController.text;
      final contrasenia = _contraseniaController.text;
      final usuario = _usuarioController.text;

      if (nombre.isEmpty || _selectedRol == null) return;

      final user = Usuario(
          id: _selectedUsuario?.id,
          nombres: nombre,
          estado: 'Activo',
          identificacion: identificacion,
          email: email,
          apellidos: apellidos,
          telefono: telefono,
          direccion: direccion,
          contrasenia: contrasenia,
          nombreUsuario: usuario,
          fechaNacimiento: _selectedFechaNacimiento!,
          roles: _selectedRol);
      try {
        GenericResponseDto<void> response;
        if (_selectedUsuario == null) {
          LoadingDialog.show(context, message: 'Creando nuevo Usuario...');
          response = await _usuarioService.createUsuario(user, _token!);
          LoadingDialog.hide(context);
        } else {
          LoadingDialog.show(context, message: 'Actualizando Usuario...');
          response =
              await _usuarioService.updateUsuario(user.id!, user, _token!);
          LoadingDialog.hide(context);
        }
        Messages.showMessage(context, response.message);
        Navigator.of(context).pop();
        _fetchUsuarios(page: _currentPage, size: _rowsPerPage);
        setState(() {
          _isAddingOrUpdating = false;
          _selectedUsuario = null;
          _selectedRol = [];
        });
      } catch (e) {
        LoadingDialog.hide(context);
        final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
        Messages.showMessage(context, 'Error: $errorMessage',
            isError: true, isTop: true);
      }
    }
  }

  void _selectDateTime(BuildContext context, TextEditingController controller,
      DateTime? selectedDateTime, Function(DateTime) onDateTimeSelected) async {
    // Selección de la fecha
    final DateTime? pickedDate = await showDatePicker(
      context: context,
      initialDate: selectedDateTime ?? DateTime.now(),
      firstDate: DateTime(1950),
      lastDate: DateTime(2099),
    );

    if (pickedDate != null) {
      // Selección de la hora
      final TimeOfDay? pickedTime = await showTimePicker(
        context: context,
        initialTime: TimeOfDay.fromDateTime(selectedDateTime ?? DateTime.now()),
      );

      if (pickedTime != null) {
        // Combina la fecha y la hora seleccionadas
        final DateTime combinedDateTime = DateTime(
          pickedDate.year,
          pickedDate.month,
          pickedDate.day,
          pickedTime.hour,
          pickedTime.minute,
        );

        // Formatea la fecha y hora en el formato deseado
        final formattedDateTime =
            DateFormat('yyyy-MM-dd HH:mm:ss').format(combinedDateTime);
        // Actualiza el controlador de texto y llama al callback
        controller.text = formattedDateTime;
        onDateTimeSelected(combinedDateTime);
      }
    }
  }

  String _getRoleNames(List<int> roleIds) {
    return roleIds.map((id) => _rolesMap[id] ?? 'Desconocido').join(', ');
  }

  Future<void> _deleteUsuario(int id) async {
    GenericResponseDto<void> response;
    LoadingDialog.show(context, message: 'Eliminando Usuario...');
    if (_token == null) return;
    try {
      response = await _usuarioService.deleteUsuario(id, _token!);
      LoadingDialog.hide(context);
      _fetchUsuarios(page: _currentPage, size: _rowsPerPage);
      Messages.showMessage(context, response.message);
    } catch (e) {
      LoadingDialog.hide(context);
      final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
      Messages.showMessage(context, 'Error: $errorMessage',
          isError: true, isTop: true);
    }
  }

  void _showDetails(Usuario user) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16.0), // Bordes redondeados
          ),
          title: Text(
            'Detalles',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          content: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildDetailRow('ID:', user.id.toString()),
                _buildDetailRow('Nombres:', user.nombres),
                _buildDetailRow('Apellidos:', user.apellidos),
                _buildDetailRow('Cédula:', user.identificacion),
                _buildDetailRow('Email:', user.email),
                _buildDetailRow('Nombre de usuario:', user.nombreUsuario),
                _buildDetailRow('Fe. de Nacimiento:',
                    user.fechaNacimiento.toIso8601String()),
                _buildDetailRow('Teléfono:', user.telefono),
                _buildDetailRow('Dirección:', user.direccion),
                _buildDetailRow('Roles:', _getRoleNames(user.roles!)),
                _buildDetailRow('Estado:', user.estado),
                _buildDetailRow('Fe. Creación:', user.fechaCreacion.toString()),
                _buildDetailRow(
                    'Fe. Modificación:', user.fechaModificacion.toString()),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                'Cerrar',
                style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  Widget _buildDetailRow(String title, String value) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(
              text: '$title ',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16,
                color: Colors.black,
              ),
            ),
            TextSpan(
              text: value,
              style: TextStyle(
                fontSize: 16,
                color: Colors.black,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _showForm() {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return Padding(
          padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      _selectedUsuario == null
                          ? 'Crear Usuario'
                          : 'Actualizar Usuario',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 10),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            controller: _nombreController,
                            maxLength: 30,
                            decoration: InputDecoration(
                              labelText: 'Nombres',
                              border: OutlineInputBorder(),
                              counterText: '',
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Ingresa los nombres';
                              }
                              return null;
                            },
                          ),
                        ),
                        SizedBox(width: 10), // Espacio entre los TextField
                        Expanded(
                          child: TextFormField(
                            maxLength: 30,
                            controller: _apellidosController,
                            decoration: InputDecoration(
                              labelText: 'Apellidos',
                              counterText: '',
                              border: OutlineInputBorder(),
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Ingresa los apellidos';
                              }
                              return null;
                            },
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            controller: _identificacionController,
                            maxLength: 10,
                            decoration: InputDecoration(
                              labelText: 'Cédula',
                              border: OutlineInputBorder(),
                              counterText: '',
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Ingresa la cédula';
                              }
                              return null;
                            },
                            keyboardType: TextInputType.number,
                          ),
                        ),
                        SizedBox(width: 10), // Espacio entre los TextField
                        Expanded(
                          child: TextFormField(
                            maxLength: 30,
                            controller: _emailController,
                            decoration: InputDecoration(
                              labelText: 'Correo elecrónico',
                              counterText: '',
                              border: OutlineInputBorder(),
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Ingresa el correo';
                              }
                              final emailRegex =
                                  RegExp(r'^[^@]+@[^@]+\.[^@]+$');
                              if (!emailRegex.hasMatch(value)) {
                                return 'Por favor ingresa un correo electrónico válido';
                              }
                              return null;
                            },
                            keyboardType: TextInputType.emailAddress,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            controller: _usuarioController,
                            maxLength: 15,
                            decoration: InputDecoration(
                              labelText: 'Nombre de usuario',
                              counterText: '',
                              border: OutlineInputBorder(),
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Ingresa el usuario';
                              }
                              return null;
                            },
                          ),
                        ),
                        SizedBox(width: 10), // Espacio entre los TextField
                        Expanded(
                          child: TextFormField(
                            maxLength: 15,
                            controller: _contraseniaController,
                            decoration: InputDecoration(
                              labelText: 'Contraseña',
                              counterText: '',
                              border: OutlineInputBorder(),
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Ingresa la contraseña';
                              }
                              return null;
                            },
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                    TextFormField(
                      controller: _fechaNacimientoController,
                      decoration: InputDecoration(
                        labelText: 'Fecha de Nacimiento',
                        border: OutlineInputBorder(),
                        suffixIcon: IconButton(
                          icon: Icon(Icons.clear),
                          onPressed: () {
                            _fechaNacimientoController.clear();
                          },
                        ),
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Por favor selecciona la fecha de nacimiento';
                        }
                        return null;
                      },
                      readOnly: true,
                      onTap: () {
                        _selectDateTime(context, _fechaNacimientoController,
                            _selectedFechaNacimiento, (pickedDate) {
                          _selectedFechaNacimiento = pickedDate;
                        });
                      },
                    ),
                    SizedBox(height: 10),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            controller: _telefonoController,
                            maxLength: 10,
                            decoration: InputDecoration(
                              labelText: 'Celular',
                              border: OutlineInputBorder(),
                              counterText: '',
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Ingresa el celular';
                              }
                              return null;
                            },
                            keyboardType: TextInputType.number,
                          ),
                        ),
                        SizedBox(width: 10), // Espacio entre los TextField
                        Expanded(
                          child: TextFormField(
                            maxLength: 100,
                            controller: _direccionController,
                            decoration: InputDecoration(
                              labelText: 'Dirección',
                              counterText: '',
                              border: OutlineInputBorder(),
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Ingresa la dirección';
                              }
                              return null;
                            },
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    DropdownButtonFormField<int>(
                      value: _selectedRol?.isNotEmpty == true
                          ? _selectedRol!.first
                          : null, // Muestra el rol seleccionado
                      decoration: InputDecoration(
                        labelText: 'Rol',
                        border: OutlineInputBorder(),
                      ),
                      items: _rolesMap.entries.map((entry) {
                        return DropdownMenuItem<int>(
                          value: entry.key,
                          child: Text(entry.value),
                        );
                      }).toList(),
                      onChanged: (int? newValue) {
                        setState(() {
                          _selectedRol = [newValue!];
                        });
                      },
                      validator: (value) =>
                          value == null ? 'Por favor seleccione un rol' : null,
                    ),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        ElevatedButton(
                          onPressed: () {
                            _saveUsuario();
                          },
                          child: Text('Guardar'),
                        ),
                        ElevatedButton(
                          onPressed: () {
                            Navigator.of(context).pop(); // Cierra el modal
                            // Limpiar selección de roles si se cancela
                            setState(() {
                              _isAddingOrUpdating = false;
                              _nombreController.clear();
                              _selectedRol = null;
                            });
                          },
                          child: Text('Cancelar'),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: roles.contains(Constants.roleAdmin)
          ? FloatingActionButton(
              onPressed: () {
                setState(() {
                  _isAddingOrUpdating = true;
                  _selectedUsuario = null;
                  _nombreController.clear();
                  _apellidosController.clear();
                  _identificacionController.clear();
                  _emailController.clear();
                  _usuarioController.clear();
                  _contraseniaController.clear();
                  _telefonoController.clear();
                  _direccionController.clear();
                  _fechaNacimientoController.clear();
                  _selectedRol = null;
                  _showForm(); // Muestra el formulario
                });
              },
              child: Icon(Icons.add),
              shape: CircleBorder(), // Asegura que el botón sea redondo
              backgroundColor: Colors.blue, // Color de fondo del botón
            )
          : null,
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: _searchController,
                    decoration: InputDecoration(
                      hintText: 'Buscar por ID o Nombre',
                      border: OutlineInputBorder(),
                      suffixIcon: Icon(Icons.search),
                    ),
                    onChanged: (value) => _filterUsuarios(),
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: PaginatedDataTable(
                    rowsPerPage: _rowsPerPage,
                    availableRowsPerPage: [5, 10, 15],
                    onRowsPerPageChanged: (rowsPerPage) {
                      setState(() {
                        _rowsPerPage = rowsPerPage ?? 5;
                        _currentPage = 1; // Reinicia a la primera página
                      });
                      _fetchUsuarios(page: _currentPage, size: _rowsPerPage);
                    },
                    onPageChanged: (page) {
                      final newPage = page ~/ _rowsPerPage + 1;
                      if (newPage != _currentPage) {
                        setState(() {
                          _currentPage = newPage;
                        });
                        _fetchUsuarios(page: _currentPage, size: _rowsPerPage);
                      }
                    },
                    columns: [
                      DataColumn(label: Center(child: Text('ID'))),
                      DataColumn(label: Center(child: Text('Nombres'))),
                      DataColumn(label: Center(child: Text('Email'))),
                      DataColumn(label: Center(child: Text('Acciones'))),
                    ],
                    source: _source
                    // UsuarioDataSource(
                    //     context: context,
                    //     usuarios: _filteredUsuarios,
                    //     onDelete: _deleteUsuario,
                    //     onEdit: (Usuario usuario) {
                    //       setState(() {
                    //         _selectedUsuario = usuario;
                    //         _nombreController.text = usuario.nombres;
                    //         _apellidosController.text = usuario.apellidos;
                    //         _identificacionController.text =
                    //             usuario.identificacion;
                    //         _emailController.text = usuario.email;
                    //         _usuarioController.text = usuario.nombreUsuario;
                    //         _contraseniaController.text = usuario.contrasenia;
                    //         _telefonoController.text = usuario.telefono;
                    //         _direccionController.text = usuario.direccion;
                    //         _fechaNacimientoController.text =
                    //             usuario.fechaNacimiento.toString();
                    //         _selectedFechaNacimiento = usuario.fechaNacimiento;
                    //         _selectedRol = usuario.roles;
                    //         _isAddingOrUpdating = true;
                    //       });
                    //       _showForm(); // Muestra el formulario de actualización
                    //     },
                    //     onView: _showDetails,
                    //     roles: roles,
                    //     totalRecords: _totalRecords),
                    ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class UsuarioDataSource extends DataTableSource {
  final BuildContext context;
  final List<Usuario> usuarios;
  final Function(int) onDelete;
  final Function(Usuario) onEdit;
  final Function(Usuario) onView;
  final List<String> roles;
  final int totalRecords;
  bool isFilter;

  UsuarioDataSource({
    required this.context,
    required this.usuarios,
    required this.onDelete,
    required this.onEdit,
    required this.onView,
    required this.roles,
    required this.totalRecords,
    this.isFilter = false,
  });

  @override
  DataRow? getRow(int index) {
    final d;
    if (!usuarios.isEmpty || index < usuarios.length) {
      if (isFilter) {
        // Aquí puedes aplicar una lógica distinta si la propiedad isFilter es true
        if (usuarios.isEmpty || index >= usuarios.length) {
          return DataRow(cells: [
            DataCell(Center(child: Text(''))),
            DataCell
                .empty, // Celdas vacías para mantener la estructura de la tabla
            DataCell.empty,
            DataCell.empty,
          ]);
        } else {
          final d = usuarios[index];
        }
      }
      int adjustedIndex = index % usuarios.length;
      d = usuarios[adjustedIndex];
    } else {
      return DataRow(cells: [
        DataCell(Center(child: Text(''))),
        DataCell.empty, // Celdas vacías para mantener la estructura de la tabla
        DataCell.empty,
        DataCell.empty,
      ]);
    }
    return DataRow(cells: [
      DataCell(Center(child: Text(d.id.toString()))),
      DataCell(Center(child: Text('${d.nombres} ${d.apellidos}'))),
      DataCell(Center(child: Text(d.estado))),
      DataCell(Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: _buildActionButtons(d),
      )),
    ]);
  }

  List<Widget> _buildActionButtons(Usuario user) {
    List<Widget> buttons = [];

    // Mostrar el botón de detalles a todos los roles
    buttons.add(
      IconButton(
        icon: Icon(Icons.visibility),
        onPressed: () => onView(user),
      ),
    );

    // Mostrar el botón de edición solo para ADMIN
    if (roles.contains(Constants.roleAdmin)) {
      buttons.add(
        IconButton(
          icon: Icon(Icons.edit),
          onPressed: () => onEdit(user),
        ),
      );
    }

    // Mostrar el botón de eliminación solo para ADMIN
    if (roles.contains(Constants.roleAdmin)) {
      buttons.add(
        IconButton(
          icon: Icon(Icons.delete),
          onPressed: () {
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text('Confirmación'),
                  content: Text(
                      '¿Estás seguro de que quieres eliminar este Usuario?'),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text('Cancelar'),
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                        onDelete(user.id!);
                      },
                      child: Text('Eliminar'),
                    ),
                  ],
                );
              },
            );
          },
        ),
      );
    }

    return buttons;
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => totalRecords;

  @override
  int get selectedRowCount => 0;

  @override
  void selectAll(bool checked) {}
}
