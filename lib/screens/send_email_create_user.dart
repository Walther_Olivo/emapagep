import 'package:emapagep/screens/login.dart';
import 'package:emapagep/widgets/loading_dialog.dart';
import 'package:flutter/material.dart';
import 'package:emapagep/services/auth_service.dart';
import 'package:emapagep/widgets/messages.dart';
import 'package:emapagep/dto/validation_code_dto.dart';
import 'package:emapagep/dto/generic_response_dto.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class ValidEmailRegisterScreen extends StatefulWidget {
  final String email;

  ValidEmailRegisterScreen({required this.email});

  @override
  _ValidEmailRegisterState createState() => _ValidEmailRegisterState();
}

class _ValidEmailRegisterState extends State<ValidEmailRegisterScreen> {
  final _formKey = GlobalKey<FormState>();
  final _codeController = TextEditingController();
  final AuthService _authService = AuthService(FlutterSecureStorage());

  @override
  void dispose() {
    _codeController.dispose();
    super.dispose();
  }

  Future<void> _validateCode() async {
    if (_formKey.currentState?.validate() ?? false) {
      LoadingDialog.show(context, message: 'Comprobando código...');
      final validationDto = ValidationCodeDto(
        email: widget.email,
        code: _codeController.text,
        purpose: 'user-registration',
      );
      GenericResponseDto<String> response;
      try {
        response = await _authService.validateCode(validationDto);
        if (response.code == 201) {
          LoadingDialog.hide(context);
          Messages.showMessage(context, 'Usuario registrado exitosamente!');
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => LoginRegisterScreen(),
            ),
          );
        } else {
          LoadingDialog.hide(context);
          Messages.showMessage(context, response.payload, isError: true);
        }
      } catch (e) {
        LoadingDialog.hide(context);
        final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
        Messages.showMessage(context, 'Verificación fallida: $errorMessage',
            isError: true);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Verificación por email'),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(
                context); // Simplemente regresar a la pantalla anterior
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: Image.asset(
                  'assets/candado.png',
                  width: 150,
                  height: 150,
                ),
              ),
              SizedBox(height: 20),
              Text(
                'Hemos enviado un código de verificación a ${widget.email}, introduce el código en el recuadro inferior.',
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 20),
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    TextFormField(
                      controller: _codeController,
                      keyboardType: TextInputType.number,
                      maxLength: 6,
                      decoration: InputDecoration(
                        labelText: 'Código de verificación',
                        border: OutlineInputBorder(),
                        counterText: '',
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Por favor ingresa el código de verificación';
                        }
                        if (!RegExp(r'^\d+$').hasMatch(value)) {
                          return 'El código de verificación debe ser un número';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 20),
                    MaterialButton(
                      onPressed: _validateCode,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      color: Colors.blueAccent,
                      padding: const EdgeInsets.symmetric(
                        horizontal: 80,
                        vertical: 15,
                      ),
                      child: const Text(
                        'Enviar código',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
