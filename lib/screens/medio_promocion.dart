import 'package:emapagep/config/constants.dart';
import 'package:emapagep/dto/generic_response_dto.dart';
import 'package:emapagep/models/medio_promocion.dart';
import 'package:emapagep/services/medio_promocion_service.dart';
import 'package:emapagep/widgets/loading_dialog.dart';
import 'package:flutter/material.dart';
import 'package:emapagep/widgets/messages.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:jwt_decoder/jwt_decoder.dart';

class MedioPromocionScreen extends StatefulWidget {
  @override
  _MedioPromocionScreenState createState() => _MedioPromocionScreenState();
}

class _MedioPromocionScreenState extends State<MedioPromocionScreen> {
  final TextEditingController _nombreController = TextEditingController();
  final TextEditingController _descripcionController = TextEditingController();
  final TextEditingController _searchController = TextEditingController();
  final MedioPromocionService _medioPromocionService = MedioPromocionService();
  List<MedioPromocion> _medioPromociones = [];
  List<MedioPromocion> _filteredMedioPromociones = [];
  MedioPromocion? _selectedMedioPromocion;
  bool _isAddingOrUpdating = false;
  final storage = FlutterSecureStorage();
  List<String> roles = [];
  String? _token;
  final _formKey = GlobalKey<FormState>();
  int _currentPage = 1;
  int _totalRecords = 0;
  int _rowsPerPage = 5;

  @override
  void initState() {
    super.initState();
    _loadRolesAndToken();
  }

  Future<void> _loadRolesAndToken() async {
    String? token = await storage.read(key: 'auth_token');
    if (token != null) {
      Map<String, dynamic> decodedToken = JwtDecoder.decode(token);
      setState(() {
        roles = List<String>.from(
            decodedToken['roles'].map((role) => role['name']));
        _token = token;
      });
      _fetchMedioPromociones(page: _currentPage, size: _rowsPerPage);
    }
  }

  Future<void> _fetchMedioPromociones({int page = 1, int size = 5}) async {
    if (_token == null) return;
    LoadingDialog.show(context, message: 'Cargando Medios de Promoción...');
    try {
      final response = await _medioPromocionService.getAllMedioPromociones(
        _token!,
        page: page,
        size: size,
      );
      final medioPromociones = response.payload;
      setState(() {
        _medioPromociones = medioPromociones;
        _filteredMedioPromociones =
            medioPromociones; // Initialize filtered list
        _totalRecords = response.totalRecords ??
            medioPromociones.length; // Ensure totalRecords is set
        _currentPage = page; // Update current page
        _rowsPerPage = size; // Update rows per page
        LoadingDialog.hide(context);
      });
    } catch (e) {
      LoadingDialog.hide(context);
      final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
      Messages.showMessage(context, 'Error: $errorMessage',
          isError: true, isTop: true);
    }
  }

  void _filterMedioPromocions() {
    final query = _searchController.text.toLowerCase();
    final filteredList = _medioPromociones.where((medioPromocion) {
      return medioPromocion.nombre.toLowerCase().contains(query) ||
          medioPromocion.id.toString().contains(query);
    }).toList();

    setState(() {
      _filteredMedioPromociones = filteredList;
      _totalRecords =
          filteredList.length; // Update total records to filtered count
      if (_currentPage > (filteredList.length / _rowsPerPage).ceil()) {
        // If current page exceeds the number of available pages, go back to the last page
        _currentPage = (filteredList.length / _rowsPerPage).ceil();
      }
    });
  }

  Future<void> _saveMedioPromocion() async {
    if (_token == null) return;
    if (_formKey.currentState?.validate() ?? false) {
      final nombre = _nombreController.text;
      if (nombre.isEmpty) return;

      final medioPromocion = MedioPromocion(
        id: _selectedMedioPromocion?.id,
        nombre: nombre,
        estado: 'Activo',
        descripcion: _descripcionController.text,
      );

      try {
        GenericResponseDto<void> response;
        if (_selectedMedioPromocion == null) {
          LoadingDialog.show(context, message: 'Creando Medio de Promoción...');
          response = await _medioPromocionService.createMedioPromocion(
              medioPromocion, _token!);
          LoadingDialog.hide(context);
        } else {
          LoadingDialog.show(context,
              message: 'Actualizando Medio de Promoción...');
          response = await _medioPromocionService.updateMedioPromocion(
              medioPromocion.id!, medioPromocion, _token!);
          LoadingDialog.hide(context);
        }
        Messages.showMessage(context, response.message);
        Navigator.of(context).pop(); // Close modal
        _fetchMedioPromociones(page: _currentPage, size: _rowsPerPage);
        setState(() {
          _isAddingOrUpdating = false;
          _selectedMedioPromocion = null;
        });
      } catch (e) {
        LoadingDialog.hide(context);
        final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
        Messages.showMessage(context, 'Error: $errorMessage',
            isError: true, isTop: true);
      }
    }
  }

  Future<void> _deleteMedioPromocion(int id) async {
    if (_token == null) return;
    LoadingDialog.show(context, message: 'Eliminando Medio Promoción...');
    try {
      final response =
          await _medioPromocionService.deleteMedioPromocion(id, _token!);
      LoadingDialog.hide(context);
      _fetchMedioPromociones(page: _currentPage, size: _rowsPerPage);
      Messages.showMessage(context, response.message);
    } catch (e) {
      LoadingDialog.hide(context);
      final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
      Messages.showMessage(context, 'Error: $errorMessage',
          isError: true, isTop: true);
    }
  }

  void _showDetails(MedioPromocion medioPromocion) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16.0), // Rounded corners
          ),
          title: Text(
            'Detalles',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          content: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildDetailRow('ID:', medioPromocion.id.toString()),
                _buildDetailRow('Nombre:', medioPromocion.nombre),
                _buildDetailRow(
                    'Descripción:', medioPromocion.descripcion ?? 'N/A'),
                _buildDetailRow('Estado:', medioPromocion.estado),
                _buildDetailRow('Fe. Creación:',
                    medioPromocion.fechaCreacion?.toString() ?? 'N/A'),
                _buildDetailRow('Fe. Modificación:',
                    medioPromocion.fechaModificacion?.toString() ?? 'N/A'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                'Cerrar',
                style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  Widget _buildDetailRow(String title, String value) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(
              text: '$title ',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16,
                color: Colors.black,
              ),
            ),
            TextSpan(
              text: value,
              style: TextStyle(
                fontSize: 16,
                color: Colors.black,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _showForm() {
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (BuildContext context) {
        return Padding(
          padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          child: Container(
            padding: EdgeInsets.all(16),
            child: Form(
              key: _formKey,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      _selectedMedioPromocion == null
                          ? 'Agregar Medio de Promoción'
                          : 'Actualizar Medio de Promoción',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 10),
                    TextFormField(
                      controller: _nombreController,
                      maxLength: 25,
                      decoration: InputDecoration(
                        labelText: 'Nombre del Medio Promoción',
                        border: OutlineInputBorder(),
                        counterText: '',
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Por favor ingresa el nombre del Medio de promoción';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 10),
                    TextFormField(
                      controller: _descripcionController,
                      maxLength: 100,
                      decoration: InputDecoration(
                        labelText: 'Descripción del Medio Promoción',
                        border: OutlineInputBorder(),
                        counterText: '',
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Por favor ingresa la descripción del Medio de promoción';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        ElevatedButton(
                          onPressed: () {
                            _saveMedioPromocion();
                          },
                          child: Text('Guardar'),
                        ),
                        ElevatedButton(
                          onPressed: () {
                            Navigator.of(context).pop(); // Close modal
                            setState(() {
                              _isAddingOrUpdating = false;
                              _nombreController.clear();
                              _descripcionController.clear();
                              _selectedMedioPromocion = null;
                            });
                          },
                          child: Text('Cancelar'),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: roles.contains(Constants.roleAdmin)
          ? Padding(
              padding: const EdgeInsets.only(
                  bottom: 60.0), // Adjust this value as needed
              child: FloatingActionButton(
                onPressed: () {
                  setState(() {
                    _isAddingOrUpdating = true;
                    _selectedMedioPromocion = null;
                    _nombreController.clear();
                    _descripcionController.clear();
                    _showForm(); // Show form
                  });
                },
                child: Icon(Icons.add),
                shape: CircleBorder(), // Ensure the button is round
                backgroundColor: Colors.blue, // Button background color
              ),
            )
          : null,
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: _searchController,
                    decoration: InputDecoration(
                      hintText: 'Buscar por ID o Nombre',
                      border: OutlineInputBorder(),
                      suffixIcon: Icon(Icons.search),
                    ),
                    onChanged: (value) => _filterMedioPromocions(),
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: PaginatedDataTable(
                  rowsPerPage: _rowsPerPage,
                  availableRowsPerPage: const [5, 10, 15],
                  onRowsPerPageChanged: (rowsPerPage) {
                    setState(() {
                      _rowsPerPage = rowsPerPage ?? 5;
                      _currentPage = 1; // Reset to first page
                    });
                    _fetchMedioPromociones(
                        page: _currentPage, size: _rowsPerPage);
                  },
                  onPageChanged: (page) {
                    final newPage = page ~/ _rowsPerPage + 1;
                    if (newPage != _currentPage) {
                      setState(() {
                        _currentPage = newPage;
                      });
                      _fetchMedioPromociones(
                          page: _currentPage, size: _rowsPerPage);
                    }
                  },
                  columns: [
                    DataColumn(label: Center(child: Text('ID'))),
                    DataColumn(label: Center(child: Text('Nombre'))),
                    DataColumn(label: Center(child: Text('Estado'))),
                    DataColumn(label: Center(child: Text('Acciones'))),
                  ],
                  source: MedioPromocionDataSource(
                      context: context,
                      medioPromociones: _filteredMedioPromociones,
                      onDelete: _deleteMedioPromocion,
                      onEdit: (MedioPromocion medioPromocion) {
                        setState(() {
                          _selectedMedioPromocion = medioPromocion;
                          _nombreController.text = medioPromocion.nombre;
                          _descripcionController.text =
                              medioPromocion.descripcion ?? '';
                          _isAddingOrUpdating = true;
                        });
                        _showForm(); // Show update form
                      },
                      onView: _showDetails,
                      roles: roles,
                      totalRecords: _totalRecords),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MedioPromocionDataSource extends DataTableSource {
  final BuildContext context;
  final List<MedioPromocion> medioPromociones;
  final Function(int) onDelete;
  final Function(MedioPromocion) onEdit;
  final Function(MedioPromocion) onView;
  final List<String> roles;
  final int totalRecords;

  MedioPromocionDataSource({
    required this.context,
    required this.medioPromociones,
    required this.onDelete,
    required this.onEdit,
    required this.onView,
    required this.roles,
    required this.totalRecords,
  });

  @override
  DataRow? getRow(int index) {
    if (index >= medioPromociones.length) return null;
    final medioPromocion = medioPromociones[index];
    final d;
    if (!medioPromociones.isEmpty || index < medioPromociones.length) {
      // if (isFilter) {
      // Aquí puedes aplicar una lógica distinta si la propiedad isFilter es true
      // if (detalleParametros.isEmpty || index >= detalleParametros.length) {
      //   return null; // No hay coincidencias o estamos fuera de rango
      // } else {
      // final d = detalleParametros[index];
      // }
      // }
      int adjustedIndex = index % medioPromociones.length;
      d = medioPromociones[adjustedIndex];
    } else {
      return null; // No hay coincidencias o estamos fuera de rango
    }
    return DataRow(cells: [
      DataCell(Center(child: Text(medioPromocion.id.toString()))),
      DataCell(Center(child: Text(medioPromocion.nombre))),
      DataCell(Center(child: Text(medioPromocion.estado))),
      DataCell(Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: _buildActionButtons(medioPromocion),
      )),
    ]);
  }

  List<Widget> _buildActionButtons(MedioPromocion medioPromocion) {
    List<Widget> buttons = [];

    // Show detail button for all roles
    buttons.add(
      IconButton(
        icon: Icon(Icons.visibility),
        onPressed: () => onView(medioPromocion),
      ),
    );

    // Show edit and delete buttons only for ADMIN role
    if (roles.contains(Constants.roleAdmin)) {
      buttons.add(
        IconButton(
          icon: Icon(Icons.edit),
          onPressed: () => onEdit(medioPromocion),
        ),
      );

      buttons.add(
        IconButton(
          icon: Icon(Icons.delete),
          onPressed: () {
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text('Confirmación'),
                  content: Text(
                      '¿Estás seguro de que quieres eliminar este Medio de Promoción?'),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text('Cancelar'),
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                        onDelete(medioPromocion.id!);
                      },
                      child: Text('Eliminar'),
                    ),
                  ],
                );
              },
            );
          },
        ),
      );
    }

    return buttons;
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => totalRecords;

  @override
  int get selectedRowCount => 0;

  @override
  void selectAll(bool checked) {}
}
