import 'dart:convert';
import 'dart:io';
import 'package:emapagep/dto/generic_response_dto.dart';
import 'package:emapagep/dto/login_dto.dart';
import 'package:emapagep/models/usuario.dart';
import 'package:emapagep/screens/drawer_options.dart';
import 'package:emapagep/screens/reset_password.dart';
import 'package:emapagep/screens/send_email_create_user.dart';
import 'package:emapagep/services/auth_service.dart';
import 'package:emapagep/widgets/messages.dart';
import 'package:emapagep/widgets/loading_dialog.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';

class LoginRegisterScreen extends StatefulWidget {
  @override
  _LoginRegisterScreenState createState() => _LoginRegisterScreenState();
}

class _LoginRegisterScreenState extends State<LoginRegisterScreen> {
  bool _isLogin = true; // Estado para alternar entre Login y Registro
  final AuthService _authService = AuthService(FlutterSecureStorage());

  final TextEditingController _identifierController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _identificacionController =
      TextEditingController();
  final TextEditingController _nombresController = TextEditingController();
  final TextEditingController _apellidosController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _usuarioController = TextEditingController();
  final TextEditingController _contraseniaController = TextEditingController();
  final TextEditingController _telefonoController = TextEditingController();
  final TextEditingController _direccionController = TextEditingController();
  final TextEditingController _imageFileNameController =
      TextEditingController();
  DateTime? _selectedFechaNacimiento;
  final TextEditingController _fechaNacimientoController =
      TextEditingController();
  String email = "";
  File? _imageFile;
  bool _obscureTextLogin = true;
  bool _obscureTextLoginRegister = true;
  final GlobalKey<FormState> _loginFormKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _registerFormKey = GlobalKey<FormState>();
  bool _isRegistered = false;
  Future<void> _pickImageFile() async {
    final result = await FilePicker.platform.pickFiles(type: FileType.image);
    if (result != null) {
      setState(() {
        _imageFile = File(result.files.single.path!);
        _imageFileNameController.text = result.files.single.name;
      });
    }
  }

  void _clearImageFile() {
    setState(() {
      _imageFile = null;
      _imageFileNameController.clear();
    });
  }

  Future<void> _login() async {
    final identifier = _identifierController.text;
    final password = _passwordController.text;
    if (_loginFormKey.currentState?.validate() ?? false) {
      LoadingDialog.show(context, message: 'Iniciando sesión...');
      if (identifier.isEmpty || password.isEmpty) return;

      final login = LoginDto(
        identifier: identifier,
        contrasenia: password,
      );
      try {
        await _authService.login(login);
        LoadingDialog.hide(context);
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => DrawerOptionsScreen(),
          ),
        );
      } catch (e) {
        LoadingDialog.hide(context);
        final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
        Messages.showMessage(context, 'Login fallido: $errorMessage',
            isError: true);
      }
    }
  }

  Future<bool> _register() async {
    String? base64Image;
    if (_registerFormKey.currentState?.validate() ?? false) {
      LoadingDialog.show(context, message: 'Creando Cuenta...');

      if (_imageFile != null) {
        final imageBytes = await _imageFile!.readAsBytes();
        base64Image = base64Encode(imageBytes);
      }

      final nombre = _nombresController.text;
      final identificacion = _identificacionController.text;
      email = _emailController.text;
      final apellidos = _apellidosController.text;
      final telefono = _telefonoController.text;
      final direccion = _direccionController.text;
      final contrasenia = _contraseniaController.text;
      final usuario = _usuarioController.text;

      if (nombre.isEmpty) return false;

      final user = Usuario(
        nombres: nombre,
        estado: 'Activo',
        identificacion: identificacion,
        email: email,
        apellidos: apellidos,
        telefono: telefono,
        direccion: direccion,
        contrasenia: contrasenia,
        nombreUsuario: usuario,
        foto: base64Image,
        fechaNacimiento: _selectedFechaNacimiento!,
        roles: [],
      );

      try {
        final response = await _authService.register(user);
        LoadingDialog.hide(context);

        if (response.code == 201) {
          Messages.showMessage(context, response.message);
          _identificacionController.clear();
          _nombresController.clear();
          _apellidosController.clear();
          _emailController.clear();
          _usuarioController.clear();
          _contraseniaController.clear();
          _telefonoController.clear();
          _direccionController.clear();
          _imageFileNameController.clear();
          _fechaNacimientoController.clear();
          //_selectedFechaNacimiento = null;
          _imageFile = null;

          return true; // Registro exitoso
        } else {
          Messages.showMessage(context, response.message, isError: true);
          return false; // Registro fallido
        }
      } catch (e) {
        LoadingDialog.hide(context);
        final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
        Messages.showMessage(context, 'Usuario no registrado: $errorMessage',
            isError: true);
        return false; // Error en el registro
      }
    }
    return false; // Validación del formulario fallida
  }

  void _selectDateTime(BuildContext context, TextEditingController controller,
      DateTime? selectedDateTime, Function(DateTime) onDateTimeSelected) async {
    // Selección de la fecha
    final DateTime? pickedDate = await showDatePicker(
      context: context,
      initialDate: selectedDateTime ?? DateTime.now(),
      firstDate: DateTime(1950),
      lastDate: DateTime(2099),
    );

    if (pickedDate != null) {
      // Selección de la hora
      final TimeOfDay? pickedTime = await showTimePicker(
        context: context,
        initialTime: TimeOfDay.fromDateTime(selectedDateTime ?? DateTime.now()),
      );

      if (pickedTime != null) {
        // Combina la fecha y la hora seleccionadas
        final DateTime combinedDateTime = DateTime(
          pickedDate.year,
          pickedDate.month,
          pickedDate.day,
          pickedTime.hour,
          pickedTime.minute,
        );

        // Formatea la fecha y hora en el formato deseado
        final formattedDateTime =
            DateFormat('yyyy-MM-dd HH:mm:ss').format(combinedDateTime);

        // Actualiza el controlador de texto y llama al callback
        controller.text = formattedDateTime;
        onDateTimeSelected(combinedDateTime);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        height: double.infinity,
        child: Stack(
          children: [
            containerMethod(size),
            logoImage(),
            _isLogin ? loginForm(context) : registerForm(context),
          ],
        ),
      ),
    );
  }

  SingleChildScrollView loginForm(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          const SizedBox(height: 250),
          Container(
            padding: const EdgeInsets.all(20),
            margin: const EdgeInsets.symmetric(horizontal: 31),
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(25),
              boxShadow: const [
                BoxShadow(
                  color: Colors.black12,
                  blurRadius: 15,
                  offset: Offset(0, 5),
                )
              ],
            ),
            child: Column(
              children: [
                const SizedBox(height: 10),
                Text(
                  'Iniciar sesión',
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
                const SizedBox(height: 20),
                Form(
                  key: _loginFormKey,
                  child: Column(
                    children: [
                      TextFormField(
                        controller: _identifierController,
                        maxLength: 30,
                        autocorrect: false,
                        decoration: InputDecoration(
                          hintText: 'dev12 or dev@gmail.com',
                          labelText: 'Usuario o email',
                          counterText: '',
                          border: OutlineInputBorder(),
                          prefixIcon: const Icon(Icons.verified_user_rounded),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Por favor ingresa tu usuario o email';
                          }
                          return null;
                        },
                      ),
                      const SizedBox(height: 20),
                      TextFormField(
                        controller: _passwordController,
                        maxLength: 15,
                        obscureText: _obscureTextLogin,
                        autocorrect: false,
                        decoration: InputDecoration(
                          hintText: 'krod2024',
                          labelText: 'Contraseña',
                          counterText: '',
                          border: OutlineInputBorder(),
                          prefixIcon: const Icon(Icons.password_rounded),
                          suffixIcon: IconButton(
                            icon: Icon(
                              _obscureTextLogin
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                            ),
                            onPressed: () {
                              setState(() {
                                _obscureTextLogin =
                                    !_obscureTextLogin; // Alterna la visibilidad
                              });
                            },
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Por favor ingresa tu contraseña';
                          }
                          return null;
                        },
                      ),
                      const SizedBox(height: 10),
                      Align(
                        alignment: Alignment.centerRight,
                        child: TextButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    RecuperarContraseniaScreen(),
                              ),
                            );
                          },
                          child: Text(
                            '¿Olvidaste tu contraseña?',
                            style: TextStyle(
                              color: Colors.blue,
                              decoration: TextDecoration.underline,
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(height: 10),
                      MaterialButton(
                        onPressed: () {
                          if (_loginFormKey.currentState!.validate()) {
                            _login();
                          }
                        },
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        color: Colors.blueAccent,
                        padding: const EdgeInsets.symmetric(
                          horizontal: 80,
                          vertical: 15,
                        ),
                        child: const Text(
                          'Continuar',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      const SizedBox(height: 20),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Flexible(
                            child: Text(
                              'No tienes una cuenta? ',
                              style: TextStyle(color: Colors.black54),
                            ),
                          ),
                          TextButton(
                            onPressed: () {
                              _loginFormKey.currentState?.reset();
                              setState(() {
                                _isLogin =
                                    false; // Muestra el formulario de registro
                              });
                            },
                            child: const Text(
                              'Regístrate aquí',
                              style: TextStyle(color: Colors.blueAccent),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          const SizedBox(height: 50),
        ],
      ),
    );
  }

  SingleChildScrollView registerForm(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          const SizedBox(height: 250),
          Container(
            padding: const EdgeInsets.all(20),
            margin: const EdgeInsets.symmetric(horizontal: 31),
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(25),
              boxShadow: const [
                BoxShadow(
                  color: Colors.black12,
                  blurRadius: 15,
                  offset: Offset(0, 5),
                )
              ],
            ),
            child: Column(
              children: [
                const SizedBox(height: 10),
                Text(
                  'Registro',
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
                const SizedBox(height: 20),
                Form(
                  key: _registerFormKey,
                  child: Column(
                    children: [
                      TextFormField(
                        controller: _identificacionController,
                        keyboardType: TextInputType.number,
                        maxLength: 10,
                        decoration: InputDecoration(
                          counterText: '',
                          hintText: '1234567890',
                          labelText: 'Cédula',
                          border: OutlineInputBorder(),
                          prefixIcon: const Icon(Icons.card_membership),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Por favor ingresa tu cédula';
                          }
                          return null;
                        },
                      ),
                      const SizedBox(height: 10),
                      TextFormField(
                        controller: _nombresController,
                        maxLength: 30,
                        decoration: InputDecoration(
                          hintText: 'Juan',
                          labelText: 'Nombres',
                          counterText: '',
                          border: OutlineInputBorder(),
                          prefixIcon: const Icon(Icons.person),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Por favor ingresa tus nombres';
                          }
                          return null;
                        },
                      ),
                      const SizedBox(height: 10),
                      TextFormField(
                        controller: _apellidosController,
                        maxLength: 30,
                        decoration: InputDecoration(
                          hintText: 'Pérez',
                          labelText: 'Apellidos',
                          counterText: '',
                          border: OutlineInputBorder(),
                          prefixIcon: const Icon(Icons.person),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Por favor ingresa tus apellidos';
                          }
                          return null;
                        },
                      ),
                      const SizedBox(height: 10),
                      TextFormField(
                        controller: _emailController,
                        maxLength: 30,
                        decoration: InputDecoration(
                          hintText: 'juan.perez@example.com',
                          labelText: 'Email',
                          counterText: '',
                          border: OutlineInputBorder(),
                          prefixIcon: const Icon(Icons.email),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Por favor ingresa tu correo';
                          }
                          final emailRegex = RegExp(r'^[^@]+@[^@]+\.[^@]+$');
                          if (!emailRegex.hasMatch(value)) {
                            return 'Por favor ingresa un correo electrónico válido';
                          }
                          return null;
                        },
                      ),
                      const SizedBox(height: 10),
                      TextFormField(
                        controller: _usuarioController,
                        maxLength: 15,
                        decoration: InputDecoration(
                          hintText: 'usuario123',
                          labelText: 'Usuario',
                          counterText: '',
                          border: OutlineInputBorder(),
                          prefixIcon: const Icon(Icons.account_circle),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Por favor ingresa tu nombre de usuario';
                          }
                          return null;
                        },
                      ),
                      const SizedBox(height: 10),
                      TextFormField(
                        controller: _contraseniaController,
                        maxLength: 15,
                        obscureText: _obscureTextLoginRegister,
                        decoration: InputDecoration(
                          hintText: 'contraseña123',
                          counterText: '',
                          labelText: 'Contraseña',
                          border: OutlineInputBorder(),
                          prefixIcon: const Icon(Icons.password),
                          suffixIcon: IconButton(
                            icon: Icon(
                              _obscureTextLoginRegister
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                            ),
                            onPressed: () {
                              setState(() {
                                _obscureTextLoginRegister =
                                    !_obscureTextLoginRegister; // Alterna la visibilidad
                              });
                            },
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Por favor ingresa tu contraseña';
                          }
                          return null;
                        },
                      ),
                      const SizedBox(height: 10),
                      TextFormField(
                        controller: _telefonoController,
                        maxLength: 10,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          hintText: '0934567890',
                          labelText: 'Celular',
                          counterText: '',
                          border: OutlineInputBorder(),
                          prefixIcon: const Icon(Icons.phone),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Por favor ingresa tu número de celular';
                          }
                          return null;
                        },
                      ),
                      const SizedBox(height: 10),
                      TextFormField(
                        controller: _fechaNacimientoController,
                        decoration: InputDecoration(
                          labelText: 'Fecha de Nacimiento',
                          border: OutlineInputBorder(),
                          prefixIcon: const Icon(Icons.date_range),
                          suffixIcon: IconButton(
                            icon: Icon(Icons.clear),
                            onPressed: () {
                              _fechaNacimientoController.clear();
                            },
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Por favor ingresa tu fecha de nacimiento';
                          }
                          return null;
                        },
                        readOnly: true,
                        onTap: () {
                          _selectDateTime(context, _fechaNacimientoController,
                              _selectedFechaNacimiento, (pickedDate) {
                            _selectedFechaNacimiento = pickedDate;
                          });
                        },
                      ),
                      const SizedBox(height: 10),
                      TextFormField(
                        controller: _direccionController,
                        maxLength: 100,
                        decoration: InputDecoration(
                          hintText: 'Dirección',
                          labelText: 'Dirección',
                          border: OutlineInputBorder(),
                          counterText: '',
                          prefixIcon: const Icon(Icons.home),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Por favor ingresa tu dirección';
                          }
                          return null;
                        },
                      ),
                      const SizedBox(height: 10),
                      Row(
                        children: [
                          Expanded(
                            child: TextField(
                              controller: _imageFileNameController,
                              decoration: InputDecoration(
                                labelText: 'Foto',
                                prefixIcon: const Icon(Icons.image),
                                border: OutlineInputBorder(),
                                hintText: 'Selecciona una foto',
                                suffixIcon: _imageFile == null
                                    ? IconButton(
                                        icon: Icon(Icons.attach_file),
                                        onPressed: _pickImageFile,
                                      )
                                    : IconButton(
                                        icon: Icon(Icons.clear),
                                        onPressed: _clearImageFile,
                                      ),
                              ),
                              readOnly: true,
                            ),
                          ),
                          if (_imageFile != null)
                            SizedBox(
                              width: 100,
                              height: 100,
                              child: Image.file(_imageFile!),
                            ),
                        ],
                      ),
                      const SizedBox(height: 30),
                      MaterialButton(
                        onPressed: () async {
                          if (_registerFormKey.currentState?.validate() ??
                              false) {
                            bool registrationSuccessful = await _register();
                            if (registrationSuccessful) {
                              email;
                              if (email.isEmpty) {
                                Messages.showMessage(
                                    context, 'El email no puede estar vacío',
                                    isError: true);
                                return;
                              }
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      ValidEmailRegisterScreen(email: email),
                                ),
                              );
                            }
                          }
                        },
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        color: Colors.blueAccent,
                        padding: const EdgeInsets.symmetric(
                          horizontal: 80,
                          vertical: 15,
                        ),
                        child: const Text(
                          'Registrarme',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      const SizedBox(height: 20),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Flexible(
                            child: Text(
                              'Ya tienes una cuenta? ',
                              style: TextStyle(color: Colors.black54),
                            ),
                          ),
                          TextButton(
                            onPressed: () {
                              _registerFormKey.currentState?.reset();
                              setState(() {
                                _isLogin =
                                    true; // Muestra el formulario de inicio de sesión
                              });
                            },
                            child: const Text(
                              'Inicia sesión aquí',
                              style: TextStyle(color: Colors.blueAccent),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          const SizedBox(height: 50),
        ],
      ),
    );
  }

  SafeArea logoImage() {
    return SafeArea(
      child: Container(
        width: double.infinity,
        margin: const EdgeInsets.only(top: 30),
        child: Image.asset(
          'assets/emapag-ep.png',
          height: 165,
        ),
      ),
    );
  }

  Container containerMethod(Size size) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Color.fromRGBO(63, 90, 156, 1),
            Color.fromRGBO(90, 70, 178, 1),
          ],
        ),
      ),
      width: double.infinity,
      height: size.height * 0.4,
      child: Stack(
        children: [
          Positioned(child: bubble(), top: 80, left: 30),
          Positioned(child: bubble(), top: -40, left: -30),
          Positioned(child: bubble(), top: -50, right: -20),
          Positioned(child: bubble(), bottom: -50, left: 10),
          Positioned(child: bubble(), top: 120, left: -20),
          Positioned(child: bubble(), top: 10, right: 50),
          Positioned(child: bubble(), top: 90, right: -20),
          Positioned(child: bubble(), bottom: 10, right: 30),
        ],
      ),
    );
  }

  Container bubble() {
    return Container(
      width: 100,
      height: 100,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100),
        color: const Color.fromRGBO(255, 255, 255, 0.05),
      ),
    );
  }
}
