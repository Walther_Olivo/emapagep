import 'package:emapagep/dto/generic_response_dto.dart';
import 'package:emapagep/dto/reclamos_dto.dart';
import 'package:emapagep/services/reclamo_service.dart';
import 'package:emapagep/widgets/loading_dialog.dart';
import 'package:emapagep/widgets/messages.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:jwt_decoder/jwt_decoder.dart';

class ReclamoScreen extends StatefulWidget {
  @override
  _ReclamoScreenState createState() => _ReclamoScreenState();
}

class _ReclamoScreenState extends State<ReclamoScreen> {
  ReclamosDto? _reclamo; // Objeto que guarda el reclamo recuperado
  TextEditingController _searchController = TextEditingController();
  bool _loading = false; // Indicador de carga
  String? _token;
  final storage = FlutterSecureStorage();
  final ReclamoService _reclamoService = ReclamoService();
  int _rowsPerPage = 5; // Número de filas por página

  @override
  void initState() {
    super.initState();
    _loadRolesAndToken();
  }

  Future<void> _loadRolesAndToken() async {
    String? token = await storage.read(key: 'auth_token');
    if (token != null) {
      Map<String, dynamic> decodedToken = JwtDecoder.decode(token);
    }
    setState(() {
      _token = token;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: _searchController,
                    decoration: InputDecoration(
                      hintText: 'Buscar por Número de Contrato',
                      border: OutlineInputBorder(),
                      suffixIcon: IconButton(
                        icon: Icon(Icons.search),
                        onPressed: _buscarReclamo, // Acción de buscar
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
            _loading
                ? CircularProgressIndicator() // Indicador de carga
                : Expanded(
                    child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: PaginatedDataTable(
                        rowsPerPage: _rowsPerPage,
                        availableRowsPerPage: const [5, 10, 15],
                        columns: [
                          DataColumn(
                              label: Center(child: Text('Código Reclamo'))),
                          DataColumn(label: Center(child: Text('Descripción'))),
                          DataColumn(label: Center(child: Text('Acciones'))),
                        ],
                        source: ReclamoDataSource(
                          reclamo: _reclamo,
                          onView: _showDetails,
                        ),
                        onRowsPerPageChanged: (rowsPerPage) {
                          setState(() {
                            _rowsPerPage = rowsPerPage ?? 5;
                          });
                        },
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }

  Future<void> _buscarReclamo() async {
    String numContrato = _searchController.text;
    if (numContrato.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('Ingrese un número de contrato')),
      );
      return;
    }
    GenericResponseDto<ReclamosDto> response;

    LoadingDialog.show(context,
        message: 'Buscando Reclamo...'); // Mostrar loading
    try {
      // Llamada al servicio para obtener los datos del reclamo
      response =
          await _reclamoService.getReclamosByContrato(_token!, numContrato);
      LoadingDialog.hide(context);
      Messages.showMessage(context, response.message);
      setState(() {
        _reclamo = response.payload;
      });
    } catch (e) {
      LoadingDialog.hide(context);
      final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
      Messages.showMessage(context, 'Error: $errorMessage', isError: true);
    }
  }

  void _showDetails(ReclamosDto reclamo) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16.0), // Bordes redondeados
          ),
          title: Text(
            'Detalles',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          content: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildDetailRow('Código del Reclamo: ', reclamo.codigoReclamo),
                _buildDetailRow(
                    'Comentario del Reclamo: ', reclamo.comentarioReclamo),
                SizedBox(height: 10),
                Text(
                  'Acciones',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                ),
                SizedBox(height: 10),
                ...reclamo.detalles.map((detalle) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _buildDetailRow('Acción: ', detalle.descripcionAccion),
                      _buildDetailRow('Estado: ', detalle.estadoDetalle),
                      _buildDetailRow('Fecha: ', detalle.fechaingDetalle),
                      _buildDetailRow('Usuario: ', detalle.usuaringDetalle),
                      SizedBox(height: 10),
                    ],
                  );
                }).toList(),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('Cerrar'),
            ),
          ],
        );
      },
    );
  }
}

Widget _buildDetailRow(String title, String value) {
  return Padding(
    padding: const EdgeInsets.symmetric(vertical: 8.0),
    child: RichText(
      text: TextSpan(
        children: [
          TextSpan(
            text: '$title ',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16,
              color: Colors.black,
            ),
          ),
          TextSpan(
            text: value,
            style: TextStyle(
              fontSize: 16,
              color: Colors.black,
            ),
          ),
        ],
      ),
    ),
  );
}

class ReclamoDataSource extends DataTableSource {
  final ReclamosDto? reclamo;
  final Function(ReclamosDto) onView;

  ReclamoDataSource({
    this.reclamo,
    required this.onView,
  });

  @override
  DataRow? getRow(int index) {
    if (reclamo == null || index != 0) return null;
    return DataRow(cells: [
      DataCell(Text(reclamo!.codigoReclamo)),
      DataCell(Text(reclamo!.comentarioReclamo)),
      DataCell(
        IconButton(
          icon: Icon(Icons.visibility),
          onPressed: () => onView(reclamo!),
        ),
      ),
    ]);
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => reclamo == null ? 0 : 1;

  @override
  int get selectedRowCount => 0;

  @override
  void selectAll(bool checked) {}
}
