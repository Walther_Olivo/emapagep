import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:emapagep/dto/generic_response_dto.dart';
import 'package:emapagep/models/parametro.dart';
import 'package:emapagep/services/parametro_service.dart';
import 'package:emapagep/widgets/loading_dialog.dart';
import 'package:emapagep/widgets/messages.dart';
import 'package:emapagep/config/constants.dart';

class ParametroScreen extends StatefulWidget {
  @override
  _ParametroScreenState createState() => _ParametroScreenState();
}

class _ParametroScreenState extends State<ParametroScreen> {
  final TextEditingController _nombreController = TextEditingController();
  final TextEditingController _descripcionController = TextEditingController();
  final TextEditingController _searchController = TextEditingController();
  final ParametroService _parametroService = ParametroService();
  List<Parametro> _parametros = [];
  List<Parametro> _filteredParametros = [];
  Parametro? _selectedParametro;
  bool _isAddingOrUpdating = false;
  final storage = FlutterSecureStorage();
  List<String> roles = [];
  String? _token;
  final _formKey = GlobalKey<FormState>();
  int _currentPage = 1;
  int _totalRecords = 0;
  int _rowsPerPage = 5;

  @override
  void initState() {
    super.initState();
    _loadRolesAndToken();
  }

  Future<void> _loadRolesAndToken() async {
    String? token = await storage.read(key: 'auth_token');
    if (token != null) {
      Map<String, dynamic> decodedToken = JwtDecoder.decode(token);
      setState(() {
        roles = List<String>.from(
            decodedToken['roles'].map((role) => role['name']));
        _token = token;
      });
      _fetchParametros(page: _currentPage, size: _rowsPerPage);
    }
  }

  Future<void> _fetchParametros({int page = 1, int size = 5}) async {
    if (_token == null) return;
    LoadingDialog.show(context, message: 'Cargando Parámetros...');
    try {
      final response = await _parametroService.getAllParametros(
        _token!,
        page: page,
        size: size,
      );
      final parametros = response.payload;
      setState(() {
        _parametros = [];
        _parametros = parametros;
        _filteredParametros = _parametros;
        _totalRecords = response.totalRecords ?? 0;
        _currentPage = page; // Actualiza la página actual
        _rowsPerPage = size; // Actualiza el tamaño de página
        _filterParametros(); // Asegúrate de filtrar los datos después de la carga
        LoadingDialog.hide(context);
      });
    } catch (e) {
      LoadingDialog.hide(context);
      final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
      Messages.showMessage(context, 'Error: $errorMessage',
          isError: true, isTop: true);
    }
  }

  void _filterParametros() {
    final query = _searchController.text.toLowerCase();
    setState(() {
      _filteredParametros = _parametros.where((parametro) {
        return parametro.nombre.toLowerCase().contains(query) ||
            parametro.id.toString().contains(query);
      }).toList();
    });
  }

  Future<void> _saveParametro() async {
    if (_token == null) return;
    if (_formKey.currentState?.validate() ?? false) {
      final nombre = _nombreController.text;
      if (nombre.isEmpty) return;

      final parametro = Parametro(
        id: _selectedParametro?.id,
        nombre: nombre,
        estado: 'Activo',
        descripcion: _descripcionController.text,
      );

      try {
        GenericResponseDto<void> response;
        if (_selectedParametro == null) {
          LoadingDialog.show(context, message: 'Creando Parámetro...');
          response =
              await _parametroService.createParametro(parametro, _token!);
          LoadingDialog.hide(context);
        } else {
          LoadingDialog.show(context, message: 'Actualizando Parámetro...');
          response = await _parametroService.updateParametro(
              parametro.id!, parametro, _token!);
          LoadingDialog.hide(context);
        }
        Messages.showMessage(context, response.message);
        Navigator.of(context).pop(); // Cierra el modal
        _fetchParametros(page: _currentPage, size: _rowsPerPage);
        setState(() {
          _isAddingOrUpdating = false;
          _selectedParametro = null;
        });
      } catch (e) {
        LoadingDialog.hide(context);
        final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
        Messages.showMessage(context, 'Error: $errorMessage',
            isError: true, isTop: true);
      }
    }
  }

  Future<void> _deleteParametro(int id) async {
    GenericResponseDto<void> response;
    LoadingDialog.show(context, message: 'Eliminando Parámetro...');
    try {
      response = await _parametroService.deleteParametro(id, _token!);
      LoadingDialog.hide(context);
      _fetchParametros(page: _currentPage, size: _rowsPerPage);
      Messages.showMessage(context, response.message);
    } catch (e) {
      LoadingDialog.hide(context);
      final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
      Messages.showMessage(context, 'Error: $errorMessage',
          isError: true, isTop: true);
    }
  }

  void _showDetails(Parametro parametro) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16.0), // Bordes redondeados
          ),
          title: Text(
            'Detalles',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          content: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildDetailRow('ID:', parametro.id.toString()),
                _buildDetailRow('Nombre:', parametro.nombre),
                _buildDetailRow('Descripción:', parametro.descripcion),
                _buildDetailRow('Estado:', parametro.estado),
                _buildDetailRow('Fe. Creación:',
                    parametro.fechaCreacion.toString() ?? 'N/A'),
                _buildDetailRow('Fe. Modificación:',
                    parametro.fechaModificacion.toString() ?? 'N/A'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                'Cerrar',
                style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  Widget _buildDetailRow(String title, String value) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(
              text: '$title ',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16,
                color: Colors.black,
              ),
            ),
            TextSpan(
              text: value,
              style: TextStyle(
                fontSize: 16,
                color: Colors.black,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _showForm() {
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (BuildContext context) {
        return Padding(
          padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      _selectedParametro == null
                          ? 'Crear Parámetro'
                          : 'Actualizar Parámetro',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 16),
                    TextFormField(
                      controller: _nombreController,
                      decoration: InputDecoration(
                        labelText: 'Nombre',
                        border: OutlineInputBorder(),
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Por favor ingrese el nombre del Parámetro';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 10),
                    TextFormField(
                      controller: _descripcionController,
                      decoration: InputDecoration(
                        labelText: 'Descripción',
                        border: OutlineInputBorder(),
                      ),
                      validator: (value) {
                        if (value != null && value.length > 500) {
                          return 'La descripción no puede exceder los 500 caracteres';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        ElevatedButton(
                          onPressed: () {
                            _saveParametro();
                          },
                          child: Text('Guardar'),
                        ),
                        ElevatedButton(
                          onPressed: () {
                            Navigator.of(context).pop(); // Cierra el modal
                            setState(() {
                              _isAddingOrUpdating = false;
                              _nombreController.clear();
                              _descripcionController.clear();
                              _selectedParametro = null;
                            });
                          },
                          child: Text('Cancelar'),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: roles.contains(Constants.roleAdmin)
          ? Padding(
              padding: const EdgeInsets.only(bottom: 60.0),
              child: FloatingActionButton(
                onPressed: () {
                  setState(() {
                    _isAddingOrUpdating = true;
                    _selectedParametro = null;
                    _nombreController.clear();
                    _descripcionController.clear();
                    _showForm(); // Muestra el formulario
                  });
                },
                child: Icon(Icons.add),
                shape: CircleBorder(),
                backgroundColor: Colors.blue,
              ),
            )
          : null,
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: _searchController,
                    decoration: InputDecoration(
                      hintText: 'Buscar por ID o Nombre',
                      border: OutlineInputBorder(),
                      suffixIcon: Icon(Icons.search_sharp),
                    ),
                    onChanged: (value) => _filterParametros(),
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: PaginatedDataTable(
                  rowsPerPage: _rowsPerPage,
                  availableRowsPerPage: const [5, 10, 15],
                  onRowsPerPageChanged: (rowsPerPage) {
                    setState(() {
                      _rowsPerPage = rowsPerPage ?? 5;
                      _currentPage = 1; // Reinicia a la primera página
                    });
                    _fetchParametros(page: _currentPage, size: _rowsPerPage);
                  },
                  onPageChanged: (page) {
                    final newPage = page ~/ _rowsPerPage + 1;
                    if (newPage != _currentPage) {
                      setState(() {
                        _currentPage = newPage;
                      });
                      _fetchParametros(page: _currentPage, size: _rowsPerPage);
                    }
                  },
                  columns: [
                    DataColumn(label: Center(child: Text('ID'))),
                    DataColumn(label: Center(child: Text('Nombre'))),
                    DataColumn(label: Center(child: Text('Estado'))),
                    DataColumn(label: Center(child: Text('Acciones'))),
                  ],
                  source: ParametroDataSource(
                    context: context,
                    parametros: _filteredParametros, // Usa la lista filtrada
                    onDelete: _deleteParametro,
                    onEdit: (Parametro rol) {
                      setState(() {
                        _selectedParametro = rol;
                        _nombreController.text = rol.nombre;
                        _descripcionController.text = rol.descripcion;
                        _isAddingOrUpdating = true;
                      });
                      _showForm(); // Muestra el formulario de actualización
                    },
                    onView: _showDetails,
                    roles: roles,
                    totalRecords: _filteredParametros
                        .length, // Asegúrate de usar la cantidad filtrada
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ParametroDataSource extends DataTableSource {
  final BuildContext context;
  final List<Parametro> parametros;
  final Function(int) onDelete;
  final Function(Parametro) onEdit;
  final Function(Parametro) onView;
  final List<String> roles;
  final int totalRecords;

  ParametroDataSource({
    required this.context,
    required this.parametros,
    required this.onDelete,
    required this.onEdit,
    required this.onView,
    required this.roles,
    required this.totalRecords,
  });

  @override
  DataRow? getRow(int index) {
    final d;
    if (!parametros.isEmpty || index < parametros.length) {
      // if (isFilter) {
      // Aquí puedes aplicar una lógica distinta si la propiedad isFilter es true
      // if (detalleParametros.isEmpty || index >= detalleParametros.length) {
      //   return null; // No hay coincidencias o estamos fuera de rango
      // } else {
      // final d = detalleParametros[index];
      // }
      // }
      int adjustedIndex = index % parametros.length;
      d = parametros[adjustedIndex];
    } else {
      return null; // No hay coincidencias o estamos fuera de rango
    }
    if (parametros.isEmpty || index >= parametros.length) {
      return null; // No hay coincidencias o estamos fuera de rango
    }
    final p = parametros[index];
    return DataRow(cells: [
      DataCell(Center(child: Text(p.id.toString()))),
      DataCell(Center(child: Text(p.nombre))),
      DataCell(Center(child: Text(p.estado))),
      DataCell(Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: _buildActionButtons(p),
      )),
    ]);
  }

  List<Widget> _buildActionButtons(Parametro p) {
    List<Widget> buttons = [];

    buttons.add(
      IconButton(
        icon: Icon(Icons.visibility),
        onPressed: () => onView(p),
      ),
    );

    if (roles.contains(Constants.roleAdmin)) {
      buttons.add(
        IconButton(
          icon: Icon(Icons.edit),
          onPressed: () => onEdit(p),
        ),
      );
      buttons.add(
        IconButton(
          icon: Icon(Icons.delete),
          onPressed: () {
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text('Confirmación'),
                  content: Text(
                      '¿Estás seguro de que quieres eliminar este Parámetro?'),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text('Cancelar'),
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                        onDelete(p.id!);
                      },
                      child: Text('Eliminar'),
                    ),
                  ],
                );
              },
            );
          },
        ),
      );
    }

    return buttons;
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => totalRecords;

  @override
  int get selectedRowCount => 0;

  @override
  void selectAll(bool checked) {}
}
