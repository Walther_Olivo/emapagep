import 'package:emapagep/config/constants.dart';
import 'package:emapagep/dto/generic_response_dto.dart';
import 'package:emapagep/widgets/loading_dialog.dart';
import 'package:flutter/material.dart';
import 'package:emapagep/models/tipo_reclamo.dart';
import 'package:emapagep/services/tipo_reclamo_service.dart';
import 'package:emapagep/widgets/messages.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:jwt_decoder/jwt_decoder.dart';

class TipoReclamoScreen extends StatefulWidget {
  @override
  _TipoReclamoScreenState createState() => _TipoReclamoScreenState();
}

class _TipoReclamoScreenState extends State<TipoReclamoScreen> {
  final TextEditingController _nombreController = TextEditingController();
  final TextEditingController _descripcionController = TextEditingController();
  final TextEditingController _searchController = TextEditingController();
  final TipoReclamoService _tipoReclamoService = TipoReclamoService();
  List<TipoReclamo> _tipoReclamos = [];
  List<TipoReclamo> _filteredTipoReclamos = [];
  TipoReclamo? _selectedTipoReclamo;
  bool _isAddingOrUpdating = false;
  final storage = FlutterSecureStorage();
  List<String> roles = [];
  String? _token;
  final _formKey = GlobalKey<FormState>();
  int _currentPage = 1;
  int _totalRecords = 0;
  int _rowsPerPage = 5;
  @override
  void initState() {
    super.initState();
    _loadRolesAndToken();
  }

  Future<void> _loadRolesAndToken() async {
    String? token = await storage.read(key: 'auth_token');
    if (token != null) {
      Map<String, dynamic> decodedToken = JwtDecoder.decode(token);
      setState(() {
        roles = List<String>.from(
            decodedToken['roles'].map((role) => role['name']));
        _token = token;
      });
      _fetchTipoReclamos(page: _currentPage, size: _rowsPerPage);
    }
  }

  Future<void> _fetchTipoReclamos({int page = 1, int size = 5}) async {
    if (_token == null) return;
    LoadingDialog.show(context, message: 'Cargando Tipo Reclamos...');
    try {
      final response = await _tipoReclamoService.getAllTipoReclamos(
        _token!,
        page: page,
        size: size,
      );
      final tipoReclamos = response.payload;
      setState(() {
        _tipoReclamos = tipoReclamos;
        _filteredTipoReclamos = tipoReclamos;
        _totalRecords = response.totalRecords ?? 0;
        _currentPage = page; // Actualiza la página actual
        _rowsPerPage = size; // Actualiza el tamaño de página
        LoadingDialog.hide(context);
      });
    } catch (e) {
      LoadingDialog.hide(context);
      final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
      Messages.showMessage(context, 'Error: $errorMessage',
          isError: true, isTop: true);
    }
  }

  void _filterTipoReclamos() {
    final query = _searchController.text.toLowerCase();
    setState(() {
      _filteredTipoReclamos = _tipoReclamos.where((tipoReclamo) {
        return tipoReclamo.nombre.toLowerCase().contains(query) ||
            tipoReclamo.id.toString().contains(query);
      }).toList();
    });
  }

  Future<void> _saveTipoReclamo() async {
    if (_token == null) return;
    if (_formKey.currentState?.validate() ?? false) {
      final nombre = _nombreController.text;
      if (nombre.isEmpty) return;

      final tipoReclamo = TipoReclamo(
        id: _selectedTipoReclamo?.id,
        nombre: nombre,
        estado: 'Activo',
        descripcion: _descripcionController.text,
      );

      try {
        GenericResponseDto<void> response;
        if (_selectedTipoReclamo == null) {
          LoadingDialog.show(context, message: 'Creando Tipo Reclamo...');
          response =
              await _tipoReclamoService.createTipoReclamo(tipoReclamo, _token!);
          LoadingDialog.hide(context);
        } else {
          LoadingDialog.show(context, message: 'Actualizando Tipo Reclamo...');
          response = await _tipoReclamoService.updateTipoReclamo(
              tipoReclamo.id!, tipoReclamo, _token!);
          LoadingDialog.hide(context);
        }
        Messages.showMessage(context, response.message);
        Navigator.of(context).pop(); // Cierra el modal
        _fetchTipoReclamos(page: _currentPage, size: _rowsPerPage);
        setState(() {
          _isAddingOrUpdating = false;
          _selectedTipoReclamo = null;
        });
      } catch (e) {
        LoadingDialog.hide(context);
        final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
        Messages.showMessage(context, 'Error: $errorMessage',
            isError: true, isTop: true);
      }
    }
  }

  Future<void> _deleteTipoReclamo(int id) async {
    GenericResponseDto<void> response;
    LoadingDialog.show(context, message: 'Eliminando Tipo Reclamo...');
    try {
      response = await _tipoReclamoService.deleteTipoReclamo(id, _token!);
      LoadingDialog.hide(context);
      _fetchTipoReclamos(page: _currentPage, size: _rowsPerPage);
      Messages.showMessage(context, response.message);
    } catch (e) {
      LoadingDialog.hide(context);
      final errorMessage = e.toString().replaceFirst('Exception:', '').trim();
      Messages.showMessage(context, 'Error: $errorMessage',
          isError: true, isTop: true);
    }
  }

  void _showDetails(TipoReclamo tipoReclamo) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16.0), // Bordes redondeados
          ),
          title: Text(
            'Detalles',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          content: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildDetailRow('ID:', tipoReclamo.id.toString()),
                _buildDetailRow('Nombre:', tipoReclamo.nombre),
                _buildDetailRow('Descripción:', tipoReclamo.descripcion!),
                _buildDetailRow('Estado:', tipoReclamo.estado),
                _buildDetailRow('Fe. Creación:',
                    tipoReclamo.fechaCreacion.toString() ?? 'N/A'),
                _buildDetailRow('Fe. Modificación:',
                    tipoReclamo.fechaModificacion.toString() ?? 'N/A'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                'Cerrar',
                style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  Widget _buildDetailRow(String title, String value) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(
              text: '$title ',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16,
                color: Colors.black,
              ),
            ),
            TextSpan(
              text: value,
              style: TextStyle(
                fontSize: 16,
                color: Colors.black,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _showForm() {
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (BuildContext context) {
        return Padding(
          padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      _selectedTipoReclamo == null
                          ? 'Crear Tipo Reclamo'
                          : 'Actualizar Tipo Reclamo',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 10),
                    TextFormField(
                      controller: _nombreController,
                      maxLength: 25,
                      decoration: InputDecoration(
                        labelText: 'Nombre del Tipo Reclamo',
                        border: OutlineInputBorder(),
                        counterText: '',
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Por favor ingresa el nombre del Tipo Reclamo';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 10),
                    TextFormField(
                      controller: _descripcionController,
                      maxLength: 100,
                      decoration: InputDecoration(
                        labelText: 'Descripción del Tipo Reclamo',
                        border: OutlineInputBorder(),
                        counterText: '',
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Por favor ingresa la descripción del Tipo Reclamo';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        ElevatedButton(
                          onPressed: () {
                            _saveTipoReclamo();
                          },
                          child: Text('Guardar'),
                        ),
                        ElevatedButton(
                          onPressed: () {
                            Navigator.of(context).pop(); // Cierra el modal
                            setState(() {
                              _isAddingOrUpdating = false;
                              _nombreController.clear();
                              _descripcionController.clear();
                              _selectedTipoReclamo = null;
                            });
                          },
                          child: Text('Cancelar'),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: roles.contains(Constants.roleAdmin)
          ? Padding(
              padding: const EdgeInsets.only(
                  bottom: 60.0), // Ajusta el valor según sea necesario
              child: FloatingActionButton(
                onPressed: () {
                  setState(() {
                    _isAddingOrUpdating = true;
                    _selectedTipoReclamo = null;
                    _nombreController.clear();
                    _descripcionController.clear();
                    _showForm(); // Muestra el formulario
                  });
                },
                child: Icon(Icons.add),
                shape: CircleBorder(), // Asegura que el botón sea redondo
                backgroundColor: Colors.blue, // Color de fondo del botón
              ),
            )
          : null,
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: _searchController,
                    decoration: InputDecoration(
                      hintText: 'Buscar por ID o Nombre',
                      border: OutlineInputBorder(),
                      suffixIcon: Icon(Icons.search),
                    ),
                    onChanged: (value) => _filterTipoReclamos(),
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: PaginatedDataTable(
                  rowsPerPage: _rowsPerPage,
                  availableRowsPerPage: const [5, 10, 15],
                  onRowsPerPageChanged: (rowsPerPage) {
                    setState(() {
                      _rowsPerPage = rowsPerPage ?? 5;
                      _currentPage = 1; // Reinicia a la primera página
                    });
                    _fetchTipoReclamos(page: _currentPage, size: _rowsPerPage);
                  },
                  onPageChanged: (page) {
                    final newPage = page ~/ _rowsPerPage + 1;
                    if (newPage != _currentPage) {
                      setState(() {
                        _currentPage = newPage;
                      });
                      _fetchTipoReclamos(
                          page: _currentPage, size: _rowsPerPage);
                    }
                  },
                  columns: [
                    DataColumn(label: Center(child: Text('ID'))),
                    DataColumn(label: Center(child: Text('Nombre'))),
                    DataColumn(label: Center(child: Text('Estado'))),
                    DataColumn(label: Center(child: Text('Acciones'))),
                  ],
                  source: TipoReclamoDataSource(
                      context: context,
                      tipoReclamos: _filteredTipoReclamos,
                      onDelete: _deleteTipoReclamo,
                      onEdit: (TipoReclamo tipoReclamo) {
                        setState(() {
                          _selectedTipoReclamo = tipoReclamo;
                          _nombreController.text = tipoReclamo.nombre;
                          _descripcionController.text =
                              tipoReclamo.descripcion!;
                          _isAddingOrUpdating = true;
                        });
                        _showForm(); // Muestra el formulario de actualización
                      },
                      onView: _showDetails,
                      roles: roles,
                      totalRecords: _filteredTipoReclamos.length),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class TipoReclamoDataSource extends DataTableSource {
  final BuildContext context;
  final List<TipoReclamo> tipoReclamos;
  final Function(int) onDelete;
  final Function(TipoReclamo) onEdit;
  final Function(TipoReclamo) onView;
  final List<String> roles;
  final int totalRecords;

  TipoReclamoDataSource({
    required this.context,
    required this.tipoReclamos,
    required this.onDelete,
    required this.onEdit,
    required this.onView,
    required this.roles,
    required this.totalRecords,
  });

  @override
  DataRow? getRow(int index) {
    if (tipoReclamos.isEmpty || index >= tipoReclamos.length) {
      return null; // No hay coincidencias o estamos fuera de rango
    }
    final rol = tipoReclamos[index];
    return DataRow(cells: [
      DataCell(Center(child: Text(rol.id.toString()))),
      DataCell(Center(child: Text(rol.nombre))),
      DataCell(Center(child: Text(rol.estado))),
      DataCell(Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: _buildActionButtons(rol),
      )),
    ]);
  }

  List<Widget> _buildActionButtons(TipoReclamo tipoReclamo) {
    List<Widget> buttons = [];

    // Mostrar el botón de detalles a todos los roles
    buttons.add(
      IconButton(
        icon: Icon(Icons.visibility),
        onPressed: () => onView(tipoReclamo),
      ),
    );

    // Mostrar el botón de edición solo para ADMIN
    if (roles.contains(Constants.roleAdmin)) {
      buttons.add(
        IconButton(
          icon: Icon(Icons.edit),
          onPressed: () => onEdit(tipoReclamo),
        ),
      );
    }

    // Mostrar el botón de eliminación solo para ADMIN
    if (roles.contains(Constants.roleAdmin)) {
      buttons.add(
        IconButton(
          icon: Icon(Icons.delete),
          onPressed: () {
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text('Confirmación'),
                  content: Text(
                      '¿Estás seguro de que quieres eliminar este Tipo reclamo?'),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text('Cancelar'),
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                        onDelete(tipoReclamo.id!);
                      },
                      child: Text('Eliminar'),
                    ),
                  ],
                );
              },
            );
          },
        ),
      );
    }

    return buttons;
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => totalRecords;

  @override
  int get selectedRowCount => 0;

  @override
  void selectAll(bool checked) {}
}
