import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // Imagen
                Image.asset(
                  'assets/emapag-ep.png',
                  height: 200,
                ),
                SizedBox(height: 20),
                Text(
                  'Quiénes Somos',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 10),

                Text(
                  'La Empresa Municipal de Agua Potable y Alcantarillado de Guayaquil – EP, EMAPAG – EP, tiene como finalidad contribuir a mejorar las condiciones básicas de vida y salud de la población. Controlar que el concesionario cumpla con las obligaciones de prestación eficiente de los servicios de agua potable, alcantarillado sanitario y drenaje pluvial en el Cantón Guayaquil. Vigilar que se mantenga siempre el compromiso de servir con oportunidad y calidad para que los ciudadanos de Guayaquil reciban cada vez un mejor servicio.',
                  textAlign: TextAlign.justify,
                  style: TextStyle(fontSize: 16),
                ),
                SizedBox(height: 20),

                // Título Misión
                Text(
                  'Misión',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 10),

                // Misión
                Text(
                  'Desarrollar, mantener y operar la infraestructura instalada para la dotación de servicios básicos de agua potable y alcantarillado de manera eficiente para contribuir a la salud y bienestar de la ciudadanía ambateña, garantizando el mantenimiento y conservación de las fuentes de agua, apoyando en el cuidado ambiental de la zona de influencia, implementando tecnología adecuada y altos estándares de calidad.',
                  textAlign: TextAlign.justify,
                  style: TextStyle(fontSize: 16),
                ),
                SizedBox(height: 20),

                // Título Visión
                Text(
                  'Visión',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 10),
                Text(
                  'Ser reconocida como una empresa eficiente, rentable e innovadora en la dotación de servicios de agua potable y alcantarillado, con responsabilidad social y ambiental en el desarrollo de obras y proyectos de agua potable y alcantarillado.',
                  textAlign: TextAlign.justify,
                  style: TextStyle(fontSize: 16),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
