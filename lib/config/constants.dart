import 'package:flutter_dotenv/flutter_dotenv.dart';

class Constants {
  // Base URL
  static final String baseUrl = dotenv.env['BASE_URL'] ?? '';

  // API Paths
  static final String pathUsuario = dotenv.env['PATH_USUARIO'] ?? '';
  static final String pathRol = dotenv.env['PATH_ROL'] ?? '';
  static final String pathTipoReclamo = dotenv.env['PATH_TIPO_RECLAMO'] ?? '';
  static final String pathReclamo = dotenv.env['PATH_RECLAMO'] ?? '';
  static String pathParametro = dotenv.env['PATH_PARAMETRO'] ?? '';
  static final String pathDetalleParametro =
      dotenv.env['PATH_DETALLE_PARAMETRO'] ?? '';
  static final String pathRespuestas = dotenv.env['PATH_RESPUESTAS'] ?? '';
  static final String pathSolucionReclamos =
      dotenv.env['PATH_SOLUCION_RECLAMOS'] ?? '';
  static final String pathMotivosSolucionReclamos =
      dotenv.env['PATH_MOTIVOS_SOLUCION_RECLAMOS'] ?? '';
  static final String pathMedioPromocion =
      dotenv.env['PATH_MEDIO_PROMOCION'] ?? '';
  static final String pathAuth = dotenv.env['PATH_AUTH'] ?? '';
  static final String pathValidateCode = dotenv.env['PATH_VALIDATE_CODE'] ?? '';
  static final String pathValidEmailSendCode =
      dotenv.env['PATH_VALID_EMAIL_SEND_CODE'] ?? '';
  static final String pathChangePassword =
      dotenv.env['PATH_CHANGE_PASSWORD'] ?? '';
  static final String pathLogin = dotenv.env['PATH_LOGIN'] ?? '';
  static final String pathRegister = dotenv.env['PATH_REGISTER'] ?? '';
  static final String pathEditProfile = dotenv.env['PATH_EDIT_PROFILE'] ?? '';
  static final String pathParentescos = dotenv.env['PATH_PARENTESCOS'] ?? '';
  static final String pathFindContrato = dotenv.env['PATH_FIND_CONTRATO'] ?? '';
  static final String pathSolucion = dotenv.env['PATH_SOLUCION'] ?? '';

  // Roles
  static final String roleAdmin = dotenv.env['ROLE_ADMIN'] ?? '';
  static final String roleCliente = dotenv.env['ROLE_CLIENTE'] ?? '';
  static final String roleAtencionAlUsuario =
      dotenv.env['ROLE_ATENCION_AL_USUARIO'] ?? '';
}
