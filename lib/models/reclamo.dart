import 'package:intl/intl.dart';

class Reclamo {
  final int? id;
  final int? tipoReclamoId;
  final int? medioPromocionId;
  final String? numContrato;
  final String? identificacionCliente;
  final String? nombresCliente;
  final String? apellidosCliente;
  final String? direccionCliente;
  final String? telefonoCliente;
  final String? emailCliente;
  final String? numeroTramite;
  final int? solucionId;
  final int? parentescoId;
  final String? identificacionRepresentante;
  final String? nombreRepresentante;
  final String? direccionRepresentante;
  final String? telefonoRepresentante;
  final int? motivoSolucionId;
  final DateTime? periodoInicial;
  final DateTime? periodoFinal;
  final String? comentario;
  final String? copiaDeCedula;
  final String? respuestaInteragua;
  final String? estado;
  final String? cartaExplicativa;
  final String? cartaAutorizacion;
  final String? formularioReclamo;
  final int? creadorId;
  final DateTime? fechaCreacion;
  final DateTime? fechaModificacion;

  Reclamo({
    this.id,
    this.tipoReclamoId,
    this.medioPromocionId,
    this.numContrato,
    this.identificacionCliente,
    this.nombresCliente,
    this.apellidosCliente,
    this.direccionCliente,
    this.telefonoCliente,
    this.emailCliente,
    this.numeroTramite,
    this.solucionId,
    this.parentescoId,
    this.identificacionRepresentante,
    this.nombreRepresentante,
    this.direccionRepresentante,
    this.telefonoRepresentante,
    this.motivoSolucionId,
    this.periodoInicial,
    this.periodoFinal,
    this.comentario,
    this.copiaDeCedula,
    this.respuestaInteragua,
    this.estado,
    this.cartaExplicativa,
    this.cartaAutorizacion,
    this.formularioReclamo,
    this.creadorId,
    this.fechaCreacion,
    this.fechaModificacion,
  });

  factory Reclamo.fromJson(Map<String, dynamic> json) {
    return Reclamo(
      id: json['id'],
      tipoReclamoId: json['tipoReclamo']?['id'],
      medioPromocionId: json['medioPromocion']?['id'],
      numContrato: json['numContrato'],
      identificacionCliente: json['identificacionCliente'],
      nombresCliente: json['nombresCliente'],
      apellidosCliente: json['apellidosCliente'],
      direccionCliente: json['direccionCliente'],
      telefonoCliente: json['telefonoCliente'],
      emailCliente: json['emailCliente'],
      numeroTramite: json['numeroTramite'],
      solucionId: json['solucion']?['id'],
      parentescoId: json['parentesco']?['id'],
      identificacionRepresentante: json['identificacionRepresentante'],
      nombreRepresentante: json['nombreRepresentante'],
      direccionRepresentante: json['direccionRepresentante'],
      telefonoRepresentante: json['telefonoRepresentante'],
      motivoSolucionId: json['motivoSolucion']?['id'],
      periodoInicial: json['periodoInicial'] != null
          ? DateTime.parse(json['periodoInicial'])
          : null,
      periodoFinal: json['periodoFinal'] != null
          ? DateTime.parse(json['periodoFinal'])
          : null,
      comentario: json['comentario'],
      copiaDeCedula: json['copiaDeCedula'],
      respuestaInteragua: json['respuestaInteragua'],
      estado: json['estado'],
      cartaExplicativa: json['cartaExplicativa'],
      cartaAutorizacion: json['cartaAutorizacion'],
      formularioReclamo: json['formularioReclamo'],
      creadorId: json['creador']?['id'],
      fechaCreacion: json['fechaCreacion'] != null
          ? DateTime.parse(json['fechaCreacion'])
          : null,
      fechaModificacion: json['fechaModificacion'] != null
          ? DateTime.parse(json['fechaModificacion'])
          : null,
    );
  }

  String _formatDateTime(DateTime? dateTime) {
    if (dateTime == null) return '';
    return DateFormat('yyyy-MM-dd HH:mm:ss').format(dateTime);
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'tipoReclamo': tipoReclamoId != null ? {'id': tipoReclamoId} : null,
      'medioPromocion':
          medioPromocionId != null ? {'id': medioPromocionId} : null,
      'numContrato': numContrato,
      'identificacionCliente': identificacionCliente,
      'nombresCliente': nombresCliente,
      'apellidosCliente': apellidosCliente,
      'direccionCliente': direccionCliente,
      'telefonoCliente': telefonoCliente,
      'emailCliente': emailCliente,
      'numeroTramite': numeroTramite,
      'solucion': solucionId != null ? {'id': solucionId} : null,
      'parentesco': parentescoId != null ? {'id': parentescoId} : null,
      'identificacionRepresentante': identificacionRepresentante,
      'nombreRepresentante': nombreRepresentante,
      'direccionRepresentante': direccionRepresentante,
      'telefonoRepresentante': telefonoRepresentante,
      'motivoSolucion':
          motivoSolucionId != null ? {'id': motivoSolucionId} : null,
      'periodoInicial': _formatDateTime(periodoInicial),
      'periodoFinal': _formatDateTime(periodoFinal),
      'comentario': comentario,
      'copiaDeCedula': copiaDeCedula,
      'respuestaInteragua': respuestaInteragua,
      'estado': estado,
      'cartaExplicativa': cartaExplicativa,
      'cartaAutorizacion': cartaAutorizacion,
      'formularioReclamo': formularioReclamo,
      'creador': creadorId != null ? {'id': creadorId} : null,
      'fechaCreacion': fechaCreacion?.toIso8601String(),
      'fechaModificacion': fechaModificacion?.toIso8601String(),
    };
  }
}
