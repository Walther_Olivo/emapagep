class TipoReclamo {
  final int? id;
  final String nombre;
  final String estado;
  final String? descripcion;
  final DateTime? fechaCreacion;
  final DateTime? fechaModificacion;

  TipoReclamo({
    this.id,
    required this.nombre,
    required this.estado,
    this.descripcion,
    this.fechaCreacion,
    this.fechaModificacion,
  });

  factory TipoReclamo.fromJson(Map<String, dynamic> json) {
    return TipoReclamo(
      id: json['id'],
      nombre: json['nombre'],
      estado: json['estado'],
      descripcion: json['descripcion'],
      fechaCreacion: json['fechaCreacion'] != null
          ? DateTime.parse(json['fechaCreacion'])
          : null,
      fechaModificacion: json['fechaModificacion'] != null
          ? DateTime.parse(json['fechaModificacion'])
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'nombre': nombre,
      'estado': estado,
      'descripcion': descripcion,
      'fechaCreacion': fechaCreacion?.toIso8601String(),
      'fechaModificacion': fechaModificacion?.toIso8601String(),
    };
  }
}
