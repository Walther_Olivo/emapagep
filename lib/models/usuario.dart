import 'package:intl/intl.dart';

class Usuario {
  final int? id;
  final String identificacion;
  final String nombres;
  final String apellidos;
  final String email;
  final String nombreUsuario;
  final String contrasenia;
  final String telefono;
  final String direccion;
  final String estado;
  final String? foto;
  final DateTime fechaNacimiento;
  final List<int>? roles; // Cambiado a lista de IDs de roles
  final DateTime? fechaCreacion;
  final DateTime? fechaModificacion;

  Usuario({
    this.id,
    required this.identificacion,
    required this.nombres,
    required this.apellidos,
    required this.email,
    required this.nombreUsuario,
    required this.contrasenia,
    required this.telefono,
    required this.direccion,
    required this.estado,
    this.foto,
    required this.fechaNacimiento,
    this.roles, // Lista de IDs de roles
    this.fechaCreacion,
    this.fechaModificacion,
  });

  factory Usuario.fromJson(Map<String, dynamic> json) {
    var rolesJson = json['roles'] as List<dynamic>?;
    List<int> rolesList = rolesJson != null
        ? rolesJson.map((roleJson) => roleJson['id'] as int).toList()
        : [];

    return Usuario(
      id: json['id'],
      identificacion: json['identificacion'],
      nombres: json['nombres'],
      apellidos: json['apellidos'],
      email: json['email'],
      nombreUsuario: json['nombreUsuario'],
      contrasenia: json['contrasenia'],
      telefono: json['telefono'],
      direccion: json['direccion'],
      estado: json['estado'],
      foto: json['foto'],
      fechaNacimiento: DateTime.parse(json['fechaNacimiento']),
      roles: rolesList, // Lista de IDs de roles
      fechaCreacion: json['fechaCreacion'] != null
          ? DateTime.parse(json['fechaCreacion'])
          : null,
      fechaModificacion: json['fechaModificacion'] != null
          ? DateTime.parse(json['fechaModificacion'])
          : null,
    );
  }
  String _formatDateTime(DateTime? dateTime) {
    if (dateTime == null) return '';
    return DateFormat('yyyy-MM-dd HH:mm:ss').format(dateTime);
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'identificacion': identificacion,
      'nombres': nombres,
      'apellidos': apellidos,
      'email': email,
      'nombreUsuario': nombreUsuario,
      'contrasenia': contrasenia,
      'telefono': telefono,
      'direccion': direccion,
      'estado': estado,
      'foto': foto,
      'fechaNacimiento': _formatDateTime(fechaNacimiento),
      'roles': roles
          ?.map((roleId) => {'id': roleId})
          .toList(), // Lista de IDs de roles
      'fechaCreacion': fechaCreacion?.toIso8601String(),
      'fechaModificacion': fechaModificacion?.toIso8601String(),
    };
  }
}
