class Parametro {
  final int? id;
  final String nombre;
  final String descripcion;
  final String estado;
  final DateTime? fechaCreacion;
  final DateTime? fechaModificacion;

  Parametro({
    this.id,
    required this.nombre,
    required this.descripcion,
    required this.estado,
    this.fechaCreacion,
    this.fechaModificacion,
  });

  factory Parametro.fromJson(Map<String, dynamic> json) {
    return Parametro(
      id: json['id'],
      nombre: json['nombre'],
      descripcion: json['descripcion'],
      estado: json['estado'],
      fechaCreacion: json['fechaCreacion'] != null
          ? DateTime.parse(json['fechaCreacion'])
          : null,
      fechaModificacion: json['fechaModificacion'] != null
          ? DateTime.parse(json['fechaModificacion'])
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'nombre': nombre,
      'descripcion': descripcion,
      'estado': estado,
      'fechaCreacion': fechaCreacion?.toIso8601String(),
      'fechaModificacion': fechaModificacion?.toIso8601String(),
    };
  }
}
