class DetalleParametro {
  final int? id;
  final int? parametroId; // Solo ID del parametro
  final String nombre;
  final String descripcion;
  final String estado;
  final String? valor;
  final DateTime? fechaCreacion;
  final DateTime? fechaModificacion;

  DetalleParametro({
    this.id,
    required this.parametroId, // Solo ID del parametro
    required this.nombre,
    required this.descripcion,
    required this.estado,
    this.valor,
    this.fechaCreacion,
    this.fechaModificacion,
  });

  factory DetalleParametro.fromJson(Map<String, dynamic> json) {
    return DetalleParametro(
      id: json['id'],
      parametroId: json['parametro']['id'], // Solo ID del parametro
      nombre: json['nombre'],
      descripcion: json['descripcion'],
      estado: json['estado'],
      valor: json['valor'] as String?,
      fechaCreacion: json['fechaCreacion'] != null
          ? DateTime.parse(json['fechaCreacion'])
          : null,
      fechaModificacion: json['fechaModificacion'] != null
          ? DateTime.parse(json['fechaModificacion'])
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'parametro': {
        'id': parametroId,
      },
      'nombre': nombre,
      'descripcion': descripcion,
      'estado': estado,
      'valor': valor,
      'fechaCreacion': fechaCreacion?.toIso8601String(),
      'fechaModificacion': fechaModificacion?.toIso8601String(),
    };
  }
}
