import 'package:flutter/material.dart';

class Messages {
  static void showMessage(BuildContext context, String message,
      {bool isError = false, bool isTop = false}) {
    final backgroundColor = isError ? Colors.red : Colors.green;

    if (isTop) {
      // Mostrar el mensaje en la parte superior usando Overlay
      _showTopMessage(context, message, backgroundColor);
    } else {
      // Mostrar el mensaje en la parte inferior como SnackBar
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            message,
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: backgroundColor,
        ),
      );
    }
  }

  static void _showTopMessage(
      BuildContext context, String message, Color backgroundColor) {
    final overlay = Overlay.of(context);
    final overlayEntry = OverlayEntry(
      builder: (context) => Positioned(
        top: MediaQuery.of(context).padding.top, // Para estar pegado al top
        left: 0,
        right: 0,
        child: Material(
          color: Colors.transparent,
          child: Container(
            color: backgroundColor,
            padding: const EdgeInsets.all(15), // Agregar padding
            child: Text(
              message,
              style: TextStyle(
                color: Colors.white,
                //fontSize: 16, // Ajustar el tamaño de fuente
              ),
              //  textAlign: TextAlign.center, // Centrar el texto
            ),
          ),
        ),
      ),
    );

    overlay?.insert(overlayEntry);

    // Ocultar el mensaje después de 3 segundos
    Future.delayed(Duration(seconds: 3), () {
      overlayEntry.remove();
    });
  }
}
