import 'package:flutter/material.dart';

class LoadingDialog {
  static void show(BuildContext context, {String message = 'Cargando...'}) {
    showDialog(
      barrierDismissible: false, // No permitir cerrar el diálogo con un toque
      context: context,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () async =>
              false, // Evita cerrar el diálogo presionando 'back'
          child: Dialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                mainAxisSize: MainAxisSize.min, // Ajusta el tamaño del diálogo
                children: [
                  CircularProgressIndicator(),
                  const SizedBox(height: 20),
                  Text(message),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  static void hide(BuildContext context) {
    Navigator.of(context, rootNavigator: true).pop();
  }
}
